/**
* \file Screen.cpp
* \brief Implémentation partielle de Screen.
* \author Equipe 5
* \version 1.0
*
*/
#include "Screen.h"
#include <iostream>

Screen::Screen() {
	memset(keyIsDown, false, 512);
}



void Screen::keyPressed(int key) {
	if (key >= 0 && key < 512) keyIsDown[key] = true;
}




void Screen::keyReleased(int key) {
	if (key >= 0 && key < 512) keyIsDown[key] = false;
}