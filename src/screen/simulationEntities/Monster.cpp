/**
* \file Monster.cpp
* \brief Classe responsable de la gestion du monstre dans le labyrinthe
* \author Equipe 5
* \version 1.0
*
*/
#include "Monster.h"
#include <algorithm>

ofVec2f Monster::Move::getEnd() const {
	return ofVec2f(start.x + weather::vec(direction).first,
				   start.y + weather::vec(direction).second);
}



void Monster::update(Monster::SectorsInRange sectors, double secs_ellapsed) {

	if(currentMove.stayPut() || currentMove.isDone()) {
		//Le monstre ne se déplace pas, présentement.
		//On lui cherche un nouveau déplacement.
		
		//Trouver les déplacements possibles
		std::vector<weather::CardinalDirection> possible_directions;
		
		for(SectorsInRange::iterator it = sectors.begin(); it != sectors.end(); it++) {
			if(it->second->isPassable()) {
				possible_directions.push_back(it->first);
			}
		}
		
		//Un peu d'imprévisibilité...
		std::random_shuffle(possible_directions.begin(), possible_directions.end());
		
		//Ne considérer le déplacement inverse au déplacement précédent
		//qu'en dernier recours. On évite qu'il fasse des va-et-vient ennuyants.
		if(!currentMove.stayPut()) {
			std::vector<weather::CardinalDirection>::iterator it =
				std::find(possible_directions.begin(),
						  possible_directions.end(),
						  weather::opposite(currentMove.direction));
			
			if(it != possible_directions.end()) {
				possible_directions.erase(it);
			}
			possible_directions.push_back(weather::opposite(currentMove.direction));
		}

		//Nouveau déplacement
		currentMove = Move();
		if(0 < possible_directions.size()) {
			currentMove.duration = 1;
			currentMove.timeSpent = 0;
			currentMove.start = ofVec2f(x, y);
			currentMove.direction = possible_directions.front();
		}
	}
	
	if(!currentMove.stayPut() && ! currentMove.isDone()) {
		//Il existe un déplacement en cours.
		//Le faire progresser selon le temps écoulé.
		
		if(secs_ellapsed < currentMove.timeLeft()) {
			currentMove.timeSpent += secs_ellapsed;
		} else {
			currentMove.timeSpent = currentMove.duration;
		}
		
		ofVec2f new_pos(currentMove.start);
		new_pos.interpolate(currentMove.getEnd(), currentMove.timeSpent / currentMove.duration);
		x = new_pos.x;
		y = new_pos.y;
	}
	
}
