/**
* \file Player.cpp
* \brief Permets de creer une instance joueur pour un labyrinthe
* \author Equipe 5
* \version 1.0
*
*/
 
#include "Player.h"
#include <math.h>

Player::Player(double _w, double _h):
w(_w), h(_h), orientation(0, ofVec3f(0, 0, 1)), head(0, ofVec3f(1, 0, 0))
{ }



Player::Player(const Player & old):
x(old.x), y(old.y), w(old.w), h(old.h), orientation(old.orientation), head(old.head)
{ }



Player& Player::operator= (const Player& other) {
	if (this != &other) {
		x = other.x;
		y = other.y;
		w = other.w;
		h = other.h;
	}

	return *this;
}



Player::~Player()
{}



ofVec2f Player::getDirection() const {
	nosmaths::Quaternion behind(PI, ofVec3f(0, 0, 1)); //Pourquoi faut-il que je fasse ca??
	ofMatrix4x4 rot = (orientation * behind).getMatx();
	ofVec4f result = ofVec4f(0, 1, 0, 1) * rot;
	return ofVec2f(result.x, result.y);
}



void Player::setHead(double angle) {
	head = nosmaths::Quaternion(angle, ofVec3f(1, 0, 0));
}



void Player::incrementAngle(double increment) {
	orientation = orientation * nosmaths::Quaternion(increment, ofVec3f(0, 0, 1));
}

