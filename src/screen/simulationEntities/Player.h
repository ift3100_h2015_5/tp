/**
* \file Player.h
* \brief Permets de creer une instance joueur pour un labyrinthe
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include "ofMain.h"
#include "../../nosmaths/nosmaths.h"

/**
 * \brief Point de vue de l'utilisateur en mode Jeu.
 *
 * Contient les rotations appliquées à la caméra première personne
 * du mode Jeu, sa position, ainsi que sa dimension.
 * Les rotations sont stockées sous forme de quaternions.
 * La dimension du joueur est utilisée dans les calculs de collisions
 * avec les obstacles.
 */
class Player
{
private:
	nosmaths::Quaternion orientation; ///<Orientation du joueur. Rotation sur l'axe Z, comme si le joueur se déplace sur le plan (*, *, 0).
	nosmaths::Quaternion head; ///<Rotation de la tête/du regard du joueur. Rotation axe des X.

public:
	double w, h; ///< Dimension
	double x, y; ///< Position, exprimée comme si le joueur se déplace sur le plan (*, *, 0).

	Player(double _w, double _h);
	Player(const Player & old);
	Player & operator= (const Player & other);
	~Player();

	
	/**
	 * \brief La direction vers laquelle le joueur regarde,
	 *        en ne tenant compte que de la rotation sur l'axe des Y.
	 *
	 * La direction est retournée sous la forme d'un vecteur 2D normalisé.
	 * Seul l'axe des Y est pris en compte, afin de fournir un vecteur
	 * utile dans le calcul des déplacements.
	 *
	 * Le vecteur est exprimé comme si le joueur se déplace sur le plan (*, *, 0),
	 * ce qui est plus naturel dans un environnement 2D.
	 */
	ofVec2f getDirection() const;


	
	/**
	 * \brief Modifie la rotation sur l'axe des X (le joueur regarde en haut/en bas).
	 *
	 * \param angle Le nouvel angle de rotation en radians.
	 */
	void setHead(double angle);
	nosmaths::Quaternion getHead() const
	{ return head; }

	
	
	/**
	 * \brief Incrémente la rotation de l'orientation (le joueur se tourne).
	 *
	 * \param angle L'angle à ajouter à l'angle de rotation existant, en radians.
	 */
	void incrementAngle(double increment);
	nosmaths::Quaternion getOrientation() const
	{ return orientation; }
};

