/**
* \file Monster.h
* \brief Classe responsable de la gestion du monstre dans le labyrinthe
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include <map>
#include "../../weather.h"
#include "../../maze/Sector.h"
#include "ofMain.h"

class Monster {
private:
	
	/**
	 * \brief Classe qui représente un déplacement d'une case par le Monstre.
	 *
	 * Le déplacement a une progression exprimée en termes de temps.
	 * La vitesse en cases pas seconde est implicite et correspond à 1/duration.
	 */
	class Move {
	public:
		double duration = -1; ///< durée en secondes. -1 équivaut à stationnaire.
		double timeSpent = 0; ///< temps écoulé depuis le début du déplacement.
		
		weather::CardinalDirection direction = weather::NORTH; ///< Direction du déplacement
		ofVec2f start; ///< Coordonnée (x, y) de départ.
		
		/**
		 * \brief Coordonnée (x, y) de la fin du déplacement
		 */
		ofVec2f getEnd() const;
		
		/**
		 * \brief Temps restant au déplacement, en secondes
		 */
		double timeLeft() const { return duration - timeSpent; }
		
		/**
		 * \brief Vrai si le monstre n'est pas présentement en déplacement.
		 *
		 * Se produit au début de la partie ou si le monstre est dans un secteur sans issue.
		 */
		bool stayPut() const { return duration < 0; }
		
		/**
		 * \brief Le déplacement est terminé.
		 */
		bool isDone() const { return timeSpent >= duration; }
	};
	
public:
	/**
	 * \brief Structure utile pour passer au Monstre sa perception de son environnement.
	 */
	typedef std::map<weather::CardinalDirection, Sector *> SectorsInRange;
	
	Monster() {}
	~Monster() {}
	double w = 0.5, h = 0.5; ///< Dimension
	double x, y; ///< Position, exprimée comme si le monstre se déplace sur le plan (*, *, 0).
	
	Move currentMove; ///< Déplacement en cours pour le Monstre
	
	/**
	 * \brief Met à jour la position du monstre.
	 *
	 * \param sectors Secteurs environnant que le Monstre perçoit.
	 * \param secs_ellapsed Nombre de secondes écoulés depuis ce dernier appel de fonction
	 */
	void update(std::map<weather::CardinalDirection, Sector *> sectors, double secs_ellapsed);
};