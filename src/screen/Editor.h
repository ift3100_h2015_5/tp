/**
* \file Editor.h
* \brief Declaration des methodes de l'editeur de labyrinthe
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include "Screen.h"
#include "../maze/Maze.h"
#include "../scenegraph/orthographic.h"
#include "../graphics/GraphicSelector.h"

/**
 * \brief Interface d'édition de labyrinthe
 *
 * Cette interface permet d'enregistrer et d'ouvrir
 * un labyrinthe, ainsi que de le modifier.
 * Un seul secteur est sélectionné à la fois.
 * Le secteur sélectionné est modifié par le clavier.
 */
class Editor :
public Screen {
public:
	Editor();
	~Editor();
	
	virtual void draw();
	
	/**
	 * \brief Met à jour l'état de l'interface
	 *
	 * \return Nouvelle interface, que ofApp devra adopter comme nouvel état. Nul s'il n'y a pas de changement.
	 */
	virtual Screen * update();
	virtual void setup();
	
	virtual void keyReleased(int key);
	virtual void mousePressed(int x, int y, int button);
	virtual void mouseDragged(int x, int y, int button);
	virtual void mouseReleased(int x, int y, int button);
	virtual void windowResized(int w, int h);
	virtual void guiEvent(ofxUIEventArgs &e);
	
private:
	scenegraph::orthographic::MODE drawMode = scenegraph::orthographic::RENDER;

	/**
	 * \brief Prépare à demander à ofApp de passer à l'interface Menu.
	 *
	 */
	void loadMenu();
	
	
	
	/**
	 * \brief Ouvre un fichier contenant un labyrinthe.
	 *
	 */
	void open();
	
	
	
	/**
	 * \brief Enregistre le labyrinthe courant dans un fichier.
	 *
	 */
	void saveAs();
	
	
	
	/**
	 * \brief Modifie le type du secteur courant.
	 *
	 */
	void changeSector(Sector::Kind);

	
	Screen * nextScreen = 0; ///< Nouvelle interface à retourner à la prochaine mise à jour.

	Maze currentMaze;
	string currentTheme;
	
	scenegraph::NodePtr scene; ///< Graphe de scène du labyrinthe
	
	ofxUICanvas * gui;

	std::vector<Sector *> selectedSectors;
	ofVec2f selectStartPoint;
	ofVec2f selectEndPoint;

	GraphicSelector gfxSelector; ///< Détecteur de secteur cliqué
};