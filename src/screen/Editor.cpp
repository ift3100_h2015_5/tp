/**
* \file Editor.cpp
* \brief Implementation de l'editeur de labyrinthe
* \author Equipe 5
* \version 1.0
*
*/

#include "Editor.h"
#include "Menu.h"
#include "ofMain.h"

static const std::string LABEL_MENU = "Retourner au menu [m]";
static const std::string LABEL_OPEN = "Ouvrir [o]";
static const std::string LABEL_SAVE_AS = "Enregistrer sous [s]";
static const std::string LABEL_SET_WALL = "Mur [w]";
static const std::string LABEL_SET_PASSAGE = "Passage [p]";
static const std::string LABEL_SET_BOULDER = "Rocher [r]";
static const std::string LABEL_SET_RED_POTION = "Potion rouge [1]";
static const std::string LABEL_SET_BLUE_POTION = "Potion bleue [2]";
static const std::string LABEL_SET_TREASURE = "Tresor [t]";
static const std::string LABEL_SET_TORCH = "Torche [c]";
static const std::string LABEL_SET_THEME = "Theme [h]";

const char* args[] = { "Haie", "Bunker", "Ciment", "Pierre" };
static const std::vector<std::string> THEME_LIST(args, args + 4);

Editor::Editor() : currentMaze(15, 15), selectStartPoint(-1, -1), selectEndPoint(-1, -1) {
	
	gui = new ofxUICanvas();

	scenegraph::orthographic::MazeBuilder builder(&currentMaze, &selectedSectors, &drawMode);
	scenegraph::MazeAssembler assembler;
	scene = assembler.assemble(builder);
	selectedSectors.push_back(currentMaze.getSector(0, 0));
}



Editor::~Editor() {
	delete gui;
}



void Editor::setup() {
	gui->clearWidgets();

	gui->addButton(LABEL_MENU, true);
	gui->addButton(LABEL_OPEN, true);
	gui->addButton(LABEL_SAVE_AS, true);
	gui->addSpacer(40, 1);
	gui->addButton(LABEL_SET_WALL, true);
	gui->addButton(LABEL_SET_PASSAGE, true);
	gui->addButton(LABEL_SET_BOULDER, true);
	gui->addButton(LABEL_SET_RED_POTION, true);
	gui->addButton(LABEL_SET_BLUE_POTION, true);
	gui->addButton(LABEL_SET_TREASURE, true);
	gui->addButton(LABEL_SET_TORCH, true);
	gui->addSpacer(40, 1);
	gui->addDropDownList(LABEL_SET_THEME, THEME_LIST);

	gui->autoSizeToFitWidgets();
	gui->setColorBack(ofxUIColor(25, 25, 25));
	ofAddListener(gui->newGUIEvent, this, &Editor::guiEvent);

	gfxSelector.setup();
}



Screen * Editor::update() {
	return nextScreen;
}



void Editor::draw() {
	//Réduire le viewport pour ne pas être sous le menu
	float w = ofGetViewportWidth();
	float h = ofGetViewportHeight();
	glViewport(gui->getRect()->getWidth(), 0, w - gui->getRect()->getWidth(), h);

	drawMode = scenegraph::orthographic::RENDER;
	scene->draw();
	
	glViewport(0, 0, w, h);

	ofSetColor(128, 128, 128, 128);
	ofRectangle select_rect(selectStartPoint, selectEndPoint);
	ofRect(select_rect.x, select_rect.y, select_rect.width, select_rect.height);
	
	ofSetColor(255, 255, 255);
}



void Editor::keyReleased(int key) {
	Screen::keyReleased(key);
	
	if(0 != nextScreen) return;
	
	if(key == 'm') {
		loadMenu();
	}

	if (key == 's') {
		saveAs();
	}

	if (key == 'o') {
		open();
	}
	
	if (key == 'w') {
		changeSector(Sector::WALL);
	}

	if (key == 'p') {
		changeSector(Sector::PASSAGE);
	}

	if (key == 'r') {
		changeSector(Sector::BOULDER);
	}

	if (key == 't') {
		changeSector(Sector::TREASURE);
	}

	if (key == 'c') {
		changeSector(Sector::LIGHT);
	}


	if(key == '1') {
		changeSector(Sector::RED_POTION);
	}

	if (key == '2') {
		changeSector(Sector::BLUE_POTION);
	}
	
	//Find range of selection
	ofVec2f min(1000, 1000);
	ofVec2f max(-1, -1);
	for(std::vector<Sector *>::iterator it = selectedSectors.begin(); it != selectedSectors.end(); ++it) {
		int x = (*it)->getMazePosX();
		int y = (*it)->getMazePosY();
		
		if(x < min.x) min.x = x;
		if(x > max.x) max.x = x;
		
		if(y < min.y) min.y = y;
		if(y > max.y) max.y = y;
	}
	
	int move_x = 0, move_y = 0;

	if (key == OF_KEY_DOWN && currentMaze.getNbSectorsV() - 1 > max.y) {
		++move_y;
	}

	if (key == OF_KEY_UP && 0 < min.y) {
		--move_y;
	}

	if (key == OF_KEY_RIGHT && currentMaze.getNbSectorsH() - 1 > max.x) {
		++move_x;
	}

	if (key == OF_KEY_LEFT && 0 < min.x) {
		--move_x;
	}
	
	for(std::vector<Sector *>::iterator it = selectedSectors.begin(); it != selectedSectors.end(); ++it) {
		Sector * new_selection = currentMaze.getSector((*it)->getMazePosX() + move_x,
													   (*it)->getMazePosY() + move_y);
		(*it) = new_selection;
	}
}

void Editor::windowResized(int w, int h) {
	gfxSelector.rebuildFramebuffer(w, h);
}

void Editor::mousePressed(int x, int y, int button) {
	selectStartPoint.x = x;
	selectStartPoint.y = y;
	selectEndPoint.x = x;
	selectEndPoint.y = y;
}

void Editor::mouseDragged(int x, int y, int button) {

	if(selectStartPoint.x > 0.5f && selectStartPoint.y > 0.5f) {
		selectEndPoint.x = x;
		selectEndPoint.y = y;
	}
}

void Editor::mouseReleased(int x, int y, int button) {
	//Ignorer le clic, car il est sur le menu
	if (gui->getRect()->inside(x, y)) {
		return;
	}

	selectEndPoint.x = x;
	selectEndPoint.y = y;
	
	//Render selection mode frame
	//Réduire le viewport pour ne pas être sous le menu
	float w = ofGetViewportWidth();
	float h = ofGetViewportHeight();
	glViewport(gui->getRect()->getWidth(), 0, w - gui->getRect()->getWidth(), h);

	drawMode = scenegraph::orthographic::SELECT;
	std::vector<Sector *> new_selection = gfxSelector.findSectors(ofRectangle(selectStartPoint, selectEndPoint), scene, &currentMaze);
	
	glViewport(0, 0, w, h);

	if (0 < new_selection.size()) {
		selectedSectors = new_selection;
	}
	
	
	selectStartPoint.x = selectStartPoint.y = selectEndPoint.x = selectEndPoint.y = -1;
}



void Editor::guiEvent(ofxUIEventArgs &e)
{
	if (e.getName() == LABEL_MENU && e.getButton()->getValue())
	{
		loadMenu();
	}
	else if (e.getName() == LABEL_OPEN && e.getButton()->getValue())
	{
		open();
	}
	else if (e.getName() == LABEL_SAVE_AS && e.getButton()->getValue())
	{
		saveAs();
	}
	else if (e.getName() == LABEL_SET_WALL && e.getButton()->getValue())
	{
		changeSector(Sector::WALL);
	}
	else if (e.getName() == LABEL_SET_PASSAGE && e.getButton()->getValue())
	{
		changeSector(Sector::PASSAGE);
	}
	else if (e.getName() == LABEL_SET_BOULDER && e.getButton()->getValue())
	{
		changeSector(Sector::BOULDER);
	}
	else if (e.getName() == LABEL_SET_RED_POTION && e.getButton()->getValue())
	{
		changeSector(Sector::RED_POTION);
	}
	else if (e.getName() == LABEL_SET_BLUE_POTION && e.getButton()->getValue())
	{
		changeSector(Sector::BLUE_POTION);
	}
	else if (e.getName() == LABEL_SET_TREASURE && e.getButton()->getValue())
	{
		changeSector(Sector::TREASURE);
	}
	else if (e.getName() == LABEL_SET_TORCH && e.getButton()->getValue())
	{
		changeSector(Sector::LIGHT);
	}
	else if (e.getName() == LABEL_SET_THEME)		
	{	
		ofxUIDropDownList *list = (ofxUIDropDownList *)e.widget;
		vector<ofxUIWidget *> &selectedTheme = list->getSelected();
		for (int i = 0; i < selectedTheme.size(); i++)	{
			currentTheme = selectedTheme[i]->getName();
		}
	}


}



void Editor::loadMenu()
{
	ofRemoveListener(gui->newGUIEvent, this, &Editor::guiEvent);
	gui->setVisible(false);

	nextScreen = new Menu();
}



void Editor::saveAs()
{
	ofFileDialogResult path = ofSystemSaveDialog("maze.txt", "Enregistrer sous...");
	if (path.bSuccess) {
		std::ofstream ofs(path.getPath().c_str());
		ofs << currentTheme<<endl;
		ofs << currentMaze; // store the object to file
		ofs.close();
	}
}



void Editor::open()
{
	ofFileDialogResult path = ofSystemLoadDialog("Ouvrir");
	char themeName[50];

	if (path.bSuccess) {
		std::ifstream ifs(path.getPath().c_str());
		ifs.getline(themeName,50);
		currentTheme = themeName;		
		ifs >> currentMaze; // read object from file
		ifs.close();
		selectedSectors.clear();
		selectedSectors.push_back(currentMaze.getSector(0, 0));
	}
}



void Editor::changeSector(Sector::Kind kind) {
	for(std::vector<Sector *>::iterator it = selectedSectors.begin(); it != selectedSectors.end(); ++it) {
		(*it)->setKind(kind);
	}
}