/**
* \file Menu.h
* \brief Implementation du menu principal
* \author Equipe 5
* \version 1.0
*
*/

#pragma once
#include "Screen.h"


/**
 * \brief Interface d'entrée dans l'application
 *
 * Permet de passer aux deux autres interfaces :
 * le jeu et l'éditeur. Il est possible de passer
 * au jeu en utilisant un labyrinthe aléatoire ou
 * un labyrinthe créé dans l'éditeur.
 */

class Menu :
public Screen
{
public:
	Menu() : gui(new ofxUICanvas())
	{}
	
	~Menu();
	
	virtual void setup();
	virtual void draw();
	
	/**
	 * \brief Met à jour l'état de l'interface
	 *
	 * \return Nouvelle interface, que ofApp devra adopter comme nouvel état. Nul s'il n'y a pas de changement.
	 */
	virtual Screen * update();
	
	virtual void keyReleased(int key);
	virtual void guiEvent(ofxUIEventArgs &e);
private:
	Screen * nextScreen = 0;  ///< Nouvelle interface à retourner à la prochaine mise à jour.
	ofxUICanvas * gui;

	void loadMaze();
	void loadCustomMaze();
	void loadEditor();
	void loadCredits();
	
	/// Bannière
	/// \todo refactoriser dans une classe imbriquée
	/// \todo mieux gérer le redimensionnement de fenêtre
	int nbBannerMazeSectorsH = 45;
	int nbBannerMazeSectorsV = 8;
	std::vector < std::vector< bool > > bannerMaze;
	float roll = 0;
	typedef std::vector < std::vector< bool > > BannerCells;
};

