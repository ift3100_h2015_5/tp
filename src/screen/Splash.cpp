/**
* \file Splash.cpp
* \brief Ecran de demarage de l'application
* \author Equipe 5
* \version 1.0
*
*/

#include "Splash.h"

Splash::Splash() {	
	gui = new ofxUICanvas();
	gui->clearWidgets();

	splashLSystem = new lSys::lSystem();
	splashTurtle = new lSys::tortoise();
}


Splash::~Splash() {
	delete gui;
	delete splashLSystem;
	delete splashTurtle;
}

void Splash::setup() {
	gui->setFont("uiAssets/Leo-Arrow.ttf");
	gui->setFontSize(OFX_UI_FONT_LARGE, 24); 
	gui->setFontSize(OFX_UI_FONT_MEDIUM, 12);
	gui->setFontSize(OFX_UI_FONT_SMALL, 10);
	gui->clearWidgets();
	gui->autoSizeToFitWidgets();
	gui->setColorBack(ofxUIColor(0, 0, 0));
	ofAddListener(gui->newGUIEvent, this, &Splash::guiEvent);

	cloud.loadImage("textures/cloud.png");

	splashTurtle->setX(ofGetViewportWidth() / 2);
	splashTurtle->setY(2 * ofGetViewportHeight() / 3);
	splashTurtle->setZ(0);
	splashTurtle->setColor(ofColor(0, 150, 50));
	growingTime = 0;
	treeDepth = 0;

	float randomRange = ofRandom(3); 
	if (randomRange <= 1){
		splashLSystem->addRule("F", "GG[-F][+F][&&&F][^^^F][\\\F][///F]");
		splashLSystem->setStart("GGGGGGF");
		splashTurtle->setAngle(30);
		splashTurtle->setLength(15);
		maxTreeDepth = 3;
		splashDuration = 7;
	}
	else if (randomRange <= 2){
		splashLSystem->addRule("X", "S[-FX][&&FX][^^FX][\\FX][//FX]+FX");
		splashLSystem->setStart("FX");
		splashTurtle->setAngle(45);
		splashTurtle->setLength(100);
		maxTreeDepth = 4;
		splashDuration = 10;
	}
	else{
		splashLSystem->addRule("F", "FF-[-F+F+F][^^F&F&F]+[+F-F-F][&&F^F^F]");
		splashLSystem->setStart("GGGGF[&&&&F][////F]");
		splashTurtle->setAngle(22.5);
		splashTurtle->setLength(12);
		maxTreeDepth = 2;
		splashDuration = 7;
	}

	splashSequence = splashLSystem->generateNthLevel(treeDepth);
	
	shader3.load("shaders/specialEffects/bNw");
	shader2.load("shaders/specialEffects/wave");
	shader.load("shaders/specialEffects/splash");
	addEffect(shader, splashDuration);
	addEffect(shader2, splashDuration);
	addEffect(shader3, splashDuration);
}



Screen * Splash::update() {
	double delta = ofGetLastFrameTime();
	
	growingTime += delta;
	int newDepth = growingTreeDepth(growingTime, splashDuration, maxTreeDepth);
	if (newDepth > treeDepth){
		treeDepth = newDepth;
		splashSequence = splashLSystem->generateNthLevel(treeDepth);
	}
	
	
	//Update effects
	//Remove expired ones
	for (std::vector<FullScreenEffect *>::iterator it = effects.begin(); it != effects.end();) {
		(*it)->update(delta);		
		if ((*it)->done()) {
			delete (*it);
			it = effects.erase(it);
		}
		else {
			++it;
		}
	}

	return nextScreen;	
}



void Splash::draw() {		
	ofBackground(ofColor(0));

	if (effects.size() > 0) {
		FullScreenEffect * effect = effects[0];
		effect->begin();
		drawSplash();
		effect->end();

		int ii = 1;
		while (ii < effects.size()) {
			FullScreenEffect * next_effect = effects[ii];

			next_effect->begin();
			effect->draw();
			next_effect->end();
			effect = next_effect;
			++ii;
		}

		effect->draw();
	}
	else {		
		loadMenu();
	}

	ofSetColor(200, 200, 200);	
	int title_y = 100;
	std::string title("EERIE TREE SOFTWARE PRESENTE");
	float title_width = gui->getFontMedium()->stringWidth(title);
	float title_height = gui->getFontMedium()->stringHeight(title);
	gui->getFontMedium()->drawString(title, ofGetViewportWidth() / 2 - title_width / 2, 6 * ofGetViewportHeight() / 7);

	
	ofSetColor(255, 255, 255);
	primitives::p_circle(ofGetViewportWidth() - 100, 100, 50);
	cloud.draw(ofGetViewportWidth() - 250 - growingTime * 150/splashDuration, 0, 250, 200);	
}

void Splash::addEffect(ofShader effect_shader, float duration_seconds) {
	FullScreenEffect * effect = new FullScreenEffect();
	effect->setup(effect_shader, duration_seconds);
	effects.push_back(effect);
}



void Splash::keyReleased(int key) {
	Screen::keyReleased(key);
	
	if(0 != nextScreen) return;
	
	loadMenu();	

}

void Splash::windowResized(int w, int h) {
	splashTurtle->setX(ofGetViewportWidth() / 2);
	splashTurtle->setY(2 * ofGetViewportHeight() / 3);

	for (std::vector<FullScreenEffect *>::iterator it = effects.begin(); it != effects.end(); ++it) {
		(*it)->rebuildFramebuffer(w, h);
	}
}

void Splash::mousePressed(int x, int y, int button) {
}

void Splash::mouseDragged(int x, int y, int button) {

}

void Splash::mouseReleased(int x, int y, int button) {
}



void Splash::guiEvent(ofxUIEventArgs &e)
{	
}



void Splash::loadMenu()
{
	ofRemoveListener(gui->newGUIEvent, this, &Splash::guiEvent);
	gui->setVisible(false);

	nextScreen = new Menu();
}



void Splash::drawSplash()
{	
	splashTurtle->setX(ofGetViewportWidth() / 2);
	splashTurtle->setY(2 * ofGetViewportHeight() / 3);
	splashTurtle->setZ(0);
	splashTurtle->draw(splashSequence);
}

int Splash::growingTreeDepth(float elapsedTime, float totalTime, int depth)
{
	float depthDuration = totalTime / (depth+1);
	int actualDepth = int(elapsedTime / depthDuration);	
	return actualDepth;
}




