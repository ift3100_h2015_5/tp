/**
* \file Credits.h
* \brief Afichage du generique et de son arriere plan (l-systeme)
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include "Screen.h"
#include "../graphics/Framebuffer.h"
#include "../graphics/FullScreenEffect.h"
#include "../lsys/lSystem.h"
#include "../lsys/tortoise.h"


/**
* \class Credits
* \brief Générique de l'application
*
* Affiche la liste des créateurs des l'application
* de même qu'une série de remerciements
* Un l-système est affiché en arrière plan pour
* rendre ça joli.
*/
class Credits :
public Screen {
public:

	/**
	* \brief Constructeur
	*/
	Credits();

	/**
	* \brief Destructeur
	*/
	~Credits();
	
	/**
	* \brief Dessine la page de du générique
	*/
	virtual void draw();
	
	/**
	 * \brief Met à jour l'état de l'interface
	 *
	 * \return Nouvelle interface, que ofApp devra adopter comme nouvel état. Nul s'il n'y a pas de changement.
	 */
	virtual Screen * update();

	/**
	* \brief Fait les réglagles initiaux pour la générique. 
	*/
	virtual void setup();
	
	virtual void keyReleased(int key);
	virtual void mousePressed(int x, int y, int button);
	virtual void mouseDragged(int x, int y, int button);
	virtual void mouseReleased(int x, int y, int button);
	virtual void windowResized(int w, int h);
	virtual void guiEvent(ofxUIEventArgs &e);
	
private:

	/**
	 * \brief Prépare à demander à ofApp de passer à l'interface Menu.
	 *
	 */
	void loadMenu();

	/**
	* \brief Méthode privée qui prend en charge une partie de l'affichage
	*/
	void drawcredits();

	/**
	* \brief Ajoute un effet provenant d'un shader.
	* \param effect_shader Shader à utiliser pour l'effet.
	* \param duration_seconds Durée désirée de l'effet en secondes.
	*/
	void addEffect(ofShader effect_shader, float duration_seconds);

	/**
	* \brief Mets à jour le l-système utilisé comme arrière plan
	*/
	void updateLSystem();

	/**
	* \brief Règle la longueur d'une section du l-système en fonction
	* de la taille du viewport.
	*/
	void setSegmentLength();

	bool resizeUpdate1;
	bool resizeUpdate2;
	Screen * nextScreen = 0; ///< Nouvelle interface à retourner à la prochaine mise à jour.		
	ofxUICanvas * gui;
	lSys::lSystem * creditsLSystem;
	lSys::tortoise * creditsTurtle;
	string creditsSequence="";
	int maxTreeDepth;
	float lsysPosXIni;
	float lsysPosYIni;

	float viewportW;
	float viewportH;
	float segmentLength;
	float correctionFactor;
	float textY;
	string title;
	float title_width;
	float title_height;


	//Effect shaders
	ofShader shader;

	Framebuffer * lsysFB; // Framebuffer stockant le dessin lsysteme
	FullScreenEffect * lsysEffect; 

};