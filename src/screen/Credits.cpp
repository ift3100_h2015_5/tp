/**
* \file Credits.cpp
* \brief Afichage du generique et de son arriere plan (l-systeme)
* \author Equipe 5
* \version 1.0
*
*/

#include "Credits.h"
#include "ofMain.h"
#include "Menu.h"


Credits::Credits():lsysFB(0), lsysEffect(0) {	
	gui = new ofxUICanvas();
	gui->clearWidgets();
	creditsLSystem = new lSys::lSystem();
	creditsTurtle = new lSys::tortoise();
}



Credits::~Credits() {
	delete gui;
	delete creditsLSystem;
	delete creditsTurtle;

	if (0 != lsysFB) {
		delete lsysFB;
	}

	if (0 != lsysEffect) {
		delete lsysEffect;
	}
}



void Credits::setup() {
	gui->clearWidgets();

	gui->setFont("uiAssets/Leo-Arrow.ttf");
	gui->setFontSize(OFX_UI_FONT_LARGE, 24);
	gui->setFontSize(OFX_UI_FONT_MEDIUM, 12);
	gui->setFontSize(OFX_UI_FONT_SMALL, 10);
	gui->autoSizeToFitWidgets();
	gui->setColorBack(ofxUIColor(0, 0, 0));
	ofAddListener(gui->newGUIEvent, this, &Credits::guiEvent);

	viewportW = ofGetViewportWidth();
	viewportH = ofGetViewportHeight();
	textY = viewportH;

    title = "EERIE TREE SOFTWARE\n\nune filiale de Equipe 5\n\n\nLANDRY Patrick\n\n\nMOREAU Francois\n\n\nRENAULD Pascal";
	title = title + "\n\n\n\nRemerciements\n\nNos proches\n\nPhilippe Voyer (+1 point?)\n\nOpenframeworks\n\nlearnopengl.com";
	title = title + "\n\nLa carte video de Francois\n\nTous les pokemons\n\n";
	title_width = gui->getFontMedium()->stringWidth(title);
	title_height = gui->getFontMedium()->stringHeight(title);

	creditsTurtle->setColor(ofColor(50));
	int lsysChoice = rand() % 3;	
	if (lsysChoice == 0)	{
		creditsLSystem->addRule("F", "FF-F-F-F-FF");
		creditsLSystem->setStart("F-F-F-F");
		creditsTurtle->setAngle(90);
		correctionFactor = 81.0;
		maxTreeDepth = 4;
		setSegmentLength();
	}
	else if (lsysChoice == 1)	{
		creditsLSystem->addRule("F", "FF-G-G+F+F-G-GF+G+FFG-F+G+FF+G-FG-G-F+F+GG-");
		creditsLSystem->addRule("G", "+FF-G-G+F+FG+F-GG-F-G+FGG-F-GF+F+G-G-F+F+GG");
		creditsLSystem->setStart("-G");
		creditsTurtle->setAngle(90);
		correctionFactor = 25.0;
		maxTreeDepth = 2;
		setSegmentLength();
	}
	else{
		creditsLSystem->addRule("F", "FF-F--F-F");
		creditsLSystem->setStart("F-F-F-F");
		creditsTurtle->setAngle(90);
		correctionFactor = 81.0;		
		maxTreeDepth = 4;
		setSegmentLength();
	}

	creditsSequence = creditsLSystem->generateNthLevel(maxTreeDepth);
	
	lsysFB = new Framebuffer(viewportW, viewportH);
	lsysFB->setup();
	shader.load("shaders/specialEffects/credits");
	lsysEffect = new FullScreenEffect();
	lsysEffect->setup(shader, -1);
	
	lsysFB->begin();
	drawcredits();
	lsysFB->end();
}



Screen * Credits::update() {

	if (resizeUpdate1){
		updateLSystem();
		resizeUpdate1 = false;
	}
	else if (resizeUpdate2)
	{
		updateLSystem();
		resizeUpdate2 = false;
	}
	return nextScreen;	
}



void Credits::draw() {	
	ofBackground(ofColor(0));	
	shader.begin();
	lsysFB->draw(shader);
	shader.end();

	ofSetColor(200, 200, 200);	
	gui->getFontMedium()->drawString(title, viewportW / 2 - title_width / 2, textY--);
	if (textY <= -title_height)
		loadMenu();
}


void Credits::keyReleased(int key) {
	Screen::keyReleased(key);

	if(0 != nextScreen) return;
	
	loadMenu();
}

void Credits::windowResized(int w, int h) {
	resizeUpdate1 = true;
	resizeUpdate2 = true;
}

void Credits::mousePressed(int x, int y, int button) {
}

void Credits::mouseDragged(int x, int y, int button) {

}

void Credits::mouseReleased(int x, int y, int button) {
}



void Credits::guiEvent(ofxUIEventArgs &e)
{	
}



void Credits::loadMenu()
{
	ofRemoveListener(gui->newGUIEvent, this, &Credits::guiEvent);
	gui->setVisible(false);

	nextScreen = new Menu();
}



void Credits::drawcredits()
{	
	float positionX = viewportW - (viewportW - segmentLength*correctionFactor) / 2;
	float positionY = viewportH - (viewportH - segmentLength*correctionFactor) / 2;
	
	creditsTurtle->setX(positionX);
	creditsTurtle->setY(positionY);
	creditsTurtle->setZ(0);
	creditsTurtle->draw(creditsSequence);
}

void Credits::updateLSystem(){
	setSegmentLength();

	if (0 != lsysFB) {
		delete lsysFB;
	}
	gui->setFontSize(OFX_UI_FONT_MEDIUM, 12);
	lsysFB = new Framebuffer(viewportW, viewportH);
	lsysFB->setup();

	lsysFB->begin();
	ofSetColor(255, 255, 255);
	drawcredits();
	lsysFB->end();
}

void Credits::setSegmentLength(){
	viewportW = ofGetViewportWidth();
	viewportH = ofGetViewportHeight();

	if (viewportW >= viewportH){
		segmentLength = viewportH / correctionFactor;
	}
	else {
		segmentLength = viewportW / correctionFactor;
	}
	creditsTurtle->setLength(segmentLength);
}




