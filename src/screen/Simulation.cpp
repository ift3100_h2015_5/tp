/**
* \file Simulation.cpp
* \brief Classe responsable de la gestion du labyrinthe en mode jeu
* \author Equipe 5
* \version 1.0
*
*/

#include "Simulation.h"
#include "Menu.h"
#include <stdio.h>


Simulation::Simulation(Maze * _maze, string theme) : player(1, 1), maze(_maze), compass(), drawMode(scenegraph::perspective::RENDER) {
	//Placer le joueur à côté de l'entrée
	player = Player(0.5f, 0.5f);
	Sector * entrance = maze->getEntrance();
	player.x = entrance->getMazePosX() + 1.0f;
	player.y = entrance->getMazePosY() + 0.0f;
	
	monster.x = maze->getNbSectorsH() / 2;
	monster.y = maze->getNbSectorsV() / 2;


	// Choix aleatoire de la texture
	if (theme == ""){
		int textureChoice = rand() % 4;
		
		if (textureChoice == 0)	{
			textureFolder = "textures/simulation/";
		}
		else if (textureChoice == 1){
			textureFolder = "textures/simulationBunker/";
		}
		else if (textureChoice == 2){
			textureFolder = "textures/simulationCement/";
		}
		else {
			textureFolder = "textures/simulationStone/";
		}
	}
	else
	{
		if (theme == "Haie")	{
			textureFolder = "textures/simulation/";
		}
		else if (theme == "Bunker"){
			textureFolder = "textures/simulationBunker/";
		}
		else if (theme == "Ciment"){
			textureFolder = "textures/simulationCement/";
		}
		else if (theme == "Pierre"){
			textureFolder = "textures/simulationStone/";
		}
	}
		
	scenegraph::perspective::MazeBuilder perspective_builder(maze, &player, &monster, &timeOfDay, &drawMode, &sectorClicks, &textureFolder);
	scenegraph::plane::MazeBuilder plane_builder(maze, &player, &monster);
	scenegraph::MazeAssembler assembler;
	scene = assembler.assemble(perspective_builder);
	map = assembler.assemble(plane_builder);
	gui = new ofxUICanvas();
	gui->clearWidgets();
	instructions.loadImage("uiAssets/directives_jeu.png");
	scroll.loadImage("textures/messageBox.png");
}

Simulation::~Simulation() {
	delete maze;
	delete gui;
}

void Simulation::setup() {
	glowEffect.setup();
	
	gui->clearWidgets();

	gui->autoSizeToFitWidgets();
	ofAddListener(gui->newGUIEvent, this, &Simulation::guiEvent);
	music.loadSound("soundtrack/01.mp3");
	music.setLoop(true);
	music.play();

	beginShader.load("shaders/specialEffects/drunk");
	teleportShader.load("shaders/specialEffects/wave");
	bluePotionShader.load("shaders/specialEffects/winter");
	redPotionShader.load("shaders/specialEffects/bNw");
	fShader.load("shaders/specialEffects/ray");
	gShader.load("shaders/specialEffects/edge");
	hShader.load("shaders/specialEffects/emboss");
	idShader.load("shaders/specialEffects/id");
	addEffect(beginShader, 10);

	gfxSelector.setup();
}



Screen * Simulation::update() {
	double delta = ofGetLastFrameTime();
	ofVec2f player_move(0.0f, 0.0f);

	try{
		//Update effects
		//Remove expired ones
		for (std::vector<FullScreenEffect *>::iterator it = effects.begin(); it != effects.end(); ) {
			(*it)->update(delta);

			if ((*it)->done()) {
				delete (*it);
				it = effects.erase(it);
			} else {
				++it;
			}
		}

		//GESTION DES ENTRÉES

		if (keyIsDown[OF_KEY_UP]) {
			//Le joueur avance
			ofVec2f direction = player.getDirection();
			player_move.x += direction.x * delta * PLAYER_WALK_SPEED;
			player_move.y += direction.y * delta * PLAYER_WALK_SPEED;
		}
		if (keyIsDown[OF_KEY_DOWN]) {
			//Le joueur recule
			ofVec2f direction = player.getDirection();
			player_move.x -= direction.x * delta * PLAYER_WALK_SPEED;
			player_move.y -= direction.y * delta * PLAYER_WALK_SPEED;
		}
		if (keyIsDown[OF_KEY_LEFT]) {
			//Le joueur effectue une rotation anti-horaire
			player.incrementAngle(-delta * PLAYER_ROTATE_SPEED);

		}
		if (keyIsDown[OF_KEY_RIGHT]) {
			//Le joueur effectue une rotation horaire
			player.incrementAngle(delta * PLAYER_ROTATE_SPEED);
		}

		double viewport_height = ofGetViewportHeight();
		double dist_y = ofGetMouseY() - viewport_height / 2.0f;
		double angle = ofMap(dist_y, -viewport_height / 2.0f, viewport_height / 2.0f, -PI / 2, PI / 2);
		player.setHead(angle);


		//GESTION DES COLLISIONS
		//Pour chaque axe de déplacement, on fait une copie du joueur,
		//on effectue le déplacement et ne permet le déplacement que
		//si la copie n'est pas entrée dans un mur.
		//Procéder un axe à la fois permet de "glisser" sur un mur
		//avec lequel on est en collision de biais.

		Player player_x_moved(player);
		player_x_moved.x += player_move.x;
		if (player_x_moved.x > 0 && player_x_moved.x < maze->getNbSectorsH()) {
			bool outOfBound = false;
			Sector * sector_x;
			try
			{
				sector_x = playerCollision(player_x_moved);
			}
			catch (const std::out_of_range& e)
			{
				outOfBound = true;
			}

			if (!outOfBound){
				if (0 == sector_x) {
					player = player_x_moved;
				}
				else if (sector_x->getKind() == Sector::EXIT) {
					ofRemoveListener(gui->newGUIEvent, this, &Simulation::guiEvent);
					gui->setVisible(false);
					return new Menu();
				}
			}
			else{
				drawMessage = true;
				currentMessage = "Vous ne voulez \nquand meme pas\nvous jetter dans le\nvide!";
			}
		}

		Player player_y_moved(player);
		player_y_moved.y += player_move.y;
		if (player_y_moved.y > 0 && player_y_moved.y < maze->getNbSectorsV()) {
			bool outOfBound = false;
			Sector * sector_y;
			try
			{
				sector_y = playerCollision(player_y_moved);
			}
			catch (const std::out_of_range& e)
			{
				outOfBound = true;
			}

			if (!outOfBound){
				Sector * sector_y = playerCollision(player_y_moved);
				if (0 == sector_y) {
					player = player_y_moved;
				}
				else if (sector_y->getKind() == Sector::EXIT) {
					ofRemoveListener(gui->newGUIEvent, this, &Simulation::guiEvent);
					gui->setVisible(false);
					return new Menu();
				}
			}	
			else
			{
				drawMessage = true;
				currentMessage = "Vous ne voulez \nquand meme pas\nvous jetter dans le\nvide!";
			}

		}

		Sector * sector_stuck = playerCollision(player);
		if (0 != sector_stuck) {
			throw std::logic_error("Player definitely stuck in wall.");
		}
	
		//Mettre à jour le monstre
		Monster::SectorsInRange seen_sectors;
		const std::vector<weather::CardinalDirection> & directions = weather::getCardinalDirections();

	
		for(std::vector<weather::CardinalDirection>::const_iterator it = directions.begin(); it != directions.end(); ++it) {
			ofVec2f seen_position;
			seen_position.x = monster.x + weather::vec(*it).first;
			seen_position.y = monster.y + weather::vec(*it).second;
			Sector * s = getSectorByPosition(seen_position);
			if(s != 0) {
				seen_sectors[*it] = s;
			}
		}
	
		monster.update(seen_sectors, ofGetLastFrameTime());



		//Collisions monstre/joueur
		ofRectangle rect1(player.x - player.w / 2.0f, player.y - player.h / 2.0f, player.w, player.h);
		ofRectangle rect2(monster.x - monster.w / 2.0f, monster.y - monster.h / 2.0f, monster.w, monster.h);
		if (rect1.x < rect2.x + rect2.width &&
			rect1.x + rect1.width > rect2.x &&
			rect1.y < rect2.y + rect2.height &&
			rect1.height + rect1.y > rect2.y) {

			// Touché par le montre! Le joueur est téléporté
			Sector * teleport_to = maze->getRandomPassableSector();
			player.x = teleport_to->getMazePosX();
			player.y = teleport_to->getMazePosY();

			drawMessage = true;
			currentMessage = "La creature vous a\nteleporte !";

			//Effet visuel accompagnant la téléportation
			addEffect(teleportShader, 3);
		}
	
		return 0;
	}
	catch (...)	{
		return new Menu();   // Retour au menu si une erreur imprevue survient.
	}
}


void Simulation::displayMessage()
{
	ofSetColor(255, 255, 255);

	scroll.drawSubsection(ofGetWidth() - 200, ofGetHeight() - 200, 100, 100, 0, 0); //top left
	scroll.drawSubsection(ofGetWidth() - 100, ofGetHeight() - 200, 100, 100, 200, 0); //top right

	scroll.drawSubsection(ofGetWidth() - 200, ofGetHeight() - 100, 100, 100, 0, 200); //bottom left
	scroll.drawSubsection(ofGetWidth() - 100, ofGetHeight() - 100, 100, 100, 200, 200); //bottom right
	
	ofSetColor(0);
	gui->getFontMedium()->drawString(currentMessage, ofGetWidth() - 180, ofGetHeight() - 150);
}

void Simulation::addEffect(ofShader effect_shader, float duration_seconds) {
	FullScreenEffect * effect = new FullScreenEffect();
	effect->setup(effect_shader, duration_seconds);
	effects.push_back(effect);
}


Sector * Simulation::playerCollision(const Player &a_player) {
	ofPoint corners[4] = {
		ofPoint(a_player.x - a_player.w/2, a_player.y - a_player.h/2),
		ofPoint(a_player.x + a_player.w/2, a_player.y - a_player.h/2),
		ofPoint(a_player.x + a_player.w/2, a_player.y + a_player.h/2),
		ofPoint(a_player.x - a_player.w/2, a_player.y + a_player.h/2)
	};
	for (int ii = 0; ii < 4; ++ii) {
		Sector * sector = getSectorByPosition(corners[ii]);
		if (!sector->isPassable()) {
			return sector;
		}
	}
	return 0;
}


void Simulation::draw() {
	if(isDrawingMap) {
		drawMap();
	} else {
		drawScene();

		ofSetColor(255, 255, 255);
		instructions.draw(10, 10);
		compass.draw(player);

		if (drawMessage)
		{
			++currentMessageTimeLength;
			displayMessage();
			if (currentMessageTimeLength == NB_FRAMES_DISPLAY_MESSAGE)
			{
				currentMessageTimeLength = 0;
				drawMessage = false;
				currentMessage = "";
			}
		}
	}
}

void Simulation::drawScene(Framebuffer * destination) {
	glowEffect.beginGlow();
	drawMode = scenegraph::perspective::GLOW;
	scene->draw();
	glowEffect.endGlow();

	glowEffect.beginScene();
	drawMode = scenegraph::perspective::RENDER;
	scene->draw();
	glowEffect.endScene();

	if (effects.size() > 0) {
		FullScreenEffect * effect = effects[0];

		effect->begin();
		glowEffect.draw();
		effect->end();

		int ii = 1;
		while (ii < effects.size()) {
			FullScreenEffect * next_effect = effects[ii];

			next_effect->begin();
			effect->draw();
			next_effect->end();

			effect = next_effect;

			++ii;
		}

		if (destination != 0) destination->begin();
		effect->draw();
		if (destination != 0) destination->end();
	}
	else {
		if (destination != 0) destination->begin();
		glowEffect.draw();
		if (destination != 0) destination->end();
	}
}

void Simulation::drawMap() {
	map->draw();
}

Sector * Simulation::getSectorByPosition(const ofVec2f & pos) {
	int sector_x = pos.x + 0.5f;
	int sector_y = pos.y + 0.5f;
	
	if(sector_x < 0 || sector_x >= maze->getNbSectorsH() ||
       sector_y < 0 || sector_y >= maze->getNbSectorsV()) {
		return 0;
	}
	
	return maze->getSector(sector_x, sector_y);
}



void Simulation::keyReleased(int key) {
	Screen::keyReleased(key);
	
	if(key == ' ') {
		isDrawingMap = !isDrawingMap;
	}

	if (key == 'c') {
		if (timeOfDay == weather::NIGHT) {
			timeOfDay = weather::DAY;
		}
		else {
			timeOfDay = weather::NIGHT;
		}
	}
	
	if(key == 'e' && !isDrawingMap) {
		takeScreenshot();
	}

	//Pour tester les effets spéciaux
	if (key == 'f') {
		drawMessage = true;
		currentMessage = "Effet:\nRayons de lumiere";
		addEffect(fShader, 15);
	}
	else if (key == 'g') {
		drawMessage = true;
		currentMessage = "Effet:\nDetection des\ncontours";
		addEffect(gShader, 15);
	}
	else if (key == 'h') {
		drawMessage = true;
		currentMessage = "Effet:\nEmbossage!";
		addEffect(hShader, 15);
	}

	if (key == '1') {
		drawMessage = true;
		currentMessage = "Attention\npeinture fraiche!";
		textureFolder = "textures/simulation/";
	}
	else if (key == '2') {
		drawMessage = true;
		currentMessage = "Attention\npeinture fraiche!";
		textureFolder = "textures/simulationBunker/";		
	}
	else if (key == '3') {
		drawMessage = true;
		currentMessage = "Attention\npeinture fraiche!";
		textureFolder = "textures/simulationCement/";
	}
	else if (key == '4') {
		drawMessage = true;
		currentMessage = "Attention\npeinture fraiche!";
		textureFolder = "textures/simulationStone/";
	}
}

void Simulation::takeScreenshot() {
	ofSetColor(255, 255, 255);
	
	int ss_width = ofGetViewportWidth();
	int ss_height = ofGetViewportHeight();
	
	Framebuffer screenshot(ss_width, ss_height);
	screenshot.setup();

	drawScene(&screenshot);

	int buffer_size = 4 * ss_width * ss_height;
	GLubyte * pixels = new GLubyte[buffer_size];
	
	screenshot.readPixels(0, 0, ss_width, ss_height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	
	ofImage ss_image;
	ss_image.setFromPixels(pixels, ss_width, ss_height, OF_IMAGE_COLOR_ALPHA);
	ss_image.mirror(true, false);
	
	ofFileDialogResult path = ofSystemSaveDialog("capture.png", "Enregistrer sous...");
	if (!path.bSuccess) {
		delete []pixels;
		return;
	}
	//ss_image.grabScreen(0, 0, ss_width, ss_height);
	ss_image.saveImage(path.getPath());
	delete []pixels;
}

void Simulation::mouseMoved(int x, int y) {}

void Simulation::windowResized(int w, int h) {
	//Attention : durant l'exécution de cette fonction,
	// w n'égale pas encore ofGetViewportWidth() et
	// h n'égale pas encore ofGetViewportHeight().
	gfxSelector.rebuildFramebuffer(w, h);
	
	glowEffect.rebuildFramebuffers(w, h);
	
	float currw = ofGetViewportWidth();
	float currh = ofGetViewportHeight();

	for (std::vector<FullScreenEffect *>::iterator it = effects.begin(); it != effects.end(); ++it) {
		(*it)->rebuildFramebuffer(w, h);
	}
}

void Simulation::mouseReleased(int x, int y, int button) {
	//Render selection mode frame
	drawMode = scenegraph::perspective::SELECT;

	Sector * sector = gfxSelector.findSector(x, y, scene, maze);

	if (0 == sector) return;
	
	if(sectorClicks.find(sector) == sectorClicks.end()) {
		sectorClicks[sector] = 1;
	} else {
		++sectorClicks[sector];
	}
	
	unsigned int nb_clicks = sectorClicks[sector];
	
	if(sector->getKind() == Sector::RED_POTION) {
		drawMessage = true;
		
		if(1 == nb_clicks) {
			currentMessage = "Une potion ! Hmmm...\nL'essayer ? ou ne\npas l'essayer ?";
		} else if(2 == nb_clicks) {
			currentMessage = "Hmmm... Bonheur !\nVous vous sentez mieux.\nVous avez bien fait\nde boire cette potion !";
			addEffect(bluePotionShader, 15);
		} else {
			currentMessage = "C'est vide.";
		}
	}
		
	if(sector->getKind() == Sector::BLUE_POTION) {
		drawMessage = true;
		
		if(1 == nb_clicks) {
			currentMessage = "Une potion ! Hmmm...\nL'essayer ? ou ne\npas l'essayer ?";
		} else if(2 == nb_clicks) {
			currentMessage = "Potion bleue : votre\nvision est troublee.\nVous n'auriez peut-etre\npas du la prendre...";
			addEffect(redPotionShader, 15);
		} else {
			currentMessage = "C'est vide.";
		}
	}
	
	if(sector->getKind() == Sector::TREASURE) {
		drawMessage = true;
		
		if(1 == nb_clicks) {
			currentMessage = "Un tresor! Empochez\nvite ces gemmes.";
		} else if(2 == nb_clicks) {
			ostringstream msg;
			msg << "Cela doit bien valoir\n" << rand() % 50 + 50 << " pieces d'or.";
			currentMessage = msg.str();
		} else {
			currentMessage = "C'est vide.";
		}
	}
}


void Simulation::guiEvent(ofxUIEventArgs &e)
{

}

void Simulation::Compass::draw(const Player & player) {
	//Boussole
	float width = ofGetViewportWidth();
	ofPushMatrix();
	ofTranslate(width - 50, 50); //centre de la boussole

	//arriere plan
	ofSetColor(250, 250, 250, 50);
	primitives::p_rectangle(-50, -50, 50, 50);
	//ofRect(-50, -50, 100, 100);
	ofSetColor(255, 255, 255, 50);
	primitives::p_circle(0, 0, 50);
	//ofEllipse(0, 0, 100, 100);

	//points cardinaux
	ofSetColor(255);
	ofDrawBitmapString("N", -4, -40);
	ofDrawBitmapString("S", -4, 50);
	ofDrawBitmapString("O", -50, 5);
	ofDrawBitmapString("E", 43, 5);

	ofMultMatrix(player.getOrientation().getMatx());

	//aiguille noire
	ofSetColor(0);
	primitives::p_line(0, 0, 0, -35);
	//ofLine(0, 0, 0, -35);

	//pointe rouge
	ofSetColor(125, 0, 0);
	primitives::p_triangle(-5, -30, 5, -30, 0, -45);
	//ofTriangle(-5, -30, 5, -30, 0, -45);
	ofPopMatrix();
}
