/**
* \file Simulation.h
* \brief Classe responsable de la gestion du labyrinthe en mode jeu
* \author Equipe 5
* \version 1.0
*
*/

#pragma once
#include "Screen.h"
#include "../maze/Maze.h"
#include "simulationEntities/Player.h"
#include "simulationEntities/Monster.h"
#include "../scenegraph/perspective.h"
#include "../scenegraph/plane.h"
#include "../graphics/FullScreenEffect.h"
#include "../graphics/GraphicSelector.h"
#include "../graphics/GlowEffect.h"

/**
 * Interface de jeu
 *
 * Est responsable de capter les entrées utilisateurs
 * et de mettre à jour l'état du jeu en conséquence.
 * Délègue le rendu du jeu aux graphes de scènes
 * appropriés.
 *
 * UNITÉS DE MESURE
 * L'unité de mesure de base est le secteur de labyrinthe,
 * considéré comme un cube. Chaque secteur est centré sur
 * sa position, c'est-à-dire que le secteur à la position
 * (2, 3) occupe un rectangle dont le coin supérieur
 * gauche est (1.5, 2.5) et le coin inférieur droit (2.5, 3.5).
 *
 */
class Simulation :
	public Screen
{
public:
	Simulation(Maze *, string theme="");
	~Simulation();

	virtual void draw();
	virtual void setup();
	
	/**
	 * \brief Met à jour l'état de l'interface
	 *
	 * \return Nouvelle interface, que ofApp devra adopter comme nouvel état. Nul s'il n'y a pas de changement.
	 */
	virtual Screen * update();
	virtual void keyReleased(int key);
	virtual void mouseMoved(int x, int y);
	virtual void mouseReleased(int x, int y, int button);
	virtual void windowResized(int w, int h);
	
	virtual void guiEvent(ofxUIEventArgs &e);

private:
	static const int PLAYER_WALK_SPEED = 3; ///< En secteurs par seconde
	static const int PLAYER_ROTATE_SPEED = PI; ///< En radians
	
	static const int NB_FRAMES_DISPLAY_MESSAGE = 300;

    Player player;
	Monster monster;
	Maze * maze;
	std::map<Sector *, unsigned int> sectorClicks;
	scenegraph::perspective::MODE drawMode;


	
	scenegraph::NodePtr scene; ///< Graphe de scène, rendu première personne
	scenegraph::NodePtr map; ///< Graphe de scène, rendu carte
	
	bool isDrawingMap = false; ///< Doit-on dessiner la carte?
	
	void addEffect(ofShader effect_shader, float duration_seconds);
	
	void displayMessage();
	bool drawMessage = true;
	string currentMessage = "Vous vous reveillez.\nLa porte derriere vous\nest verrouillee.";
	int currentMessageTimeLength = 0;

	/**
	 * \brief Recherche des secteurs non traversables en collision
	 *        avec un joueur hypothétique. Sert à valider le déplacement du joueur.
	 *
	 * \param a_player Une copie du joueur représentant un déplacement.
	 *
	 * \return Un secteur non traversable en collision avec le joueur, s'il se déplace. 0 s'il n'y en a pas.
	 */
	Sector * playerCollision(const Player &a_player);
	
	/**
	 * \brief Dessiner la scène en première personne.
	 */
	void drawScene(Framebuffer * destination = 0);
	
	/**
	 * \brief Dessiner la carte 2D exclusivement.
	 *
	 */
	void drawMap();
	
	void takeScreenshot();
	
	Sector * getSectorByPosition(const ofVec2f & pos);

	ofxUICanvas * gui;
	ofSoundPlayer music; ///< Trame sonore
	ofImage instructions; ///< Instructions
	weather::TimeOfDay timeOfDay = weather::NIGHT; ///<Période du jour. Influence le graphe de scène en première personne.
	string textureFolder = "textures/simulation/";
	
	ofImage scroll;
	
	/**
	 * \brief Boussole dessinée lors du déplacement en première personne et indiquant l'orientantion du joueur.
	 *
	 */
	class Compass {
	public:
		void draw(const Player & player);
	};
	Compass compass;

	//Effects currently running
	std::vector<FullScreenEffect *> effects;

	//Effect shaders
	ofShader beginShader;
	ofShader teleportShader;
	ofShader bluePotionShader;
	ofShader redPotionShader;
	ofShader fShader;
	ofShader gShader;
	ofShader hShader;
	ofShader idShader;

	GlowEffect glowEffect;

	GraphicSelector gfxSelector; ///< Détecteur de secteur cliqué
};

