/**
* \file Screen.h
* \brief Declaration de Screen.
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include "../ofxUi/ofxUI.h"

/**
 * \brief Classe abstraite dont hériter pour définir une interface de ofApp.
 */

class Screen
{
public:
	Screen();
	virtual ~Screen() {}

	virtual void draw() = 0;
	
	/**
	 * \brief Met à jour l'état de l'interface
	 *
	 * \return Nouvelle interface, que ofApp devra adopter comme nouvel état. Nul s'il n'y a pas de changement.
	 */
	virtual Screen * update() = 0;
	virtual void setup() = 0;

	
	
	/**
	 * \brief Stocke la touche qui vient d'être appuyée comme une touche tenue.
	 *
	 * Convivialité offerte au sous-classes. Cela permet de consulter
	 * Quelles touches sont présentement tenues dans update().
	 */
	virtual void keyPressed(int key);
	
	
	
	/**
	 * \brief Retire la touche des touches considérées comme tenues.
	 *
	 */
	virtual void keyReleased(int key);
	
	
	
	virtual void mouseMoved(int x, int y) {}
	virtual void mousePressed(int x, int y, int button) {}
	virtual void mouseDragged(int x, int y, int button) {}
	virtual void mouseReleased(int x, int y, int button) {}
	virtual void windowResized(int w, int h) {}
	virtual void guiEvent(ofxUIEventArgs &e) = 0;

protected:
	bool keyIsDown[512]; ///< Touches présentement tenues

private:
	ofxUICanvas *gui;
};

