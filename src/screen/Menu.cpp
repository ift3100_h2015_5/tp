/**
* \file Menu.cpp
* \brief Implementation du menu principal
* \author Equipe 5
* \version 1.0
*
*/

#include "Menu.h"
#include "ofMain.h"
#include "../maze/Maze.h"
#include "Simulation.h"
#include "Editor.h"
#include "Credits.h"

static const std::string LABEL_PLAY = "Labyrinthe aleatoire [a]";
static const std::string LABEL_PLAY_CUSTOM = "Ouvrir un labyrinthe [o]";
static const std::string LABEL_EDITOR = "Editeur de labyrinthes [e]";
static const std::string LABEL_CREDITS = "Credits [c]";

Menu::~Menu() {
	delete gui;
}

Screen * Menu::update() {
	float sector_size = ((float)ofGetViewportWidth()) / ((float)nbBannerMazeSectorsH);
	gui->setPosition(ofGetViewportWidth() / 2 - gui->getRect()->getHalfWidth(), (nbBannerMazeSectorsV+2)*sector_size);
	return nextScreen;
}

void Menu::setup() {	
	//Banner Maze
	srand(time(0));
	bannerMaze.resize(nbBannerMazeSectorsH+1);
	for (BannerCells::iterator it = bannerMaze.begin(); it != bannerMaze.end(); ++it) {
		(*it).resize(nbBannerMazeSectorsV);
		for (std::vector< bool >::iterator it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
			int randomval = rand() % 2;
			(*it2) = (randomval == 1);
		}
	}

	//GUI

	gui->setFont("uiAssets/Leo-Arrow.ttf");
	gui->setFontSize(OFX_UI_FONT_LARGE, 24); //These call are optional, but if you want to resize the LARGE, MEDIUM, and SMALL fonts, here is how to do it.
	gui->setFontSize(OFX_UI_FONT_MEDIUM, 12);
	gui->setFontSize(OFX_UI_FONT_SMALL, 10); //SUPER IMPORTANT NOTE: CALL THESE FUNTIONS BEFORE ADDING ANY WIDGETS, THIS AFFECTS THE SPACING OF THE GUI

	gui->addImageButton(LABEL_PLAY, "uiAssets/location68.png", true, 50, 50);
	gui->addLabel(LABEL_PLAY, OFX_UI_FONT_SMALL);
	gui->addSpacer(1, 25)->setToggleColor(false);

	gui->addImageButton(LABEL_PLAY_CUSTOM, "uiAssets/folder256.png", true, 50, 50);
	gui->addLabel(LABEL_PLAY_CUSTOM, OFX_UI_FONT_SMALL);
	gui->addSpacer(1, 25)->setToggleColor(false);

	gui->addImageButton(LABEL_EDITOR, "uiAssets/screwdriver26.png", true, 50, 50);
	gui->addLabel(LABEL_EDITOR, OFX_UI_FONT_SMALL);
	gui->addSpacer(1, 25)->setToggleColor(false);

	gui->addImageButton(LABEL_CREDITS, "uiAssets/credits.png", true, 50, 50);
	gui->addLabel(LABEL_CREDITS, OFX_UI_FONT_SMALL);

	gui->autoSizeToFitWidgets();
	float sector_size = ((float)ofGetViewportWidth()) / ((float)nbBannerMazeSectorsH);
	gui->setPosition(ofGetViewportWidth() / 2 - gui->getRect()->getHalfWidth(), (nbBannerMazeSectorsV + 2)*sector_size);
	
	gui->setColorBack(ofxUIColor(0, 0, 0));

	ofAddListener(gui->newGUIEvent, this, &Menu::guiEvent);
}

void Menu::draw() {
	ofBackground(ofColor(0));

	//banner maze
	float sector_size = ((float)ofGetViewportWidth()) / ((float)nbBannerMazeSectorsH);
	int x = 0;
	int y = 0;

	int title_y = 100;
	std::string title("MEANDRES");
	float title_width = gui->getFontLarge()->stringWidth(title);
	float title_height = gui->getFontLarge()->stringHeight(title);

	ofPushMatrix();
	ofSetColor(ofColor(128));
	
	float elapsed = ofGetLastFrameTime();
	roll += elapsed * 10.0f;
	while (roll > sector_size) {
		roll -= sector_size;
		//new column
		bannerMaze.erase(bannerMaze.end()-1);
		std::vector< bool > col;
		col.resize(nbBannerMazeSectorsV);
		for (std::vector< bool >::iterator it = col.begin(); it != col.end(); ++it) {
			int randomval = rand() % 2;
			(*it) = (randomval == 1);
		}
		bannerMaze.insert(bannerMaze.begin(), col);
	}


	ofTranslate(roll - sector_size, title_y - title_height / 2 - sector_size * nbBannerMazeSectorsV / 2, 0);
	ofScale(sector_size, sector_size);


	for (BannerCells::iterator it = bannerMaze.begin(); it != bannerMaze.end(); ++it) {
		for (std::vector< bool >::iterator it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
			ofPoint start, end;
			if ((*it2)) {
				start = ofPoint(x, y);
				end =   ofPoint((x+1), (y+1));
			}
			else {
				start = ofPoint((x+1), y);
				end =   ofPoint(x, (y+1));
			}
			ofLine(start, end);
			++y;
		}
		y = 0;
		++x;
	}
	ofPopMatrix();

	ofSetColor(ofColor(255));
	gui->getFontLarge()->drawString(title, ofGetViewportWidth() / 2 - title_width / 2, title_y);
}


void Menu::keyReleased(int key) {
	if(0 != nextScreen) return;
	
	switch (key)
	{
	case 'a':
		loadMaze();
		break;
	case 'e':
		loadEditor();
		break;
	case 'o':
		loadCustomMaze();
		break;
	case 'c':
		loadCredits();
		break;
	}
}

void Menu::guiEvent(ofxUIEventArgs &e)
{
	if (e.getName() == LABEL_PLAY && e.getButton()->getValue())
	{
		loadMaze();
	}
	else if (e.getName() == LABEL_PLAY_CUSTOM && e.getButton()->getValue()) {
		loadCustomMaze();
	}
	else if (e.getName() == LABEL_EDITOR && e.getButton()->getValue())
	{
		loadEditor();
	}
	else if (e.getName() == LABEL_CREDITS && e.getButton()->getValue())
	{
		loadCredits();
	}
}

void Menu::loadMaze()
{
	Maze * maze = new Maze(15, 15);  //Dimension du labyrinthe, en secteurs

	ofRemoveListener(gui->newGUIEvent, this, &Menu::guiEvent);
	gui->setVisible(false);

	nextScreen = new Simulation(maze);
}

void Menu::loadCustomMaze()
{
	ofFileDialogResult path = ofSystemLoadDialog("Ouvrir");
	char themeName[50];

	if (path.bSuccess) {
		Maze * maze = new Maze(15, 15);
		
		std::ifstream ifs(path.getPath().c_str());
		ifs.getline(themeName, 50);
		string theme = themeName;
		ifs >> *maze; // read object from file
		ifs.close();

		nextScreen = new Simulation(maze, theme);
	}
}

void Menu::loadEditor()
{
	ofRemoveListener(gui->newGUIEvent, this, &Menu::guiEvent);
	gui->setVisible(false);

	nextScreen = new Editor();
}

void Menu::loadCredits()
{
	ofRemoveListener(gui->newGUIEvent, this, &Menu::guiEvent);
	gui->setVisible(false);

	nextScreen = new Credits();
}