/**
* \file Splash.h
* \brief Ecran de demarage de l'application
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include "Screen.h"
#include "../lsys/lSystem.h"
#include "../lsys/tortoise.h"
#include "Menu.h"
#include "../graphics/FullScreenEffect.h"
#include "../ofxPrimitives/ofxPrimitives.h"

/**
* \class Splash
* \brief Écran de démarrage
*
*/
class Splash :
public Screen {
public:
	/**
	* \brief Constructeur
	*/
	Splash();
	/**
	* \brief Destructeur
	*/
	~Splash();
	
	/**
	* \brief Dessine la page de l'écrand de démarrage
	*/
	virtual void draw();
	
	/**
	 * \brief Met à jour l'état de l'interface
	 *
	 * \return Nouvelle interface, que ofApp devra adopter comme nouvel état. Nul s'il n'y a pas de changement.
	 */
	virtual Screen * update();

	/**
	* \brief Fait les réglagles initiaux pour la générique.
	*/
	virtual void setup();
	
	virtual void keyReleased(int key);
	virtual void mousePressed(int x, int y, int button);
	virtual void mouseDragged(int x, int y, int button);
	virtual void mouseReleased(int x, int y, int button);
	virtual void windowResized(int w, int h);
	virtual void guiEvent(ofxUIEventArgs &e);
	
private:

	/**
	 * \brief Prépare à demander à ofApp de passer à l'interface Menu.
	 *
	 */
	void loadMenu();
	void drawSplash();
	void addEffect(ofShader effect_shader, float duration_seconds);
	int growingTreeDepth(float elapsedTime, float totalTime, int depth);

	Screen * nextScreen = 0; ///< Nouvelle interface à retourner à la prochaine mise à jour.		
	ofxUICanvas * gui;
	lSys::lSystem * splashLSystem;
	lSys::tortoise * splashTurtle;
	string splashSequence;
	float growingTime;
	int treeDepth;
	int maxTreeDepth;
	float splashDuration;

	ofImage cloud;
	
	//Effects currently running
	std::vector<FullScreenEffect *> effects;

	//Effect shaders
	ofShader shader;
	ofShader shader2;
	ofShader shader3;
	

};