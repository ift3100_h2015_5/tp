/**
 * \file ofApp.h
 * \brief Déclaration de ofApp
 */
#pragma once

#include "ofMain.h"
#include "screen/Screen.h"


/**
 * \brief L'application
 *
 * Reçoit toutes les entrées utilisateurs et en délègue la gestion
 * à son membre currentScreen, qui joue le rôle d'état de l'application,
 * c'est-à-dire d'interface présentement active.
 */
class ofApp : public ofBaseApp {
	private:
		Screen * currentScreen; ///< Interface active

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		void guiEvent(ofxUIEventArgs &e);

		void exit();
};
