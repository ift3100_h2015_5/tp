/**
* \file weather.cpp
* \brief Pour la gestion du jour et de la nuit
* \author Equipe 5
* \version 1.0
*
*/
#include "weather.h"

namespace weather {

CardinalDirection opposite(CardinalDirection d) {
	if(NORTH == d) return SOUTH;
	if(EAST == d)  return WEST;
	if(SOUTH == d) return NORTH;
	if(WEST == d)  return EAST;
};

std::pair<int, int> vec(CardinalDirection d) {
	if(NORTH == d) return std::pair<int, int>(0, -1);
	if(EAST == d)  return std::pair<int, int>(1, 0);
	if(SOUTH == d) return std::pair<int, int>(0, 1);
	if(WEST == d)  return std::pair<int, int>(-1, 0);
}


const std::vector< CardinalDirection > & getCardinalDirections() {
	static std::vector< CardinalDirection > directions;

	if (directions.size() == 0) {
		directions.push_back(weather::NORTH);
		directions.push_back(weather::EAST);
		directions.push_back(weather::WEST);
		directions.push_back(weather::SOUTH);
	}

	return directions;
}
	
}