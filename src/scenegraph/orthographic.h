/**
* \file orthographic.h
* \brief Déclaration des classes pour le rendu global d'un labyrinthe en caméra orthographique
* \author Equipe 5
* \version 1.0
*
*/

#pragma once
#include "abstract.h"
#include "../maze/Maze.h"

namespace scenegraph {

/**
 * \brief Regroupe les classes du graphe de scène pour rendu orthographique.
 */
namespace orthographic {
	
	enum MODE { RENDER, SELECT };
		
	/**
	 * \brief Assure le rendu de la scène.
	 */
	class SceneNode : public scenegraph::Node {
	public:
		SceneNode(Maze *, MODE * mode);
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
	private:
		Maze * maze;
		MODE * mode;
		ofShader renderShader;
		ofShader selectShader;
		
		/**
		 * \brief Calcule la matrice de projection.
		 *
		 * Le calcul est fait de sorte à inclure le labyrinthe dans son ensemble,
		 * quel que soit sa dimension. On trouve la sphère minimale englobant le
		 * labyrinthe, puis on créer un frustum minimal englobant la sphère.
		 *
		 * https://www.opengl.org/archives/resources/faq/technical/viewing.htm#view0070
		 */
		ofMatrix4x4 computeProjection();
	};
	
		
	
	/**
	 * \brief Assure le rendu du labyrinthe.
	 */
	class MazeNode : public scenegraph::Node {
	public:
		MazeNode(Maze *);
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
	private:
		Maze * maze;
		ofMatrix4x4 model;
		const float rotX = -20.0f; ///< Rotation appliquée afin de mettre en évidence la hauteur des blocs.
		const float rotY = 5.0f;  ///< Rotation appliquée afin de mettre en évidence la hauteur des blocs.
	};

	
	
	/**
	 * \brief Assure le rendu d'une rangée de secteur.
	 */
	class SectorRowNode : public scenegraph::Node {
	public:
		SectorRowNode(int _position);
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
	private:
		int position;
		ofMatrix4x4 model;
	};

	
	
	/**
	 * \brief Assure le rendu d'un secteur.
	 *
	 * Chaque obstacle est rendu comme un cube.
	 * Chaque passage est rendu comme un plancher carré.
	 * Le secteur sélectioné est rendu en bleu.
	 */
	class SectorNode : public scenegraph::Node {
	public:
		SectorNode(Sector *, int position, std::vector<Sector *> * selected_sectors, MODE * mode);
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
	private:
		Sector * sector;
		ofMatrix4x4 model;
		std::vector<Sector *> * selectedSectors;
		MODE * mode;
		void bindIf(ofImage & texture);
		void unbindIf(ofImage & texture);

		const static GLfloat wallVertices[180];
		const static GLfloat passageVertices[30];
		
		static bool isReady;
		static GLuint vaoWall;
		static GLuint vaoFloor;
		static ofImage wallSkin;
		static ofImage floorSkin;
		static ofImage entranceSkin;
		static ofImage exitSkin;
		static ofImage shapesSkin;
		static ofImage redPotionSkin;
		static ofImage bluePotionSkin;
		static ofImage treasureSkin;
		static ofImage torchSkin;
		
		/**
		 * \brief Si ce n'est pas déjà fait, initialise les membres statiques de SectorNode.
		 *
		 * Les membres statiques incluent des Vertex Array Object à envoyer au GPU et des textures.
		 */
		static void prepare();
	};
	
	
	
	/**
	 * \brief Fabrique les noeuds du graphe pour ce rendu.
	 */
	class MazeBuilder : public scenegraph::MazeBuilder {
	public:
		MazeBuilder(Maze * _maze, std::vector<Sector *> * selected_sectors, MODE * mode);
		
		virtual NodePtr buildScene();
		virtual NodePtr buildMaze();
		virtual NodePtr buildSectorRow(int x);
		virtual NodePtr buildSector(int x, int y);
	private:
		std::vector<Sector *> * selectedSectors;
		MODE * mode;
	};

} }