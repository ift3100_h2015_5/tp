/**
* \file orthographic.cpp
* \brief Implémentation des classes pour le rendu global d'un labyrinthe en caméra orthographique
* \author Equipe 5
* \version 1.0
*
*/
#include "orthographic.h"
#include "../ColorIDManager.h"

namespace scenegraph { namespace orthographic {

	///////////////////
	// SceneNode
	///////////////////

	SceneNode::SceneNode(Maze * _maze, MODE * _mode) : maze(_maze), mode(_mode) {
		ofDisableArbTex();
		renderShader.load("shaders/orthographic/shader");
		selectShader.load("shaders/orthographic/shader.vert", "shaders/orthographic/select.frag");
	}
	
	
	
	ofMatrix4x4 SceneNode::computeProjection() {
		//Trouver le frustum qui va contenir le modèle.
		//La dimension d'un secteur est de 1.0f.

		float wf = maze->getNbSectorsH();
		float hf = maze->getNbSectorsV();

		float a = sqrt(
			pow(wf / 2, 2) +
			pow(hf / 2, 2)
			);

		float boundingSphereDiam = sqrt(
			pow(a, 2) +
			0.25f //(wallsize / 2)^2
			);

		ofVec3f boundingSphereCenter(
			wf / 2.0f - 0.5f,
			hf / 2.0f - 0.5f,
			0.5f);


		GLdouble left = boundingSphereCenter.x - boundingSphereDiam;
		GLdouble right = boundingSphereCenter.x + boundingSphereDiam;
		GLdouble bottom = boundingSphereCenter.y - boundingSphereDiam;
		GLdouble top = boundingSphereCenter.y + boundingSphereDiam;

		//Considérer l'aspect du viewport
		ofRectangle viewport = ofGetCurrentViewport();
		GLfloat aspect = viewport.width / viewport.height;
		if (aspect < 1.0) {
			// window taller than wide
			bottom /= aspect; top /= aspect;
		} else { 
			left *= aspect;
			right *= aspect;
		}

		ofMatrix4x4 projection;
		projection.makeOrthoMatrix(left, right, bottom, top, -1000.0f, 1000.0f);
		return projection;
	}

	
	
	void SceneNode::draw(const ofMatrix4x4 &parent_model, ofShader *) {
		if (*mode == RENDER) {
			glClearColor(0.05f, 0.05f, 0.075f, 1.0f);
		}

		if (*mode == SELECT) {
			glClearColor(0, 0, 1, 1);
		}
		
		glClear(GL_COLOR_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		ofMatrix4x4 view;
		view.translate(0.0f, 0.0f, 5.0f);
		ofMatrix4x4 projection = computeProjection();

		ofShader * shader;
		if (*mode == SELECT) {
			shader = &selectShader;
		}

		if (*mode == RENDER) {
			shader = &renderShader;
		}

		shader->begin();
		shader->setUniformMatrix4f("view", view);
		shader->setUniformMatrix4f("projection", projection);
		Node::draw(parent_model, shader);
		shader->end();
		
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
	}
	
	
	
	///////////////////
	// MazeNode
	///////////////////

	MazeNode::MazeNode(Maze * _maze) : maze(_maze) {
		float wf = maze->getNbSectorsH();
		float hf = maze->getNbSectorsV();

		model.translate(-(wf / 2.0f - 0.5f), -(hf / 2.0f - 0.5f), 0.0);
		model.rotate(rotX, 1.0f, 0.0f, 0.0f);
		model.rotate(rotY, 0.0f, 1.0f, 0.0f);
		model.translate(wf / 2.0f - 0.5f, hf / 2.0f - 0.5f, 0.0);
	}
	
	

	void MazeNode::draw(const ofMatrix4x4 & parent_model, ofShader * shader) {
		ofMatrix4x4 new_model = model * parent_model;
		Node::draw(model, shader);
	}
	
	
	
	///////////////////
	// SectorRowNode
	///////////////////

	SectorRowNode::SectorRowNode(int _position) : position(_position) {
		model.translate(1.0f * position, 0.0f, 0.0f);
	}

	
	
	void SectorRowNode::draw(const ofMatrix4x4 &parent_model, ofShader * shader) {
		ofMatrix4x4 new_model = model * parent_model;
		Node::draw(new_model, shader);
	}


	
	///////////////////
	// SectorNode
	///////////////////

	bool SectorNode::isReady = false;
	GLuint SectorNode::vaoWall  = 0;
	GLuint SectorNode::vaoFloor = 0;
	ofImage SectorNode::wallSkin;
	ofImage SectorNode::floorSkin;
	ofImage SectorNode::entranceSkin;
	ofImage SectorNode::exitSkin;
	ofImage SectorNode::shapesSkin;
	ofImage SectorNode::redPotionSkin;
	ofImage SectorNode::bluePotionSkin;
	ofImage SectorNode::treasureSkin;
	ofImage SectorNode::torchSkin;
	
	const GLfloat SectorNode::passageVertices[30] = {
		//back face ok
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
		0.5f, -0.5f, -0.5f, 1.0f, 0.0f,
		0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
		0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f
	};

	const GLfloat SectorNode::wallVertices[180] = {
		//vertices              //tex coords

		//back face ok
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
		0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
		0.5f, -0.5f, -0.5f, 1.0f, 0.0f,
		0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f,

		//front face ok
		-0.5f, -0.5f, 0.5f, 0.0f, 0.0f,
		0.5f, -0.5f, 0.5f, 1.0f, 0.0f,
		0.5f, 0.5f, 0.5f, 1.0f, 1.0f,
		0.5f, 0.5f, 0.5f, 1.0f, 1.0f,
		-0.5f, 0.5f, 0.5f, 0.0f, 1.0f,
		-0.5f, -0.5f, 0.5f, 0.0f, 0.0f,

		//left face ok
		-0.5f, 0.5f, 0.5f, 1.0f, 1.0f,
		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
		-0.5f, -0.5f, 0.5f, 1.0f, 0.0f,
		-0.5f, 0.5f, 0.5f, 1.0f, 1.0f,

		//right face ok
		0.5f, 0.5f, 0.5f, 1.0f, 0.0f,
		0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
		0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
		0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
		0.5f, 0.5f, 0.5f, 1.0f, 0.0f,
		0.5f, -0.5f, 0.5f, 0.0f, 0.0f,

		//top face ok
		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f,
		0.5f, 0.5f, 0.5f, 1.0f, 0.0f,
		0.5f, 0.5f, -0.5f, 1.0f, 1.0f,
		0.5f, 0.5f, 0.5f, 1.0f, 0.0f,
		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f,
		-0.5f, 0.5f, 0.5f, 0.0f, 0.0f,

		//bottom face ok
		-0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
		0.5f, -0.5f, -0.5f, 1.0f, 1.0f,
		0.5f, -0.5f, 0.5f, 1.0f, 0.0f,
		0.5f, -0.5f, 0.5f, 1.0f, 0.0f,
		-0.5f, -0.5f, 0.5f, 0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f, 0.0f, 1.0f
	};

	
	
	void SectorNode::prepare() {
		if (isReady) return;

		wallSkin.loadImage("textures/editorSectors/ortho_wall.png");
		floorSkin.loadImage("textures/editorSectors/ortho_floor.png");
		entranceSkin.loadImage("textures/editorSectors/ortho_entrance.png");
		exitSkin.loadImage("textures/editorSectors/ortho_exit.png");
		shapesSkin.loadImage("textures/editorSectors/ortho_boulder.jpg");
		redPotionSkin.loadImage("textures/editorSectors/ortho_red_potion.png");
		bluePotionSkin.loadImage("textures/editorSectors/ortho_blue_potion.png");
		treasureSkin.loadImage("textures/editorSectors/ortho_treasure.png");
		torchSkin.loadImage("textures/editorSectors/ortho_torch.png");

		GLuint vertexbuffer;

		//WALL

		glGenVertexArrays(1, &vaoWall);
		glGenBuffers(1, &vertexbuffer);

		glBindVertexArray(vaoWall);

		//Envoyer les données
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(wallVertices), wallVertices, GL_STATIC_DRAW);

		//Indiquer quoi faire de ces données
		// Position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		// TexCoord attribute
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);

		//FLOOR

		glGenVertexArrays(1, &vaoFloor);
		glGenBuffers(1, &vertexbuffer);

		glBindVertexArray(vaoFloor);

		//Envoyer les données
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(passageVertices), passageVertices, GL_STATIC_DRAW);

		//Indiquer quoi faire de ces données
		// Position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		// TexCoord attribute
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);

		glBindVertexArray(0);

		isReady = true;
	}

	
	
	SectorNode::SectorNode(Sector * _sector, int position, std::vector<Sector *> * selected_sectors, MODE * _mode) :
		sector(_sector),
		selectedSectors(selected_sectors),
		mode(_mode) {
		prepare();
		model.translate(0.0, 1.0f * position, 0.0f);
	}
	
	void SectorNode::draw(const ofMatrix4x4 &parent_model, ofShader * shader) {
		ofMatrix4x4 new_model = model * parent_model;
	
		ofVec3f color_effect;

		if (*mode == RENDER) {
			//En mode rendu, l'effet de couleur est utilisé
			//pour indiquer le secteur sélectionné
			if (std::find(selectedSectors->begin(), selectedSectors->end(), sector) != selectedSectors->end()) {
				color_effect = ofVec3f(0.0f, 0.0f, 1.0f);
			}
			else {
			 color_effect = ofVec3f(1.0f, 1.0f, 1.0f);
			}
		}
		
		if (*mode == SELECT) {
			//En mode sélection, l'effet de couleur est utilisé
			//pour différencier de façon unique chaque secteur.
			ColorIDManager color_id;
			color_effect = color_id.getID(sector);
		}
		
		shader->setUniform3fv("color_effect", color_effect.getPtr());

		if (sector->getKind() == Sector::WALL) {
			glBindVertexArray(vaoWall);
			bindIf(wallSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			unbindIf(wallSkin);
			glBindVertexArray(0);
		}
		else if (sector->getKind() == Sector::PASSAGE) {
			glBindVertexArray(vaoFloor);
			bindIf(floorSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			unbindIf(floorSkin);
			glBindVertexArray(0);
		}
		else if (sector->getKind() == Sector::LIGHT) {
			glBindVertexArray(vaoFloor);
			bindIf(torchSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			unbindIf(torchSkin);
			glBindVertexArray(0);
		}
		else if (sector->getKind() == Sector::ENTRANCE) {
			glBindVertexArray(vaoWall);
			bindIf(entranceSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			unbindIf(entranceSkin);
			glBindVertexArray(0);
		}
		else if (sector->getKind() == Sector::EXIT) {
			glBindVertexArray(vaoWall);
			bindIf(exitSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			unbindIf(exitSkin);
			glBindVertexArray(0);
		}
		else if (sector->getKind() == Sector::BOULDER) {
			glBindVertexArray(vaoWall);
			bindIf(shapesSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			unbindIf(shapesSkin);
			glBindVertexArray(0);
		}
		else if (sector->getKind() == Sector::RED_POTION) {
			glBindVertexArray(vaoWall);
			bindIf(redPotionSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			unbindIf(redPotionSkin);
			glBindVertexArray(0);
		}
		else if (sector->getKind() == Sector::BLUE_POTION) {
			glBindVertexArray(vaoWall);
			bindIf(bluePotionSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			unbindIf(bluePotionSkin);
			glBindVertexArray(0);
		}
		else if (sector->getKind() == Sector::TREASURE) {
			glBindVertexArray(vaoWall);
			bindIf(treasureSkin);
			shader->setUniformMatrix4f("model", new_model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			unbindIf(treasureSkin);
			glBindVertexArray(0);
		}
		
		Node::draw(new_model, shader);
	}
	
	
	
	void SectorNode::bindIf(ofImage & texture) {
		if(*mode == RENDER) {
			texture.getTextureReference().bind();
		}
	}
	
	
	
	void SectorNode::unbindIf(ofImage & texture) {
		if(*mode == RENDER) {
			texture.getTextureReference().unbind();
		}
	}
	
	///////////////////
	// MazeBuilder
	///////////////////
	
	MazeBuilder::MazeBuilder(Maze * _maze, std::vector<Sector *> * selected_sectors, MODE * _mode) :
		scenegraph::MazeBuilder(_maze),
		selectedSectors(selected_sectors),
		mode(_mode)
	{ }
	
	NodePtr MazeBuilder::buildScene()
	{ return NodePtr(new SceneNode(maze, mode)); }
	
	NodePtr MazeBuilder::buildMaze()
	{ return NodePtr(new MazeNode(maze)); }
	
	NodePtr MazeBuilder::buildSectorRow(int position)
	{ return NodePtr(new SectorRowNode(position)); }
	
	NodePtr MazeBuilder::buildSector(int x, int y)
	{
		Sector * the_sector = maze->getSector(x, y);
		int position = maze->getNbSectorsV() - 1 - y;
		return NodePtr(new SectorNode(the_sector, position, selectedSectors, mode));
	}

} }