/**
* \file perspective.h
* \brief Déclaration des classes pour le rendu en première personne
* \author Equipe 5
* \version 1.0
*
*/
#pragma once
#include "abstract.h"
#include "ofMain.h"
#include "../maze/Maze.h"
#include "../ofxPrimitives/ofxPrimitives.h"
#include "../screen/simulationEntities/Player.h"
#include "../screen/simulationEntities/Monster.h"
#include "../weather.h"
#include "../graphics/OBJWaveFrontLoader.h"
#include "../ColorIDManager.h"
#include "../graphics/CoonsHermiteSurfaceMesh.h"

namespace scenegraph {

/**
 * \brief Regroupe les classes du graphe de scène pour rendu en première personne.
 */
namespace perspective {
	
	const GLuint TANGENT_ATTRIBUTE = 10;
	const GLuint BITANGENT_ATTRIBUTE = 11;

	enum MODE { RENDER, SELECT, GLOW };

	/**
	 * \brief Assure le rendu de la scène.
	 *
	 * Est responsable de placer la caméra selon la position, l'orientation
	 * et la position de la tête du joueur. Est responsable de choisir
	 * le bon shader selon le temps (membre timeOfDay).
	 */
	class SceneNode : public scenegraph::Node {
	public:
		SceneNode(Maze *, Player *, Monster *, std::vector<Sector *> lights, weather::TimeOfDay *, MODE *, string *);
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
	private:
		class Skybox {
		public:
			Skybox(Player * _player, std::string * _textureFolder);
			~Skybox();
			void draw(const ofMatrix4x4 & projection);
		private:
			Player * player;
			weather::TimeOfDay timeOfDay;
			GLuint skyboxVAO;
			GLuint skyBoxTextureID;
			ofShader skyboxShader;
			string textureFolder;
		};

		void drawMonster(const ofMatrix4x4 & projection, ofShader * shader, float rotate);

		Player * player;
		weather::TimeOfDay * timeOfDay; ///< Influence le shader à utiliser.
		std::vector<Sector *> lights;
		MODE * mode;
		string * textureFolder;

		//Monstre
		OBJWaveFrontLoader monsterModel;
		OBJWaveFrontLoader glowMonsterModel;
		Monster * monster;

		// Rendu de nuit

		ofShader nightShader;
		ofVec3f nightAmbientLight = ofVec3f(0.2f, 0.2f, 0.2f);
		Skybox nightSkybox;

		//Lumière ponctuelle du joueur
		ofVec3f nightPlayerPtLight = ofVec3f(1.0f, 1.0f, 0.8f);
		GLfloat nightPlayerPtLightRange = 4.0f;

		//Lumière directionnelle (clair de lune)
		ofVec3f nightDirLight = ofVec3f(0.3f, 0.3f, 0.5f);
		ofVec3f nightDirLightDir = ofVec3f(0, 1, 0);

		//Lumières ponctuelles (torches)
		ofVec3f nightPtLight = ofVec3f(1, 1, 1);
		GLfloat nightPtLightRange = 1.4f;

		//Lumières projecteurs (yeux du monstre)
		static const int NB_PROJECTORS = 3;
		ofVec3f nightProjLight = ofVec3f(1, 0.5, 0.5);
		GLfloat nightProjLightCone = 64; ///< Plus grand nombre = plus petit cône
		GLfloat nightProjLightRange = 1.2f;
		ofVec3f nightProjLightDirs[NB_PROJECTORS];

		//Rendu de jour

		ofShader dayShader;
		ofVec3f dayDiffuseLight = ofVec3f(0.3f, 0.3f, 0.3f);
		ofVec3f fogColor = ofVec3f(0.5f, 0.5f, 0.5f);

		ofShader selectShader;
	};
	
	
	
	/**
	 * \brief Assure le rendu du labyrinthe
	 */
	class MazeNode : public scenegraph::Node {
	public:
		

		/**
		* \brief Si ce n'est pas déjà fait, initialise les membres statiques de SectorNode.
		*
		* Les membres statiques incluent des textures.
		*/
		static void prepare();

		/**
		 * \brief Met à jour les maillages dynamiques partagés entre les secteurs.
		 *
		 * Doit être appelé une seule fois par rendu de la scène, par le SceneNode.
		 */
		static void update();
		
		MazeNode(Maze *, MODE *);
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
	private:
		static ofImage flagEntranceSkin;
		static ofImage flagExitSkin;
		static ofImage poleSkin;
		
		static CoonsHermiteSurfaceMesh flagMesh;
		
		Maze * maze;
		ofMatrix4x4 model;
		MODE * mode;
		
		
		/**
		 * \brief Active une texture, à condition que le rendu courant ne soit pas pour fins de sélection.
		 */
		void bindIf(ofImage & texture);
		
		/**
		 * \brief Désactive une texture, à condition que le rendu courant ne soit pas pour fins de sélection.
		 */
		void unbindIf(ofImage & texture);
		
		/**
		 * \brief Dessine un glorieux drapeau à partir d'une surface de Coons formée à partir de quatre courbes d'Hermite.
		 *
		 * «De gueules à la croix d'or chargée de cinq coquilles d'azur et cantonnée de seize alérions d'argent.»
		 *
		 * \param new_model Matrice modèle du secteur.
		 * \param shader Shader actif.
		 * \param sector Secteur dont origine le drapeau (type EXIT ou ENTRANCE)
		 */
		void drawFlag(const ofMatrix4x4 & new_model, ofShader * shader, Sector * sector);
	};
	
	
	
	/**
	 * \brief Assure le rendu d'une rangée de secteur
	 */
	class SectorRowNode : public scenegraph::Node {
	public:
		SectorRowNode(int _position);
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
	private:
		int position;
		ofMatrix4x4 model;
	};
	
	
	
	/**
	 * \brief Assure le rendu d'un secteur.
	 *
	 * Seuls les secteurs PASSAGE et BOULDER sont rendus.
	 * Un secteur PASSAGE est rendu en dessiner un plancher, ainsi
	 * qu'un mur à chaque côté où c'est approprié.
	 * Un BOULDER est rendu de la même façon qu'un PASSAGE, en plus
	 * d'un cube.
	 */
	class SectorNode : public scenegraph::Node {
	public:
		SectorNode(Sector * sector, Sector * north, Sector * east, Sector * south, Sector * west, MODE * mode, std::map<Sector *, unsigned int> * sectorClicks);
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);

		/**
		* \brief Si ce n'est pas déjà fait, initialise les membres statiques de SectorNode.
		*
		* Les membres statiques incluent des Vertex Array Object à envoyer au GPU et des textures.
		*/
		static void prepare(string texture_folder);

	private:
		Sector * sector;
		Sector * north;
		Sector * east;
		Sector * south;
		Sector * west;
		MODE * mode;		
		std::map<Sector *, unsigned int> * sectorClicks;

		ofMatrix4x4 model;
		
		/**
		 * \brief Dessine un carré avec texture au sol et pour chaque mur entourant ce secteur.
		 *
		 * \param parent_model Matrice modèle du secteur.
		 * \param shader Shader actif.
		 */
		void drawSurroundingWalls(const ofMatrix4x4 & parent_model, ofShader * shader);

		/**
		 * \brief Dessine un rocher sous la forme d'un cube avec texture.
		 *
		 * \param new_model Matrice modèle du secteur.
		 * \param shader Shader actif.
		 */
		void drawBoulder(const ofMatrix4x4 & new_model, ofShader * shader);

		/**
		 * \brief Dessine un coffre ou/et une potion, selon les actions précédentes du joueur.
		 *
		 * - Si aucun clic n'a eu lieu sur le coffre, ne dessiner que le coffre. (Coffre sélectionnable)
		 * - Si un clic a eu lieu sur le coffre, dessiner le coffre et la potion. (Potion sélectionnable)
		 * - SI un clic a eu lieu sur la potion, ne dessiner que le coffre. (Coffre sélectionnable)
		 *
		 * \param new_model Matrice modèle du secteur.
		 * \param shader Shader actif.
		 */
		void drawPotion(const ofMatrix4x4 & new_model, ofShader * shader);
		
		void drawTreasure(const ofMatrix4x4 & new_model, ofShader * shader);

		void drawChest(const ofMatrix4x4 & new_model, ofShader * shader);
		
		/**
		 * \brief Active une texture, à condition que le rendu courant ne soit pas pour fins de sélection.
		 */
		void bindIf(ofImage & texture);

		/**
		* \brief Désactive une texture, à condition que le rendu courant ne soit pas pour fins de sélection.
		*/
		void unbindIf(ofImage & texture);
		
		static GLfloat wallsVertices[420];

		static GLuint vaoWall;
		
		static ofImage wallSkin;
		static ofImage wallNormalMap;
		static ofImage floorSkin;
		static ofImage floorNormalMap;

		static ofImage entranceSkin;
		static ofImage entranceNormalMap;
		static ofImage exitSkin;
		static ofImage exitNormalMap;
		static ofImage shapesSkin;
		
		static ofImage blueGemSkin;
		static ofImage redGemSkin;
		static ofImage greenGemSkin;
		static ofImage purpleGemSkin;
		
		
		static OBJWaveFrontLoader chestModel;
		static OBJWaveFrontLoader redPotionModel;
		static OBJWaveFrontLoader bluePotionModel;
		static ColorIDManager colorIDManager; ///< Utilisé pour déterminer la couleur de rendu d'un élément cliquable
		

		

		static void genTangents(GLfloat * vertices, int nb_verts, int stride);
	};
	
	
	
	/**
	 * \brief Fabrique les noeuds du graphe pour ce rendu.
	 */
	class MazeBuilder : public scenegraph::MazeBuilder {
	public:
		MazeBuilder(Maze * _maze, Player * _player, Monster * _monster, weather::TimeOfDay * _timeOfDay, MODE *, std::map<Sector *, unsigned int> * sectorClicks, string * _textureFolder);
		
		virtual NodePtr buildScene();
		virtual NodePtr buildMaze();
		virtual NodePtr buildSectorRow(int x);
		virtual NodePtr buildSector(int x, int y);
	private:
		Player * player;
		Monster * monster;
		weather::TimeOfDay * timeOfDay;
		MODE * mode;
		std::map<Sector *, unsigned int> * sectorClicks;
		string * textureFolder;
	};
	
} }