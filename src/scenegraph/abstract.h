/**
* \file abstract.cpp
* \brief Déclarations des classes abstraites utilisées dans la déclaration de graphes de scène.
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include <vector>
#include <memory>
#include "ofMain.h"
#include "../maze/Maze.h"

/**
 * Regroupe les graphes de scène, tous types de rendus confondus.
 */
namespace scenegraph {
	
	/**
	 * \brief Représente un noeud dans le graphe de scène.
	 *
	 * Un noeud du graphe de scène peut avoir des enfants,
	 * eux-mêmes de type Node. Il s'agit ici du pattern Composite.
	 */
	class Node {
	public:
		
		/**
		 * \brief Se dessiner sans fournir de matrice ou de shader.
		 * 
		 * Une matrice identité sera utilisée.
		 */
		virtual void draw();
		
		/**
		 * \brief Se dessiner en tenant compte de la matrices modèle du parent, à l'aide d'un shader.
		 *
		 * \param parent_model Matrice de modèle du nœud parent. 
		 * \param shader Pointeur sur le shader à utiliser.
		 */
		virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
		
		/**
		 * \brief Ajouter un enfant au noeud.
		 *
		 * Cette méthode ne devrait être appellée que par une instance d'Assembler.
		 */
		void addChild(std::shared_ptr< Node > child);
	
	protected:
		std::vector< std::shared_ptr< Node > > children; ///< Vecteur des enfants. Allocation mémoire gérée par pointeurs intelligents.

		/**
		 * \brief Ajoute la matrice "model" au shader, ainsi que la matrice "normal_matx", calculée à partir de l'autre.
		 */
		void bindModelMatx(const ofMatrix4x4 & model, ofShader * shader);
	};

	typedef std::shared_ptr< Node > NodePtr;
	
	
	
	/**
	 * \brief Fabricant d'instances de Node à passer à une instance MazeAssembler.
	 *
	 * Classe abstraite qui représente un fabricant 
	 * d'instances de Node sous-classées
	 * pour faire le rendu dans un contexte particulier.
	 */
	class MazeBuilder {
	public:
		MazeBuilder(Maze * _maze) : maze(_maze) {}

		virtual NodePtr buildScene() = 0;
		virtual NodePtr buildMaze() = 0;
		virtual NodePtr buildSectorRow(int x) = 0;
		virtual NodePtr buildSector(int x, int y) = 0;
		
		virtual int nbSectorRows()
		{ return maze->getNbSectorsH(); }
		
		virtual int nbSectorsPerRow()
		{ return maze->getNbSectorsV(); }
	protected:
		Maze * maze;
	};
	
	
	
	/**
	 * \brief Assemble le graphe de scène d'un labyrinthe.
	 *
	 * Le graphe de scène fera un rendu différent selon
	 * le builder utilisé.
	 */
	class MazeAssembler {
	public:
		NodePtr assemble(MazeBuilder & builder);
	};
	
}
