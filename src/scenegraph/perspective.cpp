/**
* \file perspective.cpp
* \brief Implémentation des classes pour le rendu en première personne
* \author Equipe 5
* \version 1.0
*
*/
#include "perspective.h"
#include "../graphics/GlowEffect.h"

namespace scenegraph { namespace perspective {

	///////////////////
	// SceneNode
	///////////////////
	
	SceneNode::SceneNode(Maze * maze, Player * _player, Monster * _monster, std::vector<Sector *> _lights, weather::TimeOfDay * _timeOfDay, MODE * _mode, string * _textureFolder) :
		player(_player), monster(_monster), lights(_lights), nightSkybox(_player, _textureFolder), timeOfDay(_timeOfDay), mode(_mode), textureFolder(_textureFolder) {

		ofDisableArbTex();
		MazeNode::prepare();
		SectorNode::prepare(*_textureFolder);

		nightShader.load("shaders/perspective/night");
		nightShader.bindAttribute(TANGENT_ATTRIBUTE, "tangent");
		nightShader.bindAttribute(BITANGENT_ATTRIBUTE, "bitangent");
		nightShader.linkProgram();

		dayShader.load("shaders/perspective/day");
		selectShader.load("shaders/perspective/day.vert", "shaders/perspective/select.frag");
		monsterModel.load("models/claydol/Claydol.wavefront");
		glowMonsterModel.load("models/glow_claydol/Claydol.wavefront");

		nightProjLightDirs[0] = ofVec3f(cos(0), 0, sin(0));
		nightProjLightDirs[1] = ofVec3f(cos(2 * PI / 3), 0, sin(2 * PI / 3));
		nightProjLightDirs[2] = ofVec3f(cos(4 * PI / 3), 0, sin(4 * PI / 3));
		
	}
	
	
	
	void SceneNode::draw(const ofMatrix4x4 &parent_model, ofShader *) {
		MazeNode::update(); ///< Mettre à jour les maillages dynamiques des secteurs
		ofPoint camera_position((float)player->x, 0.0f, (float)player->y);

		//Calculer la matrice de projection
		//selon l'aspect courant du viewport
		ofRectangle viewport = ofGetCurrentViewport();
		GLfloat aspect = viewport.width / viewport.height;
		ofMatrix4x4 projection;
		projection.makePerspectiveMatrix(45.0f, aspect, 0.1f, 100.0f);
		
		ofMatrix4x4 view;
		//view.makeIdentityMatrix();
		view = nosmaths::matr4x4translation(view, -(float)player->x, 0.0f, -(float)player->y);

		//Player::getOrientation() nous retourne un quaternion représentant
		//l'orientation du joueur comme s'il se déplaçait sur le plan (*, *, 0),
		//alors qu'en 3D, il se déplace plutôt sur le plan(*, 0, *).
		//On doit donc opérer une conversion.
		nosmaths::Quaternion conversion(PI / 2.0f, ofVec3f(1, 0, 0));
		nosmaths::Quaternion orientation = conversion.inverse() * player->getOrientation() * conversion;
		ofMatrix4x4 player_rotate = nosmaths::matr4x4multiplication(orientation.getMatx(), player->getHead().getMatx());
		view = nosmaths::matr4x4multiplication(view, player_rotate);

		float monster_rotate = ofGetElapsedTimef() * 100;

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		
		if (*mode == SELECT) {
			glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			selectShader.begin();
			selectShader.setUniformMatrix4f("view", view);
			selectShader.setUniformMatrix4f("projection", projection);
			selectShader.setUniform1i("do_texture", 0);
			
			Node::draw(parent_model, &selectShader);
			selectShader.end();

		}
		else if(*mode == GLOW) {
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);
			
			int w = ofGetViewportWidth();
			int h = ofGetViewportHeight();
			
			glViewport(0, 0, w * GlowEffect::viewportRatio, h * GlowEffect::viewportRatio);
			
			selectShader.begin();
			selectShader.setUniformMatrix4f("view", view);
			selectShader.setUniformMatrix4f("projection", projection);
			
			selectShader.setUniform1i("do_texture", 0);
			selectShader.setUniform3f("select_color", 0, 0, 0);
			
			drawMonster(parent_model, &selectShader, monster_rotate);
			Node::draw(parent_model, &selectShader);
			selectShader.end();
			
			glViewport(0, 0, w, h);
		}
		else if (*timeOfDay == weather::NIGHT) {
			glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			nightSkybox.draw(projection);

			//Dessiner la scène par dessus le skybox

			//Dépendre de ofShader::bindDefaults() pour lier les attributs du Vertex Array Buffer
			//à l'entrée du Vertex Shader. Je rencontrais des problèmes lorsque je tentais
			//de les redéfinir. Il semble impossible de désactiver ofShader::bindDefaults().

			nightShader.begin();
			
			nightShader.setUniform1i("do_normalmap", 0);

			nightShader.setUniform1i("do_flat_color", 0);
			nightShader.setUniform3fv("flat_color", nightPtLight.getPtr());

			nightShader.setUniform3fv("camera_position", camera_position.getPtr());
			
			//Lumière ponctuelle du joueur
			nightShader.setUniform3fv("player_pt_light_pos", camera_position.getPtr());

			double time = ofGetElapsedTimeMillis();
			double intensity = ofMap(sin(time / 100.0f), -1.0f, 1.0f, 0.7f, 1.0f);
			nightShader.setUniform3fv("player_pt_light_color", (nightPlayerPtLight * intensity).getPtr());

			nightShader.setUniform1f("player_pt_light_range", nightPlayerPtLightRange);
			
			//Lumière directionnelle Clair de lune
			nightShader.setUniform3fv("directional_light_color", nightDirLight.getPtr());
			nightShader.setUniform3fv("directional_light_dir", nightDirLightDir.getPtr());

			//Lumière ambiante
			nightShader.setUniform3fv("ambientLight", nightAmbientLight.getPtr());
			
			
			//Lumières torches
			int ii = 0, jj = 0, kk = 0, mm = 0, nn = 0;
			/// \todo réutiliser le même espace mémoire à chaque frame
			GLfloat * torches_pos = new GLfloat[lights.size() * 3];
			GLfloat * torches_color = new GLfloat[lights.size() * 3];
			GLfloat * torches_range = new GLfloat[lights.size()];

			for(std::vector<Sector *>::iterator it = lights.begin(); it != lights.end(); ++it) {
				torches_pos[ii++] = (*it)->getMazePosX();
				torches_pos[ii++] = 0;
				torches_pos[ii++] = (*it)->getMazePosY();
				
				torches_color[jj++] = nightPtLight.x;
				torches_color[jj++] = nightPtLight.y;
				torches_color[jj++] = nightPtLight.z;
				
				torches_range[kk++] = nightPtLightRange;
			}
			
			nightShader.setUniform3fv("pt_lights_pos", torches_pos, lights.size());
			nightShader.setUniform3fv("pt_lights_color", torches_color, lights.size());
			nightShader.setUniform1fv("pt_lights_range", torches_range, lights.size());
			
			delete[] torches_pos;
			delete[] torches_color;
			delete[] torches_range;

			//Lumières projecteur
			ii = jj = kk = mm = nn = 0;
			/// \todo réutiliser le même espace mémoire à chaque frame
			GLfloat * projectors_pos = new GLfloat[NB_PROJECTORS * 3];
			GLfloat * projectors_dir = new GLfloat[NB_PROJECTORS * 3];
			GLfloat * projectors_color = new GLfloat[NB_PROJECTORS * 3];
			GLfloat * projectors_range = new GLfloat[NB_PROJECTORS];
			GLfloat * projectors_cone = new GLfloat[NB_PROJECTORS];

			ofMatrix4x4 rot;
			rot = nosmaths::matr4x4rotationY(rot, monster_rotate);

			///Remplir les paramètres
			for (int proj_counter = 0; proj_counter < NB_PROJECTORS; ++proj_counter) {
				projectors_pos[ii++] = monster->x;
				projectors_pos[ii++] = 0;
				projectors_pos[ii++] = monster->y;

				ofVec3f dir = rot * nightProjLightDirs[proj_counter];

				projectors_dir[jj++] = dir.x;
				projectors_dir[jj++] = dir.y;
				projectors_dir[jj++] = dir.z;

				projectors_color[kk++] = nightProjLight.x;
				projectors_color[kk++] = nightProjLight.y;
				projectors_color[kk++] = nightProjLight.z;

				projectors_range[mm++] = nightProjLightRange;

				projectors_cone[nn++] = nightProjLightCone;
			}

			///Passer les paramètres au shader
			nightShader.setUniform1i("skip_projectors", 0);
			nightShader.setUniform3fv("projectors_pos", projectors_pos, NB_PROJECTORS);
			nightShader.setUniform3fv("projectors_color", projectors_color, NB_PROJECTORS);
			nightShader.setUniform3fv("projectors_dir", projectors_dir, NB_PROJECTORS);
			nightShader.setUniform1fv("projectors_range", projectors_range, NB_PROJECTORS);
			nightShader.setUniform1fv("projectors_cone", projectors_cone, NB_PROJECTORS);

			///Libérer la mémoire
			delete[] projectors_pos;
			delete[] projectors_color;
			delete[] projectors_dir;
			delete[] projectors_range;
			delete[] projectors_cone;
			
			nightShader.setUniformMatrix4f("view", view);
			nightShader.setUniformMatrix4f("projection", projection);
			drawMonster(parent_model, &nightShader, monster_rotate);
			Node::draw(parent_model, &nightShader);
			nightShader.end();
		}
		else if (*timeOfDay == weather::DAY) {
			glClearColor(fogColor.x, fogColor.y, fogColor.z, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			//Dépendre de ofShader::bindDefaults() pour lier les attributs du Vertex Array Buffer
			//à l'entrée du Vertex Shader. Je rencontrais des problèmes lorsque je tentais
			//de les redéfinir. Il semble impossible de désactiver ofShader::bindDefaults().
			ofVec3f light_position(player->x, 0.0f, player->y);
			
			dayShader.begin();
			dayShader.setUniformMatrix4f("view", view);
			dayShader.setUniformMatrix4f("projection", projection);
			dayShader.setUniform3fv("fogColor", fogColor.getPtr());
			dayShader.setUniform3fv("diffuseLight", dayDiffuseLight.getPtr());
			dayShader.setUniform3fv("lightPosition", light_position.getPtr());
			drawMonster(parent_model, &dayShader, monster_rotate);
			Node::draw(parent_model, &dayShader);
			dayShader.end();
		}
		
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
	}
	
	void SceneNode::drawMonster(const ofMatrix4x4 & parent_model, ofShader * shader, float rotate) {
		ofMatrix4x4 monster_model;

		float size = 0.05f;
		monster_model = nosmaths::matr4x4rotationY(monster_model, rotate);
		monster_model = nosmaths::matr4x4scale(monster_model, size, size, size);

		float altitude = ofMap(
			sin(ofGetElapsedTimef()),
			-1, 1,
			-0.4f, -0.2f);
		monster_model = nosmaths::matr4x4translation(monster_model, monster->x, altitude, monster->y);

		ofMatrix4x4 new_model = nosmaths::matr4x4multiplication(monster_model, parent_model);
		bindModelMatx(new_model, shader);
		
		if(*mode == GLOW) {
			shader->setUniform1i("do_texture", 1);
			glowMonsterModel.draw(false);
			shader->setUniform1i("do_texture", 0);
		} else {
			nightShader.setUniform1i("skip_projectors", 1);
			monsterModel.draw(*mode == SELECT);
			nightShader.setUniform1i("skip_projectors", 0);
		}
	}
	
	
	SceneNode::Skybox::Skybox(Player * _player, std::string * _textureFolder) : player(_player), textureFolder(*_textureFolder) {
		skyboxShader.load("shaders/perspective/skybox");
		
		//Load cubemap
		ofImage images[6];
		bool loaded =
		images[0].loadImage(textureFolder + "skybox/rt.tga") &&
		images[1].loadImage(textureFolder + "skybox/lf.tga") &&
		images[2].loadImage(textureFolder + "skybox/up.tga") &&
		images[3].loadImage(textureFolder + "skybox/dn.tga") &&
		images[4].loadImage(textureFolder + "skybox/ft.tga") &&
		images[5].loadImage(textureFolder + "skybox/up.tga");
		
		if(!loaded) {
			ofLogError() << "Skybox not loaded";
		} else {
			//Load skybox textures
			glGenTextures(1, &skyBoxTextureID);
			glBindTexture(GL_TEXTURE_CUBE_MAP, skyBoxTextureID);
			
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
			
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			
			unsigned char * img_data;
			int size = images[0].getWidth();
			
			img_data = images[0].getPixels();
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, size, size, 0, GL_RGB, GL_UNSIGNED_BYTE, img_data); // positive x
			
			img_data = images[1].getPixels();
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, size, size, 0, GL_RGB, GL_UNSIGNED_BYTE, img_data); // positive y
			
			img_data = images[2].getPixels();
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, size, size, 0, GL_RGB, GL_UNSIGNED_BYTE, img_data); // positive z
			
			img_data = images[3].getPixels();
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, size, size, 0, GL_RGB, GL_UNSIGNED_BYTE, img_data); // negative x
			
			img_data = images[4].getPixels();
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, size, size, 0, GL_RGB, GL_UNSIGNED_BYTE, img_data); // negative y
			
			img_data = images[5].getPixels();
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, size, size, 0, GL_RGB, GL_UNSIGNED_BYTE, img_data); // negative z
			
			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
			
			//VAO for skybox
			
			GLfloat skyboxVertices[] = {
				// Positions
				-1.0f,  1.0f, -1.0f,
				-1.0f, -1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,
				1.0f,  1.0f, -1.0f,
				-1.0f,  1.0f, -1.0f,
				
				-1.0f, -1.0f,  1.0f,
				-1.0f, -1.0f, -1.0f,
				-1.0f,  1.0f, -1.0f,
				-1.0f,  1.0f, -1.0f,
				-1.0f,  1.0f,  1.0f,
				-1.0f, -1.0f,  1.0f,
				
				1.0f, -1.0f, -1.0f,
				1.0f, -1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f,  1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,
				
				-1.0f, -1.0f,  1.0f,
				-1.0f,  1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f, -1.0f,  1.0f,
				-1.0f, -1.0f,  1.0f,
				
				-1.0f,  1.0f, -1.0f,
				1.0f,  1.0f, -1.0f,
				1.0f,  1.0f,  1.0f,
				1.0f,  1.0f,  1.0f,
				-1.0f,  1.0f,  1.0f,
				-1.0f,  1.0f, -1.0f,
				
				-1.0f, -1.0f, -1.0f,
				-1.0f, -1.0f,  1.0f,
				1.0f, -1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,
				-1.0f, -1.0f,  1.0f,
				1.0f, -1.0f,  1.0f
			};
			
			// Setup skybox VAO
			GLuint skyboxVBO;
			glGenVertexArrays(1, &skyboxVAO);
			glGenBuffers(1, &skyboxVBO);
			glBindVertexArray(skyboxVAO);
			glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
			glBindVertexArray(0);
		}
	}

	SceneNode::Skybox::~Skybox() {
		glDeleteTextures(1, &skyBoxTextureID);
		glDeleteVertexArrays(1, &skyboxVAO);
	}
	
	
	
	void SceneNode::Skybox::draw(const ofMatrix4x4 & projection) {
			glDepthMask(GL_FALSE);
			skyboxShader.begin();

			ofMatrix4x4 boxview;
			nosmaths::Quaternion conversion(PI / 2.0f, ofVec3f(1, 0, 0));
			nosmaths::Quaternion orientation = conversion.inverse() * player->getOrientation() * conversion;
			boxview = nosmaths::matr4x4multiplication(nosmaths::matr4x4multiplication(boxview, orientation.getMatx()), player->getHead().getMatx());

			skyboxShader.setUniformMatrix4f("view", boxview);
			skyboxShader.setUniformMatrix4f("projection", projection);

			// skybox cube
			glBindVertexArray(skyboxVAO);
			glActiveTexture(GL_TEXTURE0);
			glUniform1i(glGetUniformLocation(skyboxShader.getProgram(), "skybox"), 0);
			glBindTexture(GL_TEXTURE_CUBE_MAP, skyBoxTextureID);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			glBindVertexArray(0);
			skyboxShader.end();
			glDepthMask(GL_TRUE);
	}
	
	
	
	///////////////////
	// MazeNode
	///////////////////
	
	ofImage MazeNode::flagEntranceSkin;
	ofImage MazeNode::flagExitSkin;
	ofImage MazeNode::poleSkin;
	CoonsHermiteSurfaceMesh MazeNode::flagMesh(10);
	
	void MazeNode::prepare() {
		
		flagEntranceSkin.loadImage("textures/simulation/flag_entrance.png");
		flagExitSkin.loadImage("textures/simulation/flag_exit.png");
		poleSkin.loadImage("textures/simulation/marble.jpg");
		
		flagMesh.getSurfaceRef() = nosmaths::CoonsHermiteSurface(nosmaths::HermiteCurve(ofPoint(-1, 1),  ofPoint( 1, 1), ofVec3f(), ofVec3f()),
																 nosmaths::HermiteCurve(ofPoint(-1,-1),  ofPoint( 1,-1), ofVec3f(), ofVec3f()),
																 nosmaths::HermiteCurve(ofPoint(-1, 1),  ofPoint(-1,-1), ofVec3f(), ofVec3f()),
																 nosmaths::HermiteCurve(ofPoint( 1, 1),  ofPoint( 1,-1), ofVec3f(), ofVec3f()));
		flagMesh.setup();
	}
	
	MazeNode::MazeNode(Maze * _maze, MODE * _mode) :
		maze(_maze),
		mode(_mode)
	{
	}
	
	
	
	void MazeNode::bindIf(ofImage & texture) {
		if(*mode == RENDER) {
			texture.getTextureReference().bind();
		}
	}
	
	
	
	void MazeNode::unbindIf(ofImage & texture) {
		if(*mode == RENDER) {
			texture.getTextureReference().unbind();
		}
	}
	
	void MazeNode::update() {
		//UPDATE FLAG MESH
		//attetion : doit être fait une seule fois par frame
		nosmaths::CoonsHermiteSurface & surf = flagMesh.getSurfaceRef();
		
		float alpha = sin(ofGetElapsedTimef() * 3) * PI / 4.0f;
		int curve_speed = 3;
		ofPoint v1, v2;
		
		v1.x = cos(alpha);
		v1.z = -sin(alpha);
		
		v2.x = cos(alpha);
		v2.z = sin(alpha);
		
		v1 *= curve_speed;
		v2 *= curve_speed;
		
		nosmaths::HermiteCurve c1 = surf.getC1();
		c1.setV1(v1);
		c1.setV2(v2);
		surf.setC1(c1);
		
		nosmaths::HermiteCurve c2 = surf.getC2();
		c2.setV1(v1);
		c2.setV2(v2);
		surf.setC2(c2);
		
		//position of right side
		float beta1 = sin(ofGetElapsedTimef() * 2) * PI / 6.0f;
		float beta2 = sin(ofGetElapsedTimef() * 2) * -PI / 6.0f;
		
		surf.setC4(nosmaths::HermiteCurve(
										  ofPoint(cos(beta1), 1, sin(beta1)),
										  ofPoint(cos(beta2), -1, sin(beta2)),
										  ofVec3f(0, 0, sin(beta1) * 5),
										  ofVec3f(0, 0, sin(beta1) * 5)
										  ));
		
		flagMesh.update();
	}
	
	void MazeNode::draw(const ofMatrix4x4 & parent_model, ofShader * shader) {
		
		Sector * entrance = maze->getEntrance();
		Sector * exit = maze->getExit();
		
		drawFlag(parent_model, shader, entrance);
		drawFlag(parent_model, shader, exit);
		
		Node::draw(model, shader);
	}
	
	void MazeNode::drawFlag(const ofMatrix4x4 & new_model, ofShader * shader, Sector * sector) {
		
		//Prepare sector model matrix
		
		ofMatrix4x4 sector_model;
		sector_model = nosmaths::matr4x4translation(sector_model, sector->getMazePosX(), 0, sector->getMazePosY());
		if (sector->getKind() == Sector::EXIT) {
			sector_model = nosmaths::matr4x4translation(sector_model, -1, 0, 0);
		} else {
			sector_model = nosmaths::matr4x4translation(sector_model, 1, 0, 0);
		}
		
		//Prepare flag model matrix
		
		ofMatrix4x4 TEST;
		TEST = nosmaths::matr4x4rotationY(TEST, 135);

		ofMatrix4x4 TESTold;
		TESTold.rotate(135, 0, 1, 0);

		ofMatrix4x4 flag_model;
		flag_model = nosmaths::matr4x4translation(flag_model, 1, 0, 0);
		flag_model = nosmaths::matr4x4scale(flag_model, 0.25, 0.25, 0.25);

		ofMatrix4x4 flag_model_old;
		flag_model_old.translate(1, 0, 0);
		flag_model_old.scale(0.25, 0.25, 0.25);

		if (sector->getKind() == Sector::EXIT) {
			flag_model = nosmaths::matr4x4rotationY(flag_model, 135);
			flag_model = nosmaths::matr4x4translation(flag_model, .48f, 1.2f, .48f);
		} else {
			flag_model = nosmaths::matr4x4rotationY(flag_model, 45);
			flag_model = nosmaths::matr4x4translation(flag_model, -.48f, 1.2f, .48f);

			flag_model_old.rotate(45, 0, 1, 0);
			flag_model_old.translate(-.48f, 1.2f, .48f);
		}
		


		flag_model_old = flag_model_old * sector_model * new_model;

		flag_model = nosmaths::matr4x4multiplication(nosmaths::matr4x4multiplication(flag_model, sector_model), new_model);
		
		//Prepare pole model matrix
		
		ofMatrix4x4 pole_model;
		pole_model = nosmaths::matr4x4scale(pole_model, .01f, 1.5f, .01f);
		if (sector->getKind() == Sector::EXIT) {
			pole_model = nosmaths::matr4x4translation(pole_model, .48f, 0, .48f);
		}
		else {
			pole_model = nosmaths::matr4x4translation(pole_model, -.48f, 0, .48f);
		}
		
		pole_model = nosmaths::matr4x4multiplication(nosmaths::matr4x4multiplication(pole_model, sector_model), new_model);
		
		//Draw the flag and pole
		
		bindModelMatx(flag_model, shader);
		
		ofImage & skin = sector->getKind() == Sector::EXIT ?
		flagExitSkin :
		flagEntranceSkin;
		
		bindIf(skin);
		glDisable(GL_CULL_FACE);
		flagMesh.draw();
		glEnable(GL_CULL_FACE);
		unbindIf(skin);
		
		bindModelMatx(pole_model, shader);
		
		bindIf(poleSkin);
		primitives::p_cube();
		unbindIf(poleSkin);
		
	}
	
	
	///////////////////
	// SectorRowNode
	///////////////////
	
	SectorRowNode::SectorRowNode(int _position) : position(_position) {
		model = nosmaths::matr4x4translation(model, position, 0.0f, 0.0f);
	}
	
	
	
	void SectorRowNode::draw(const ofMatrix4x4 &parent_model, ofShader * shader) {
		ofMatrix4x4 new_model = nosmaths::matr4x4multiplication(model, parent_model);
		Node::draw(new_model, shader);
	}
	
	
	
	///////////////////
	// SectorNode
	///////////////////
	

	GLuint SectorNode::vaoWall  = 0;
	ofImage SectorNode::wallSkin;
	ofImage SectorNode::wallNormalMap;
	ofImage SectorNode::floorSkin;
	ofImage SectorNode::floorNormalMap;
	ofImage SectorNode::entranceSkin;
	ofImage SectorNode::entranceNormalMap;
	ofImage SectorNode::exitSkin;
	ofImage SectorNode::exitNormalMap;
	ofImage SectorNode::shapesSkin;
	ofImage SectorNode::blueGemSkin;
	ofImage SectorNode::redGemSkin;
	ofImage SectorNode::greenGemSkin;
	ofImage SectorNode::purpleGemSkin;
	OBJWaveFrontLoader SectorNode::chestModel;
	OBJWaveFrontLoader SectorNode::redPotionModel;
	OBJWaveFrontLoader SectorNode::bluePotionModel;
	ColorIDManager SectorNode::colorIDManager;
	
	GLfloat SectorNode::wallsVertices[420] = {
			//SOMMET               //TEXTURE     //NORMAL           //TANGENTS //BITANGENTS

			//back/north wall			
			-0.5f, -0.5f, -0.5f,   0.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0,0,0,      0,0,0,
			 0.5f, -0.5f, -0.5f,   1.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0,0,0,      0,0,0,
			 0.5f,  0.5f, -0.5f,   1.0f, 1.0f,   0.0f, 0.0f, 1.0f, 0,0,0,      0,0,0,
			 0.5f,  0.5f, -0.5f,   1.0f, 1.0f,   0.0f, 0.0f, 1.0f, 0,0,0,      0,0,0,
			-0.5f,  0.5f, -0.5f,   0.0f, 1.0f,   0.0f, 0.0f, 1.0f, 0,0,0,      0,0,0,
			-0.5f, -0.5f, -0.5f,   0.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0,0,0,      0,0,0,
			

			//right/east wall			
			 0.5f,  0.5f,  0.5f,   1.0f, 1.0f,  -1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			 0.5f,  0.5f, -0.5f,   0.0f, 1.0f,  -1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			 0.5f, -0.5f, -0.5f,   0.0f, 0.0f,  -1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			 0.5f, -0.5f, -0.5f,   0.0f, 0.0f,  -1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			 0.5f, -0.5f,  0.5f,   1.0f, 0.0f,  -1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			 0.5f,  0.5f,  0.5f,   1.0f, 1.0f,  -1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			

			//front/south wall			
			-0.5f, -0.5f,  0.5f,   0.0f, 0.0f,   0.0f, 0.0f, -1.0f, 0,0,0,      0,0,0,
			 0.5f,  0.5f,  0.5f,   1.0f, 1.0f,   0.0f, 0.0f, -1.0f, 0,0,0,      0,0,0,
			 0.5f, -0.5f,  0.5f,   1.0f, 0.0f,   0.0f, 0.0f, -1.0f, 0,0,0,      0,0,0,
			 0.5f,  0.5f,  0.5f,   1.0f, 1.0f,   0.0f, 0.0f, -1.0f, 0,0,0,      0,0,0,
			-0.5f, -0.5f,  0.5f,   0.0f, 0.0f,   0.0f, 0.0f, -1.0f, 0,0,0,      0,0,0,
			-0.5f,  0.5f,  0.5f,   0.0f, 1.0f,   0.0f, 0.0f, -1.0f, 0,0,0,      0,0,0,
			

			//left/west wall			
			-0.5f,  0.5f,  0.5f,   0.0f, 1.0f,  1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			-0.5f, -0.5f, -0.5f,   1.0f, 0.0f,  1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			-0.5f,  0.5f, -0.5f,   1.0f, 1.0f,  1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			-0.5f, -0.5f, -0.5f,   1.0f, 0.0f,  1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			-0.5f,  0.5f,  0.5f,   0.0f, 1.0f,  1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,
			-0.5f, -0.5f,  0.5f,   0.0f, 0.0f,  1.0f, 0.0f, 0.0f, 0,0,0,      0,0,0,

			//Floor
			-0.5f, -0.5f, -0.5f,   0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0,0,0,      0,0,0,
			 0.5f, -0.5f,  0.5f,   1.0f, 1.0f,  0.0f, 1.0f, 0.0f, 0,0,0,      0,0,0,
			 0.5f, -0.5f, -0.5f,   1.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0,0,0,      0,0,0,
			 0.5f, -0.5f,  0.5f,   1.0f, 1.0f,  0.0f, 1.0f, 0.0f, 0,0,0,      0,0,0,
			-0.5f, -0.5f, -0.5f,   0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0,0,0,      0,0,0,
			-0.5f, -0.5f,  0.5f,   0.0f, 1.0f,  0.0f, 1.0f, 0.0f, 0,0,0,      0,0,0
	};

	
	
	void SectorNode::prepare(string texture_folder) {
	
		wallSkin.loadImage(texture_folder + "haie.png");
		wallNormalMap.loadImage(texture_folder + "haie_NRM.png");
		//wallNormalMap.loadImage("textures/neutre_NRM.png");
		floorSkin.loadImage(texture_folder + "chemin_pierre.png");
		floorNormalMap.loadImage(texture_folder + "chemin_pierre_NRM.png");
		entranceSkin.loadImage(texture_folder + "entree.png");
		entranceNormalMap.loadImage(texture_folder + "entree_NRM.png");
		exitSkin.loadImage(texture_folder + "sortie.png");
		exitNormalMap.loadImage(texture_folder + "sortie_NRM.png");
		shapesSkin.loadImage(texture_folder + "marble.jpg");
		
		blueGemSkin.loadImage("textures/gems/blue.png");
		redGemSkin.loadImage("textures/gems/red.png");
		greenGemSkin.loadImage("textures/gems/green.png");
		purpleGemSkin.loadImage("textures/gems/purple.png");
				
		if (0 == vaoWall) {
			chestModel.load("models/chest/chest.wavefront");
			redPotionModel.load("models/red_potion/potion.wavefront");
			bluePotionModel.load("models/blue_potion/potion.wavefront");

			GLuint vertexbuffer;

			//Sommets
			genTangents(wallsVertices, 420, 14);

			glGenVertexArrays(1, &vaoWall);
			glGenBuffers(1, &vertexbuffer);

			glBindVertexArray(vaoWall);

			//Envoyer les données
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(wallsVertices), wallsVertices, GL_STATIC_DRAW);

			//Indiquer quoi faire de ces données
			int stride = 14 * sizeof(GLfloat);
			// Position attribute
			glVertexAttribPointer(ofShader::POSITION_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride, (GLvoid*)0);
			glEnableVertexAttribArray(ofShader::POSITION_ATTRIBUTE);
			// TexCoord attribute
			glVertexAttribPointer(ofShader::TEXCOORD_ATTRIBUTE, 2, GL_FLOAT, GL_FALSE, stride, (GLvoid*)(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(ofShader::TEXCOORD_ATTRIBUTE);
			// Normals attribute
			glVertexAttribPointer(ofShader::NORMAL_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride, (GLvoid*)(5 * sizeof(GLfloat)));
			glEnableVertexAttribArray(ofShader::NORMAL_ATTRIBUTE);
			// Tangents attribute
			glVertexAttribPointer(TANGENT_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride, (GLvoid*)(8 * sizeof(GLfloat)));
			glEnableVertexAttribArray(TANGENT_ATTRIBUTE);
			// Bitangents attribute
			glVertexAttribPointer(BITANGENT_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, stride, (GLvoid*)(11 * sizeof(GLfloat)));
			glEnableVertexAttribArray(BITANGENT_ATTRIBUTE);

			glBindVertexArray(0);
		}
	}

	void SectorNode::genTangents(GLfloat * vertices, int nb_verts, int stride) {
		GLfloat * cursor = vertices, *vertex0, *vertex1, *vertex2;

		for (int ii = 0; ii < nb_verts; ii += stride * 3) {
			vertex0 = cursor;
			cursor += stride;

			vertex1 = cursor;
			cursor += stride;

			vertex2 = cursor;
			cursor += stride;

			ofVec3f v0(*vertex0, *(vertex0 + 1), *(vertex0 + 2));
			ofVec3f v1(*vertex1, *(vertex1 + 1), *(vertex1 + 2));
			ofVec3f v2(*vertex2, *(vertex2 + 1), *(vertex2 + 2));

			ofVec2f uv0(*(vertex0 + 3), *(vertex0 + 4));
			ofVec2f uv1(*(vertex1 + 3), *(vertex1 + 4));
			ofVec2f uv2(*(vertex2 + 3), *(vertex2 + 4));

			ofVec3f normal(*(vertex0 + 5), *(vertex0 + 6), *(vertex0 + 7));

			ofVec3f delta_pos1 = v1 - v0;
			ofVec3f delta_pos2 = v2 - v0;

			ofVec3f delta_uv1 = uv1 - uv0;
			ofVec3f delta_uv2 = uv2 - uv0;

			float r = 1.0f / (delta_uv1.x * delta_uv2.y - delta_uv1.y * delta_uv2.x);

			ofVec3f tangent = (delta_pos1 * delta_uv2.y - delta_pos2 * delta_uv1.y)*r;
			tangent = (tangent - normal * normal.dot(tangent)).normalize();

			ofVec3f bitangent = (delta_pos2 * delta_uv1.x - delta_pos1 * delta_uv2.x)*r;

			vertex0[8] = tangent.x; vertex0[9] = tangent.y; vertex0[10] = tangent.z;
			vertex1[8] = tangent.x; vertex1[9] = tangent.y; vertex1[10] = tangent.z;
			vertex2[8] = tangent.x; vertex2[9] = tangent.y; vertex2[10] = tangent.z;

			vertex0[11] = bitangent.x; vertex0[12] = bitangent.y; vertex0[13] = bitangent.z;
			vertex1[11] = bitangent.x; vertex1[12] = bitangent.y; vertex1[13] = bitangent.z;
			vertex2[11] = bitangent.x; vertex2[12] = bitangent.y; vertex2[13] = bitangent.z;
		}
	}
	
	
	SectorNode::SectorNode(Sector * _sector, Sector * _north, Sector * _east, Sector * _south, Sector * _west, MODE * _mode, std::map<Sector *, unsigned int> * sector_clicks) :
		sector(_sector), north(_north), east(_east), south(_south), west(_west), mode(_mode),
		sectorClicks(sector_clicks) {
					
		model = nosmaths::matr4x4translation(model, 0.0, 0.0f, sector->getMazePosY());
	}
	
	
	void SectorNode::bindIf(ofImage & texture) {
		if(*mode == RENDER) {
			texture.getTextureReference().bind();
		}
	}
	
	
	
	void SectorNode::unbindIf(ofImage & texture) {
		if(*mode == RENDER) {
			texture.getTextureReference().unbind();
		}
	}
	
	
	
	void SectorNode::drawSurroundingWalls(const ofMatrix4x4 & new_model, ofShader * shader) {
		shader->setUniform1i("do_normalmap", 1);

		shader->setUniformTexture("texN", wallNormalMap.getTextureReference(), 1);

		glBindVertexArray(vaoWall);
		
		bindModelMatx(new_model, shader);

		if (*mode == SELECT) {
			ofVec3f select_color(0, 0, 0.5f); ///< Tout élément contenant du bleu est considéré non sélectionnable
			shader->setUniform3fv("select_color", select_color.getPtr());
		}
		
		bindIf(wallSkin);

		//if north wall
		if (north != 0 && north->getKind() == Sector::WALL) {
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}

		//if east wall
		if (east != 0 && east->getKind() == Sector::WALL) {
			glDrawArrays(GL_TRIANGLES, 6, 6);
		}


		//if south wall
		if (south != 0 && south->getKind() == Sector::WALL) {
			glDrawArrays(GL_TRIANGLES, 12, 6);
		}


		//if west wall
		if (west != 0 && west->getKind() == Sector::WALL) {
			glDrawArrays(GL_TRIANGLES, 18, 6);
		}

		unbindIf(wallSkin);

		//Entrance
		if (west != 0 && west->getKind() == Sector::ENTRANCE) {
			bindIf(entranceSkin);
			shader->setUniformTexture("texN", entranceNormalMap.getTextureReference(), 1);
			
			glDrawArrays(GL_TRIANGLES, 18, 6);

			unbindIf(entranceSkin);
		}

		//Exit
		if (east != 0 && east->getKind() == Sector::EXIT) {
			bindIf(exitSkin);
			shader->setUniformTexture("texN", exitNormalMap.getTextureReference(), 1);

			glDrawArrays(GL_TRIANGLES, 6, 6);

			unbindIf(exitSkin);
		}


		//Floor
		shader->setUniformTexture("texN", floorNormalMap.getTextureReference(), 1);
		bindIf(floorSkin);
		
		glDrawArrays(GL_TRIANGLES, 24, 6);

		unbindIf(floorSkin);
		
		glBindVertexArray(0);

		shader->setUniform1i("do_normalmap", 0);
	}
	
	
	void SectorNode::draw(const ofMatrix4x4 &parent_model, ofShader * shader) {
		ofMatrix4x4 new_model = nosmaths::matr4x4multiplication(model, parent_model);

		if (sector->getKind() == Sector::PASSAGE) {
			drawSurroundingWalls(new_model, shader);
		}

		if (sector->getKind() == Sector::BOULDER) {
			drawSurroundingWalls(new_model, shader);

			drawBoulder(new_model, shader);
		}

		if (sector->getKind() == Sector::BLUE_POTION ||
			sector->getKind() == Sector::RED_POTION) {
			drawSurroundingWalls(new_model, shader);

			drawPotion(new_model, shader);
		}
		
		if(sector->getKind() == Sector::TREASURE) {
			drawSurroundingWalls(new_model, shader);
			
			drawTreasure(new_model, shader);
		}
		
		if(sector->getKind() == Sector::LIGHT) {
			drawSurroundingWalls(new_model, shader);
			
			/// \todo Modèle plus inspiré de la torche
			
			//Pole
			ofMatrix4x4 pole_model;
			pole_model = nosmaths::matr4x4scale(pole_model, .01f, 0.25f, .01f);
			pole_model = nosmaths::matr4x4translation(pole_model, 0, -0.25f, 0);
			pole_model = nosmaths::matr4x4multiplication(pole_model, new_model);

			bindModelMatx(pole_model, shader);
			
			bindIf(shapesSkin);
			primitives::p_cube();
			unbindIf(shapesSkin);
			
			//Lumière
			ofMatrix4x4 light_model;
			light_model = nosmaths::matr4x4scale(light_model, .03f, 0.03f, .03f);
			light_model = nosmaths::matr4x4multiplication(light_model, new_model);
			
			bindModelMatx(light_model, shader);
			
			
			if(*mode == GLOW) {
				shader->setUniform3f("select_color", 1, 1, 1);
			} else if(*mode == RENDER) {
				shader->setUniform1i("do_flat_color", 1);
			}
			
			bindIf(shapesSkin);
			primitives::p_cube();
			unbindIf(shapesSkin);

			if(*mode == GLOW) {
				shader->setUniform3f("select_color", 0, 0, 0);
			} else if(*mode == RENDER) {
				shader->setUniform1i("do_flat_color", 0);
			}
		}
		
		Node::draw(new_model, shader);
	}



	void SectorNode::drawBoulder(const ofMatrix4x4 & new_model, ofShader * shader) {
		ofMatrix4x4 boulder_model;

		float size = 0.22f;
		boulder_model = nosmaths::matr4x4scale(boulder_model, size, size, size);
		boulder_model = nosmaths::matr4x4translation(boulder_model, 0, -0.5f + size, 0);
		boulder_model = nosmaths::matr4x4multiplication(boulder_model, new_model);

		bindModelMatx(boulder_model, shader);

		if (*mode == SELECT) {
			ofVec3f select_color(0, 0, 0.75f);
			shader->setUniform3fv("select_color", select_color.getPtr());
		}

		bindIf(shapesSkin);
		primitives::p_cube();
		unbindIf(shapesSkin);
	}



	void SectorNode::drawPotion(const ofMatrix4x4 & new_model, ofShader * shader) {
		unsigned int nb_clicks = 0;
		if (sectorClicks->find(sector) != sectorClicks->end()) {
			nb_clicks = (*sectorClicks)[sector];
		}

		if(*mode == SELECT) {
			ofVec3f select_color;
			if (nb_clicks == 0 || nb_clicks >= 2) {
				select_color = colorIDManager.getID(sector);
			}
			else {
				select_color = ofVec3f(0, 0, 0.25f);
			}
			shader->setUniform3fv("select_color", select_color.getPtr());
		}

		drawChest(new_model, shader);

		if (nb_clicks == 1) {
			if (*mode == SELECT) {
				ofVec3f select_color = colorIDManager.getID(sector);
				shader->setUniform3fv("select_color", select_color.getPtr());
			}

			ofMatrix4x4 potion_model;

			float scale = 0.0025f;
			potion_model = nosmaths::matr4x4rotationX(potion_model, -80);
			potion_model = nosmaths::matr4x4rotationY(potion_model, ofGetElapsedTimef() * 100);
			potion_model = nosmaths::matr4x4scale(potion_model, scale, scale, scale);
			potion_model = nosmaths::matr4x4translation(potion_model, 0, 0, 0);
			potion_model = nosmaths::matr4x4multiplication(potion_model, new_model);

			bindModelMatx(potion_model, shader);

			if (sector->getKind() == Sector::BLUE_POTION) {
				bluePotionModel.draw(*mode == SELECT);
			}
			else if (sector->getKind() == Sector::RED_POTION) {
				redPotionModel.draw(*mode == SELECT);
			}

		}
	}
	
	void SectorNode::drawTreasure(const ofMatrix4x4 &new_model, ofShader * shader) {
		unsigned int nb_clicks = 0;
		if (sectorClicks->find(sector) != sectorClicks->end()) {
			nb_clicks = (*sectorClicks)[sector];
		}
		
		if(*mode == SELECT) {
			ofVec3f select_color;
			if (nb_clicks == 0 || nb_clicks >= 2) {
				select_color = colorIDManager.getID(sector);
			}
			else {
				select_color = ofVec3f(0, 0, 0.25f);
			}
			shader->setUniform3fv("select_color", select_color.getPtr());
		}
		
		
		drawChest(new_model, shader);
		
		if(nb_clicks == 1) {
			/// \todo Dessiner des pierres précieuses
			
			if (*mode == SELECT) {
				ofVec3f select_color = colorIDManager.getID(sector);
				shader->setUniform3fv("select_color", select_color.getPtr());
			}
			
			float scale = 0.05f;
			float displacement = -.1f;
			float angleXZ = 2*PI*fmod(ofGetElapsedTimef(), 2)/2;
			float angleY = 2 * PI*fmod(ofGetElapsedTimef(), 3) / 3;
			float axis_rotation_angle = ofGetElapsedTimef() * 5;
			
			ofMatrix4x4 blue_gem_model;
			blue_gem_model = nosmaths::matr4x4scale(blue_gem_model, scale, scale, scale);
			blue_gem_model = nosmaths::matr4x4rotationY(blue_gem_model, axis_rotation_angle);
			blue_gem_model = nosmaths::matr4x4translation(blue_gem_model, -displacement*cos(angleXZ), displacement*sin(angleY), -displacement*sin(angleXZ));
			blue_gem_model = nosmaths::matr4x4multiplication(blue_gem_model, new_model);
			
			ofMatrix4x4 red_gem_model;
			red_gem_model = nosmaths::matr4x4scale(red_gem_model, scale, scale, scale);
			red_gem_model = nosmaths::matr4x4rotationY(red_gem_model, axis_rotation_angle);
			red_gem_model = nosmaths::matr4x4translation(red_gem_model, -displacement*cos(angleXZ + PI / 2), -displacement*sin(angleY + PI / 2), -displacement*sin(angleXZ + PI / 2));
			red_gem_model = nosmaths::matr4x4multiplication(red_gem_model, new_model);
			
			ofMatrix4x4 green_gem_model;
			green_gem_model = nosmaths::matr4x4scale(green_gem_model, scale, scale, scale);
			green_gem_model = nosmaths::matr4x4rotationY(green_gem_model, axis_rotation_angle);
			green_gem_model = nosmaths::matr4x4translation(green_gem_model, -displacement*cos(angleXZ + PI), displacement*sin(angleY + PI), -displacement*sin(angleXZ + PI));
			green_gem_model = nosmaths::matr4x4multiplication(green_gem_model, new_model);

			ofMatrix4x4 purple_gem_model;
			purple_gem_model = nosmaths::matr4x4scale(purple_gem_model, scale, scale, scale);
			purple_gem_model = nosmaths::matr4x4rotationY(purple_gem_model, axis_rotation_angle);
			purple_gem_model = nosmaths::matr4x4translation(purple_gem_model, -displacement*cos(angleXZ - PI / 2), -displacement*sin(angleY - PI / 2), -displacement*sin(angleXZ - PI / 2));
			purple_gem_model = nosmaths::matr4x4multiplication(purple_gem_model, new_model);
			
			bindModelMatx(blue_gem_model, shader);
			bindIf(blueGemSkin);
			primitives::p_dode();
			unbindIf(blueGemSkin);
			
			bindModelMatx(red_gem_model, shader);
			bindIf(redGemSkin);
			primitives::p_tetra();
			unbindIf(redGemSkin);
			
			bindModelMatx(green_gem_model, shader);
			bindIf(greenGemSkin);
			primitives::p_octa();
			unbindIf(greenGemSkin);

			bindModelMatx(purple_gem_model, shader);
			bindIf(purpleGemSkin);
			primitives::p_iso();
			unbindIf(purpleGemSkin);

		}

	}
	
	void SectorNode::drawChest(const ofMatrix4x4 &new_model, ofShader * shader) {
		ofMatrix4x4 chest_model;
		
		float scale = 0.25f;
		chest_model = nosmaths::matr4x4rotationY(chest_model, 45);
		chest_model = nosmaths::matr4x4scale(chest_model, scale, scale, scale);
		chest_model = nosmaths::matr4x4translation(chest_model, 0, -0.5f, 0);
		chest_model = nosmaths::matr4x4multiplication(chest_model, new_model);

		bindModelMatx(chest_model, shader);
		
		chestModel.draw(*mode == SELECT);
	}
	
	///////////////////
	// MazeBuilder
	///////////////////
	
	MazeBuilder::MazeBuilder(Maze * _maze, Player * _player, Monster * _monster, weather::TimeOfDay * _timeOfDay, MODE * _mode, std::map<Sector *, unsigned int> * sector_clicks, string * _textureFolder) :
		scenegraph::MazeBuilder(_maze),
		player(_player),
		monster(_monster),
		timeOfDay(_timeOfDay),
		mode(_mode),
		sectorClicks(sector_clicks),
		textureFolder(_textureFolder)
	{ }
	
	NodePtr MazeBuilder::buildScene() {
		std::vector< Sector * > lights;
		
		for(int ii = 0; ii < maze->getNbSectorsH(); ++ii) {
			for(int jj = 0; jj < maze->getNbSectorsV(); ++jj) {
				Sector * sector = maze->getSector(ii, jj);
				if(Sector::LIGHT == sector->getKind()) {
					lights.push_back(sector);
				}
			}
		}
		
		return NodePtr(new SceneNode(maze, player, monster, lights, timeOfDay, mode, textureFolder));
	}
	
	NodePtr MazeBuilder::buildMaze()
	{ return NodePtr(new MazeNode(maze, mode)); }
	
	NodePtr MazeBuilder::buildSectorRow(int position)
	{ return NodePtr(new SectorRowNode(position)); }
	
	NodePtr MazeBuilder::buildSector(int x, int y)
	{ 
		Sector * the_sector = maze->getSector(x, y);
		Sector * north_sector = 0;
		Sector * east_sector = 0;
		Sector * south_sector = 0;
		Sector * west_sector = 0;

		if (y > 0) {
			north_sector = maze->getSector(x, y - 1);
		}

		if (x < maze->getNbSectorsH() - 1) {
			east_sector = maze->getSector(x + 1, y);
		}

		if (y < maze->getNbSectorsV() - 1) {
			south_sector = maze->getSector(x, y + 1);
		}

		if (x > 0) {
			west_sector = maze->getSector(x - 1, y);
		}

		return NodePtr(new SectorNode(the_sector, north_sector, east_sector, south_sector, west_sector, mode, sectorClicks));
	}
} }