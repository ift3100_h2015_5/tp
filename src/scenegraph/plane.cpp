/**
* \file plane.cpp
* \brief Implémentation des classes pour le rendu en 2D d'un labyrinthe (carte)
* \author Equipe 5
* \version 1.0
*
*/
#include "plane.h"

namespace scenegraph {
	namespace plane {

		///////////////////
		// SceneNode
		///////////////////

		SceneNode::SceneNode(Maze * _maze, Player * _player, Monster * _monster) : player(_player), monster(_monster), maze(_maze)
		{ }



		void SceneNode::draw(const ofMatrix4x4 &parent_model, ofShader * shader) {
			float scale_width = ofGetViewportWidth() / maze->getNbSectorsH();
			float scale_height = ofGetViewportHeight() / maze->getNbSectorsV();

			ofPushMatrix();
			ofScale(scale_width, scale_height);
			ofTranslate(0.5f, 0.5f);
			Node::draw(parent_model, shader);
			drawPlayer();
			drawMonster();
			ofPopMatrix();
		}
		
		

		void SceneNode::drawPlayer() {
			ofPushMatrix();

			ofSetColor(0, 0, 255);
			ofTranslate(player->x, player->y);

			primitives::p_polygon(0, 0, player->w, 6);
			primitives::p_line(0, 0, player->getDirection().x, player->getDirection().y);

			ofPopMatrix();
		}

		
		void SceneNode::drawMonster() {
			ofPushMatrix();
			
			ofSetColor(128, 128, 128);
			ofTranslate(monster->x, monster->y);
			
			primitives::p_polygon(0, 0, monster->w, 6);
			
			ofPopMatrix();
		}


		///////////////////
		// MazeNode
		///////////////////

		MazeNode::MazeNode() {}

		void MazeNode::draw(const ofMatrix4x4 & parent_model, ofShader * shader) {
			Node::draw(parent_model, shader);
		}



		///////////////////
		// SectorRowNode
		///////////////////

		SectorRowNode::SectorRowNode(int _position) : position(_position) {}

		void SectorRowNode::draw(const ofMatrix4x4 &parent_model, ofShader * shader) {
			ofPushMatrix();
			ofTranslate((float)position, 0.0f);
			Node::draw(parent_model, shader);
			ofPopMatrix();
		}



		///////////////////
		// SectorNode
		///////////////////

		SectorNode::SectorNode(Sector * _sector) : sector(_sector) {}

		void SectorNode::draw(const ofMatrix4x4 &parent_model, ofShader * shader) {
			ofPushMatrix();
			ofTranslate(0.0f, sector->getMazePosY());

			if (Sector::WALL == sector->getKind()) {
				ofSetColor(0, 0, 0);
			}
			else if (Sector::PASSAGE == sector->getKind()) {
				ofSetColor(255, 255, 255);
			}
			else if (Sector::BOULDER == sector->getKind()) {
				ofSetColor(255, 128, 0);
			}
			else if (Sector::ENTRANCE == sector->getKind()) {
				ofSetColor(255, 0, 0);
			}
			else if (Sector::EXIT == sector->getKind()) {
				ofSetColor(0, 255, 0);
			}
			else if (Sector::RED_POTION == sector->getKind()) {
				ofSetColor(64, 64, 64);
			}
			else if (Sector::BLUE_POTION == sector->getKind()) {
				ofSetColor(64, 64, 64);
			}
			else if (Sector::TREASURE == sector->getKind()) {
				ofSetColor(192, 0, 192);
			}
			else if (Sector::LIGHT == sector->getKind()) {
				ofSetColor(255, 255, 0);
			}
			
			primitives::p_rectangle(-0.5f, -0.5f, 0.5f, 0.5f);
			Node::draw(parent_model, shader);
			ofPopMatrix();
		}


		///////////////////
		// MazeBuilder
		///////////////////

		MazeBuilder::MazeBuilder(Maze * _maze, Player * _player, Monster * _monster) :
			scenegraph::MazeBuilder(_maze),
			player(_player),
			monster(_monster)
		{ }

		NodePtr MazeBuilder::buildScene()
		{
			return NodePtr(new SceneNode(maze, player, monster));
		}

		NodePtr MazeBuilder::buildMaze()
		{
			return NodePtr(new MazeNode());
		}

		NodePtr MazeBuilder::buildSectorRow(int position)
		{
			return NodePtr(new SectorRowNode(position));
		}

		NodePtr MazeBuilder::buildSector(int x, int y)
		{
			Sector * the_sector = maze->getSector(x, y);
			return NodePtr(new SectorNode(the_sector));
		}

	}
}