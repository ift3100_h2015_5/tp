/**
* \file plane.h
* \brief Declaration des classes pour le rendu en 2D d'un labyrinthe (carte)
* \author Equipe 5
* \version 1.0
*
*/

#pragma once
#include "abstract.h"
#include "../maze/Maze.h"
#include "../ofxPrimitives/ofxPrimitives.h"
#include "../screen/simulationEntities/Player.h"
#include "../screen/simulationEntities/Monster.h"

/**
 * Classes pour le rendu plane d'un labyrinthe
 */
namespace scenegraph {
	
	/**
	 * \brief Regroupe les classes du graphe de scène pour le rendu en 2D (carte).
	 *
	 * Utilise nos primitives, ainsi que la pile de matrices et le shader de OpenFrameworks.
	 */
	namespace plane {

		/**
		 * \brief Assure le rendu de la scène
		 */
		class SceneNode : public scenegraph::Node {
		public:
			SceneNode(Maze *, Player *, Monster *);
			virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
		private:
			ofShader shader;
			Player * player;
			Monster * monster;
			Maze * maze;

			void drawPlayer();
			void drawMonster();
		};


		/**
		 * \brief Assure le rendu du labyrinthe
		 */
		class MazeNode : public scenegraph::Node {
		public:
			MazeNode();
			virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
		};


		/**
		 * \brief Assure le rendu d'une rangée de secteur
		 */
		class SectorRowNode : public scenegraph::Node {
		public:
			SectorRowNode(int _position);
			virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
		private:
			int position;
		};


		/**
		 * \brief Assure le rendu d'un secteur
		 *
		 * Chaque secteur est rendu comme un carré de couleur.
		 */
		class SectorNode : public scenegraph::Node {
		public:
			SectorNode(Sector * sector);
			virtual void draw(const ofMatrix4x4 & parent_model, ofShader * shader);
		private:
			Sector * sector;
		};

		
		
		/**
		 * \brief Fabrique les noeuds du graphe pour ce rendu.
		 */
		class MazeBuilder : public scenegraph::MazeBuilder {
		public:
			MazeBuilder(Maze * _maze, Player * _player, Monster * _monster);

			virtual NodePtr buildScene();
			virtual NodePtr buildMaze();
			virtual NodePtr buildSectorRow(int x);
			virtual NodePtr buildSector(int x, int y);
		private:
			Player * player;
			Monster * monster;
		};

	}
}