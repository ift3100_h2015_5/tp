/**
* \file abstract.cpp
* \brief Implémentation partielle des classes abstraites utilisées dans la déclaration de graphes de scène.
* \author Equipe 5
* \version 1.0
*
*/
#include "abstract.h"

namespace scenegraph {
	
	///////////////////
	// Node
	///////////////////
	
	void Node::draw(const ofMatrix4x4 & parent_model, ofShader * shader) {
		for(std::vector< NodePtr >::iterator it = children.begin(); it != children.end(); ++it) {
			(*it)->draw(parent_model, shader);
		}
	}
	
	
	
	void Node::addChild(NodePtr child) {
		children.push_back(child);
	}
	
	
	
	void Node::draw() {
		ofMatrix4x4 id;
		id.makeIdentityMatrix();
		draw(id, 0);
	}


	void Node::bindModelMatx(const ofMatrix4x4 & model, ofShader * shader) {
		ofMatrix4x4 normal_matx = ofMatrix4x4::getTransposedOf(model.getInverse());
		shader->setUniformMatrix4f("normal_matx", normal_matx);
		shader->setUniformMatrix4f("model", model);
	}
		
	
	///////////////////
	// MazeAssembler
	///////////////////
	
	NodePtr MazeAssembler::assemble(MazeBuilder & builder) {
		NodePtr root = builder.buildScene();
		NodePtr maze = builder.buildMaze();
		root->addChild(maze);
		
		for(int ii = 0; ii < builder.nbSectorRows(); ++ii) {
			NodePtr row = builder.buildSectorRow(ii);
			maze->addChild(row);
			
			for(int jj = 0; jj < builder.nbSectorsPerRow(); ++jj) {
				NodePtr sector = builder.buildSector(ii, jj);
				row->addChild(sector);
			}
		}
		
		return root;
	}
}

