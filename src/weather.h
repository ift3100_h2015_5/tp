/**
* \file weather.h
* \brief Pour la gestion du jour et de la nuit
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include <map>
#include <vector>


namespace weather {
	enum TimeOfDay { DAY, NIGHT };
	enum CardinalDirection { NORTH, EAST, SOUTH, WEST };
	
	CardinalDirection opposite(CardinalDirection d);
	
	std::pair<int, int> vec(CardinalDirection d);

	const std::vector< CardinalDirection > & getCardinalDirections();
}