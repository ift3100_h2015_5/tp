#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#include "ofMain.h"
#include "ofApp.h"
#include "maze/Maze.h"
#include <iostream>

int main()

//========================================================================
{
	ofSetCurrentRenderer(ofGLProgrammableRenderer::TYPE);
	ofSetupOpenGL(
		600,
		600,
		OF_WINDOW);			// <-------- setup the GL context

	ofApp * app = new ofApp();

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(app);
}
