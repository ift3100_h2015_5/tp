/**
 * \file ofApp.cpp
 * \brief Implémentation de ofApp
 */

#include "ofApp.h"
#include <iostream>
#include <stdlib.h> 
#include "screen/Simulation.h"
#include "screen/Splash.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetFrameRate(60);
	currentScreen = new Splash();
	currentScreen->setup();
}

//--------------------------------------------------------------
void ofApp::update(){
	Screen * next_screen = currentScreen->update();
	if (0 != next_screen) {
		delete currentScreen;
		currentScreen = next_screen;
		currentScreen->setup();
	}
}


//--------------------------------------------------------------
void ofApp::draw(){
	currentScreen->draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	currentScreen->keyPressed(key);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	currentScreen->keyReleased(key);
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
	currentScreen->mouseMoved(x, y);
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	currentScreen->mouseDragged(x, y, button);
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	currentScreen->mousePressed(x, y, button);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	currentScreen->mouseReleased(x, y, button);
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	currentScreen->windowResized(w, h);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void ofApp::guiEvent(ofxUIEventArgs &e)
{
	currentScreen->guiEvent(e);
}

void ofApp::exit()
{ }