/**
* \file Sector.h
* \brief Permets de creer un secteur dans un labyrinthe
* \author Equipe 5
* \version 1.0
*
*/

#pragma once
#include <iostream>

class Maze;

/**
 * \brief Un secteur du labyrinthe représenté par Maze.
 *
 * Le type de secteur est défini par un membre dont le type
 * est une énumération. Cette façon de faire a ses limites;
 * il se pourrait qu'on ait ultérieurement à utiliser
 * l'héritage et la composition.
 *
 * Peut être passé à stream C++ d'entrée ou de sortie.
 */
class Sector
{
public:
	///Type d'un secteur
	enum Kind { EMPTY, WALL, PASSAGE, ENTRANCE, EXIT, BOULDER, RED_POTION, BLUE_POTION, TREASURE, LIGHT };

private:
	int posX;
	int posY;
	Kind kind; ///< Type du secteur
	Maze * parent;

public:

	/**
	* \brief Constructeur sans paramètre
	*/
	Sector();

	/**
	* \brief Constructeur à partir d'un autre secteur
	*
	* \param other Une autre instance d'un secteur
	* dont les données sont utilisées pour instancier
	* la nouvelle instance d'un secteur.
	*/
	Sector(const Sector &other);

	/**
	* \brief Constructeur avec paramètres
	*
	* \param _pos_x Position en x du secteur
	* \param _pos_y Position en y du secteur
	* \param _kind Type de secteur
	* \param parent Labyrinthe parent du secteur
	*/
	Sector(int _pos_x, int _pos_y, Kind _kind, Maze * parent);

	/**
	* \brief Surcharge de l'opérateur d'assignation
	*
	* \param other Une autre instance d'un secteur
	*/
	Sector & operator=(const Sector &other);


	/**
	* \brief Destructeur de la classe secteur
	*/
	~Sector();

	
	/**
	* \brief Surcharge de l'opérateur <<
	*
	* \param is Flot d'entrée
	* \param s Une autre instance d'un secteur
	*/
	friend std::ostream& operator<<(std::ostream& os, const Sector& s);

	/**
	* \brief Surcharge de l'opérateur >>
	*
	* \param is Flot de sortie
	* \param s Une autre instance d'un secteur
	*/
	friend std::istream& operator>>(std::istream& is, Sector& s);

	
	
	int getMazePosX() const { return posX; }
	int getMazePosY() const { return posY; }
	Kind getKind() { return kind; }
	void setKind(Kind _kind) { kind = _kind; }
	void setParent(Maze * _parent) { parent = _parent; }
	Maze * getParent() { return parent; }
	
	
	
	/**
	 * \brief Le secteur peut-il être traversé par un explorateur?
	 *
	 * Pour l'instant, seuls les secteurs de type PASSAGE peuvent
	 * être traversés. Ça pourrait changer.
	 */
	bool isPassable() const;
};