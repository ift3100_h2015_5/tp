/**
* \file Maze.cpp
* \brief Implémentation du labyrinthe (permets de creer un labyrinthe aleatoire)
* \author Equipe 5
* \version 1.0
*
*/

#include "Maze.h"
#include <iostream>
#include <algorithm>
#include <ctime>
#include <list>
#include <set>
#include <stdexcept>

Maze::Maze():
nbSectorsH(0), nbSectorsV(0)
{ }



Maze::Maze(const int nb_sectors_h, const int nb_sectors_v):
	nbSectorsH(nb_sectors_h), nbSectorsV(nb_sectors_v)
{
	if (nbSectorsH % 2 == 0 || nbSectorsV % 2 == 0)  {
		throw std::invalid_argument("Maze : nb_sectors_h et nb_sectors_v DOIVENT être impairs.");
	}
	
	//Initialiser le seed aléatoire
	std::srand(unsigned(std::time(0)));

	//Donner sa taille finale au tableau 2d des secteurs
	sectors.resize(nbSectorsH);
	for (SectorVector2d::iterator it = sectors.begin(); it != sectors.end(); ++it) {
		(*it).resize(nbSectorsV);
	}

	//Prendre une position au hasard, sur la frontière gauche, comme entrée
	int entrance_pos_x = 0;
	int entrance_pos_y = (std::rand() % (nbSectorsV / 2)) * 2 + 1;
	sectors[entrance_pos_x][entrance_pos_y] = newSector(entrance_pos_x, entrance_pos_y, Sector::ENTRANCE);
	entrance = &(sectors[entrance_pos_x][entrance_pos_y]);
	
	//Prendre une position au hasard, sur la frontière droite, comme sortie
	int exit_pos_x = nbSectorsH - 1;
	int exit_pos_y = (std::rand() % (nbSectorsV / 2)) * 2 + 1;
	sectors[exit_pos_x][exit_pos_y] = newSector(exit_pos_x, exit_pos_y, Sector::EXIT);
	exit = &(sectors[exit_pos_x][exit_pos_y]);

	//Créer une grille de secteurs-passages isolés par des murs
	int pos_x = 0;
	int pos_y = 0;
	for (SectorVector2d::iterator it = sectors.begin(); it != sectors.end(); ++it) {
		for (SectorVector::iterator it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
			//C'est l'entrée ou la sortie, pas besoin de remplir
			if((*it2).getKind() != Sector::ENTRANCE && (*it2).getKind() != Sector::EXIT) {
				if(1 == pos_x % 2 && 1 == pos_y % 2) {
					(*it2) = newSector(pos_x, pos_y, Sector::PASSAGE);
				} else {
					(*it2) = newSector(pos_x, pos_y, Sector::WALL);
				}
			}
			
			++pos_y;
		}
		pos_y = 0;
		++pos_x;
	}
	
	makeMaze();
}



std::ostream& operator<<(std::ostream& os, const Maze& o)
{
	os << o.nbSectorsH << " " << o.nbSectorsV << "\n";
	
	Maze::SectorPtrVector removable_walls;
	for (Maze::SectorVector2d::const_iterator it = o.sectors.begin(); it != o.sectors.end(); ++it) {
		for (Maze::SectorVector::const_iterator it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
			os << *it2 << "\n";
		}
	}
	
	return os;
}



std::istream& operator>>(std::istream& is, Maze& o)
{
	is >> o.nbSectorsH >> o.nbSectorsV;
	
	//Donner tout de suite au tableau 2d des secteurs sa taille finale
	o.sectors.resize(o.nbSectorsH);
	for (Maze::SectorVector2d::iterator it = o.sectors.begin(); it != o.sectors.end(); ++it) {
		(*it).resize(o.nbSectorsV);
	}
	
	for (Maze::SectorVector2d::iterator it = o.sectors.begin(); it != o.sectors.end(); ++it) {
		for (Maze::SectorVector::iterator it2 = (*it).begin(); it2 != (*it).end(); ++it2) {
			is >> (*it2);
			(*it2).setParent(&o);
			if((*it2).getKind() == Sector::EXIT) {
				o.exit = &(*it2);
			} else if((*it2).getKind() == Sector::ENTRANCE) {
				o.entrance = &(*it2);
			}
		}
	}
	
	return is;
}



void Maze::makeMaze() {
	
	//Faire une liste de tous les murs qui séparent des secteurs-passages
	SectorPtrVector removable_walls;
	for (SectorVector2d::iterator it = sectors.begin() + 1; it != sectors.end() - 1; ++it) {
		for (SectorVector::iterator it2 = (*it).begin() + 1; it2 != (*it).end() - 1; ++it2) {
			if((*it2).getMazePosX() % 2 != (*it2).getMazePosY() % 2) {
				removable_walls.push_back(&(*it2));
			}
		}
	}
	//Un peu de poudre magique
	std::random_shuffle(removable_walls.begin(), removable_walls.end());
	
	
	//Créer des ensembles de secteurs-passages.
	//Chaque ensemble représente des secteurs-passages connectés.
	//Utiliser std::list, parce qu'on va faire beaucoup de suppressions,
	//à mesure qu'on fusionne les ensembles.
	std::list < SectorPtrVector > cell_sets;
	for (SectorVector2d::iterator it = sectors.begin() + 1; it != sectors.end() - 1; ++it) {
		for (SectorVector::iterator it2 = (*it).begin() + 1; it2 != (*it).end() - 1; ++it2) {
			if((*it2).getKind() == Sector::PASSAGE) {
				//Au début, tous les secteurs-passages sont isolés. Chacun est seul dans son ensemble.
				cell_sets.push_back(SectorPtrVector(1, &(*it2)));
			}
		}
	}
	
	//Parcourir tous les murs séparant des secteurs-passages.
	//Si les secteurs-passages ne sont pas connectés (s'il n'appartiennent pas
	//au même ensemble), retirer le mur et fusionner les ensembles où se trouvent
	//les deux secteurs-passages.
	SectorPtrVector remaining_walls;
	for (SectorPtrVector::iterator it = removable_walls.begin(); it != removable_walls.end(); ++it) {
		Sector * wall = *it;
		
		//Trouver les secteurs-passages que séparent le mur
		Sector * cell1;
		Sector * cell2;
		if (1 == (wall->getMazePosY() % 2)) {
			cell1 = getSector(wall->getMazePosX() - 1, wall->getMazePosY());
			cell2 = getSector(wall->getMazePosX() + 1, wall->getMazePosY());
		}
		else {
			cell1 = getSector(wall->getMazePosX(), wall->getMazePosY() - 1);
			cell2 = getSector(wall->getMazePosX(), wall->getMazePosY() + 1);
		}
		
		//Trouver les ensembles où sont les deux secteurs-passages
		SectorPtrVector * set1 = 0;
		SectorPtrVector * set2 = 0;
		for (std::list < SectorPtrVector >::iterator itt = cell_sets.begin();
			 itt != cell_sets.end() && (set1 == 0 || set2 == 0);
			 ++itt) {
			std::vector < Sector * >::iterator found1 = std::find((*itt).begin(), (*itt).end(), cell1);
			std::vector < Sector * >::iterator found2 = std::find((*itt).begin(), (*itt).end(), cell2);
			
			if (found1 != (*itt).end()) {
				set1 = &(*itt);
			}
			
			if (found2 != (*itt).end()) {
				set2 = &(*itt);
			}
		}
		
		//Si les deux secteurs-passages ne sont pas déjà connectés, on les connecte.
		if (set1 != set2) {
			//Enlever le mur
			Sector cell = newSector(wall->getMazePosX(), wall->getMazePosY(), Sector::PASSAGE);
			sectors[cell.getMazePosX()][cell.getMazePosY()] = cell;
			
			//Fusionner les deux ensembles
			for (std::vector<Sector*>::iterator it = set2->begin(); it != set2->end(); ++it) {
				set1->push_back(*it);
			}
			cell_sets.remove(*set2);
		}
		else {
			//Considérer ce mur pour transformation
			remaining_walls.push_back(wall);
		}

		//Change some walls into boulders
		int ii = 0;
		
		if(ii < remaining_walls.size()) {
			remaining_walls[ii++]->setKind(Sector::RED_POTION);
		}

		if(ii < remaining_walls.size()) {
			remaining_walls[ii++]->setKind(Sector::BLUE_POTION);
		}

		if(ii < remaining_walls.size()) {
			remaining_walls[ii++]->setKind(Sector::TREASURE);
		}

		float portion_boulders = 0.2;
		int nb_boulders = portion_boulders * remaining_walls.size();
		for (int jj = 0; jj < nb_boulders && ii < remaining_walls.size(); ++jj) {
			remaining_walls[ii++]->setKind(Sector::BOULDER);
		}
		
		int nb_lights = 4;
		for (int jj = 0; jj < nb_lights && ii < remaining_walls.size(); ++jj) {
			remaining_walls[ii++]->setKind(Sector::LIGHT);
		}
	}
}



Sector Maze::newSector(int pos_x, int pos_y, Sector::Kind kind)
{ return Sector(pos_x, pos_y, kind, this); }


Maze::~Maze()
{ }

Sector * Maze::getRandomPassableSector() {
	SectorPtrVector passages;
	for (SectorVector2d::iterator it = sectors.begin() + 1; it != sectors.end() - 1; ++it) {
		for (SectorVector::iterator it2 = (*it).begin() + 1; it2 != (*it).end() - 1; ++it2) {
			if ((*it2).isPassable()) {
				passages.push_back(&(*it2));
			}
		}
	}

	if (0 == passages.size()) {
		return 0;
	}



	int num = std::rand() % passages.size();

	return passages[num];
}