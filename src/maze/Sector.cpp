/**
* \file Sector.cpp
* \brief Permets de creer un secteur dans un labyrinthe
* \author Equipe 5
* \version 1.0
*
*/
#include "Sector.h"
#include <stdexcept>



Sector::Sector(int _pos_x, int _pos_y, Kind _kind, Maze * _parent):
posX(_pos_x), posY(_pos_y), kind(_kind), parent(_parent)
{}



Sector::Sector() :
posX(0), posY(0), kind(Sector::EMPTY), parent(0)
{}



Sector::Sector(const Sector & other) :
posX(other.posX), posY(other.posY), kind(other.kind), parent(other.parent)
{}



Sector & Sector::operator=(const Sector & other) {
	if (&other != this) {
		posX = other.posX;
		posY = other.posY;
		kind = other.kind;
		parent = other.parent;
	}
	
	return *this;
}



Sector::~Sector()
{}



std::ostream& operator<<(std::ostream& os, const Sector& o)
{
	os << o.posX << " " << o.posY << " " << o.kind;
	
	return os;
}



std::istream& operator>>(std::istream& is, Sector& o)
{
	is >> o.posX >> o.posY;
	
	int _kind;
	is >> _kind;
	o.kind = static_cast<Sector::Kind>(_kind);
	
	return is;
}



bool Sector::isPassable() const {

	//Pour eviter un bug si un utilisateur construit un 
	//labyrinthe sans murs
	if (this == NULL)
		throw std::out_of_range("Hors du labyrinthe!");
	return kind == PASSAGE || kind == LIGHT;
}
