/**
* \file Maze.h
* \brief Implémentation du labyrinthe (permets de creer un labyrinthe aleatoire)
* \author Equipe 5
* \version 1.0
*
*/

#pragma once

#include "Sector.h"
#include <vector>
#include <iostream>

/**
 * \brief Un labyrinthe 2d composé de secteurs carrés.
 *
 * Peut être passé à stream C++ d'entrée ou de sortie.
 */
class Maze
{
public:
	/**
	 * \brief Constructeur
	 *
	 * \param nb_sector_h Nombre de secteurs sur l'axe horizontal. DOIT ÊTRE IMPAIR
	 * \param nb_sector_h Nombre de secteurs sur l'axe vertical.   DOIT ÊTRE IMPAIR
	 */
	Maze(const int nb_sectors_h, const int nb_sectors_v);
	Maze();
	~Maze();
	
	friend std::ostream& operator<<(std::ostream& os, const Maze& s);
	friend std::istream& operator>>(std::istream& is, Maze& s);
	
	int getNbSectorsH()
	{ return nbSectorsH; }
	
	int getNbSectorsV()
	{ return nbSectorsV; }

	Sector * getSector(int x, int y)
	{ return &(sectors[x][y]); }
	
	Sector * getEntrance()
	{ return entrance; }
	
	Sector * getExit()
	{ return exit; }
	
	Sector * getRandomPassableSector();
	
private:
	int nbSectorsH; ///< Nombre de secteurs sur l'axe horizontal
	int nbSectorsV; ///< Nombre de secteurs sur l'axe vertical
	
	
	
	Sector * entrance = 0; ///< Secteur d'entrée du labyrinthe.
	Sector * exit = 0;     ///< Secteur de sortie du labyrinthe. Il n'y en a qu'un, pour l'instant.
	std::vector<std::vector<Sector> > sectors; ///< Secteurs composant le labyrinthe.
	
	
	
	/**
	 * \brief Génère un labyrinthe aléatoire.
	 *
	 * Implémentation randomisée de l'algorithme de Kruskal
	 * http://en.wikipedia.org/wiki/Maze_generation_algorithm#Randomized_Kruskal.27s_algorithm
	 */
	void makeMaze();
	
	
	/**
	 * \brief Alloue la mémoire pour un nouveau secteur à la position donnée.
	 */
	Sector newSector(int pos_x, int pos_y, Sector::Kind);

	typedef std::vector<std::vector<Sector> > SectorVector2d;
	typedef std::vector<Sector> SectorVector;
	typedef std::vector<Sector *> SectorPtrVector;
};

