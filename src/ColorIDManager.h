/**
* \file ColorIDManager.h
* \brief Permets de determiner la couleur d'un secteur pour la detection des clics.
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include "ofMain.h"
#include "maze/Maze.h"

/**
* \class ColorIDManager
* \brief Pour la gestion des secteurs par couleurs.
*
*/
class ColorIDManager
{
public:
	/**
	* \brief Constructeur sans param�tres
	*/
	ColorIDManager();
	/**
	* \brief Destructeur
	*/
	~ColorIDManager();

	/**
	* \brief D�termine la position
	* \param Sector Pointer vers un secteur
	* \return La position sous forme de vecteur
	*/
	ofVec3f getID(Sector * sector);


	/**
	* \brief D�termine la position
	* \param color_id La position de la couleur � tester
	* \param Un labyrinthe a tester
	* \return Un pointeur vers le secteur
	*/
	Sector * getSector(ofVec3f color_id, Maze * maze);
};

