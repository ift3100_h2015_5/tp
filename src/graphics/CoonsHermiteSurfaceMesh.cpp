/**
* \file CoonsHermiteSurfaceMesh.cpp
* \brief Gestion du maillage des surface d'hermite
* \author Equipe 5
* \version 1.0
*
*/

#include "CoonsHermiteSurfaceMesh.h"



int CoonsHermiteSurfaceMesh::stride = 8;



CoonsHermiteSurfaceMesh::CoonsHermiteSurfaceMesh(int _precision) :
precision(_precision), bufferdata(0), VAO(0), VBO(0) {
	nbVertices = precision * precision * 3 * 2;
}



CoonsHermiteSurfaceMesh::~CoonsHermiteSurfaceMesh() {
	if(0 != bufferdata) {
		delete []bufferdata;
	}
}



nosmaths::CoonsHermiteSurface & CoonsHermiteSurfaceMesh::getSurfaceRef()
{ return surface; }



void CoonsHermiteSurfaceMesh::setup() {
	if (VAO != 0) return;

	int vertice_size = stride * sizeof(GLfloat);
	bufferdataSize = nbVertices * vertice_size;
	bufferdata = new GLfloat[bufferdataSize];
	
	for(int ii = 0; ii < bufferdataSize; ++ii) {
		bufferdata[ii] = 0;
	}
	
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
	glBindVertexArray(VAO);
	
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, bufferdataSize, bufferdata, GL_DYNAMIC_DRAW);
	
	// Position attribute
	glVertexAttribPointer(ofShader::POSITION_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, vertice_size, (GLvoid*)0);
	glEnableVertexAttribArray(ofShader::POSITION_ATTRIBUTE);
	
	// Normals attribute
	glVertexAttribPointer(ofShader::NORMAL_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, vertice_size, (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(ofShader::NORMAL_ATTRIBUTE);
	
	// Texcoords attribute
	glVertexAttribPointer(ofShader::TEXCOORD_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, vertice_size, (GLvoid*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(ofShader::TEXCOORD_ATTRIBUTE);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind
	
	glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs)
}



void CoonsHermiteSurfaceMesh::update() {
	float du = 1.0f / (float)precision;
	float dv = 1.0f / (float)precision;

	//Tableau 2D de pointeurs sur les normales.
	//Chaque entr�e du tableau est une liste de pointeurs
	//sur les normales des sommets se trouvant au m�me endroit.
	//Sert � faire une moyenne de ces normales et obtenir une surface lisse.
	std::vector  < std::vector < std::vector < GLfloat * > > > normal_locations;
	normal_locations.resize(precision + 1);
	for (std::vector  < std::vector < std::vector < GLfloat * > > >::iterator it = normal_locations.begin();
		 it != normal_locations.end(); ++it) {
		it->resize(precision + 1);
	}
	
	GLfloat * ptr = bufferdata;
	for (int ii = 0; ii < precision; ++ii) { // x left(0) to right(1)
		for (int jj = 0; jj < precision; ++jj) {  // y top(0) to bottom(1)
			
			ofPoint pt1 = surface.getPoint((ii + 1)*du, jj    *dv);
			ofPoint pt1tex((float)(ii + 1)/precision, (float)jj/precision); // top right
			
			ofPoint pt2 = surface.getPoint(ii    *du, jj    *dv);
			ofPoint pt2tex((float)ii/precision, (float)jj/precision); // top left
			
			ofPoint pt3 = surface.getPoint(ii    *du, (jj + 1)*dv);
			ofPoint pt3tex((float)ii/precision, (float)(jj + 1)/precision); // bottom left
			
			ofPoint pt4 = surface.getPoint((ii + 1)*du, (jj + 1)*dv);
			ofPoint pt4tex((float)(ii+1)/precision, (float)(jj + 1)/precision); // bottom right
			
			//TRIANGLE 1
			ofPoint normal1 = (pt2 - pt1).cross(pt3 - pt1).normalize();
			
			//coord
			ptr[0] = pt1.x;
			ptr[1] = pt1.y;
			ptr[2] = pt1.z;
			//normal
			ptr[3] = normal1.x;
			ptr[4] = normal1.y;
			ptr[5] = normal1.z;
			normal_locations[ii + 1][jj].push_back(ptr + 3);
			
			ptr[6] = pt1tex.x;
			ptr[7] = pt1tex.y;
			
			ptr += stride;
			
			//coord
			ptr[0] = pt2.x;
			ptr[1] = pt2.y;
			ptr[2] = pt2.z;
			//normal
			ptr[3] = normal1.x;
			ptr[4] = normal1.y;
			ptr[5] = normal1.z;
			normal_locations[ii][jj].push_back(ptr + 3);
			
			ptr[6] = pt2tex.x;
			ptr[7] = pt2tex.y;
			
			ptr += stride;
			
			//coord
			ptr[0] = pt3.x;
			ptr[1] = pt3.y;
			ptr[2] = pt3.z;
			//normal
			ptr[3] = normal1.x;
			ptr[4] = normal1.y;
			ptr[5] = normal1.z;
			normal_locations[ii][jj + 1].push_back(ptr + 3);
			
			ptr[6] = pt3tex.x;
			ptr[7] = pt3tex.y;
			
			ptr += stride;
			
			//TRIANGLE 2
			ofPoint normal2 = (pt3 - pt1).cross(pt4 - pt1).normalize();
			
			//coord
			ptr[0] = pt1.x;
			ptr[1] = pt1.y;
			ptr[2] = pt1.z;
			//normal
			ptr[3] = normal2.x;
			ptr[4] = normal2.y;
			ptr[5] = normal2.z;
			normal_locations[ii + 1][jj].push_back(ptr + 3);
			
			ptr[6] = pt1tex.x;
			ptr[7] = pt1tex.y;
			
			ptr += stride;
			
			//coord
			ptr[0] = pt3.x;
			ptr[1] = pt3.y;
			ptr[2] = pt3.z;
			//normal
			ptr[3] = normal2.x;
			ptr[4] = normal2.y;
			ptr[5] = normal2.z;
			normal_locations[ii][jj + 1].push_back(ptr + 3);
			
			ptr[6] = pt3tex.x;
			ptr[7] = pt3tex.y;
			
			ptr += stride;
			
			//coord
			ptr[0] = pt4.x;
			ptr[1] = pt4.y;
			ptr[2] = pt4.z;
			//normal
			ptr[3] = normal2.x;
			ptr[4] = normal2.y;
			ptr[5] = normal2.z;
			normal_locations[ii + 1][jj + 1].push_back(ptr + 3);
			
			ptr[6] = pt4tex.x;
			ptr[7] = pt4tex.y;
			
			ptr += stride;
			
		}
	}
	
	//Faire la moyenne des normales des sommets se trouvant au m�me endroit
	for (std::vector < std::vector < std::vector < GLfloat * > > >::iterator it = normal_locations.begin();
		 it != normal_locations.end();
		 ++it) {
		for (std::vector < std::vector < GLfloat * > >::iterator itt = it->begin();
			 itt != it->end();
			 ++itt) {
			
			GLfloat sum_x = 0, sum_y = 0, sum_z = 0;
			
			for (std::vector < GLfloat * >::iterator ittt = itt->begin();
				 ittt != itt->end();
				 ++ittt) {
				sum_x += *(*ittt);
				sum_y += *((*ittt)+1);
				sum_z += *((*ittt)+2);
			}
			
			GLfloat avg_x = sum_x / itt->size();
			GLfloat avg_y = sum_y / itt->size();
			GLfloat avg_z = sum_z / itt->size();
			
			for (std::vector < GLfloat * >::iterator ittt = itt->begin();
				 ittt != itt->end();
				 ++ittt) {
				*(*ittt) = avg_x;
				*((*ittt) + 1) = avg_y;
				*((*ittt) + 2) = avg_z;
			}
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, bufferdataSize, bufferdata);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}



void CoonsHermiteSurfaceMesh::draw() {
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, nbVertices);
	glBindVertexArray(0);
}