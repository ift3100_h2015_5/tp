/**
* \file CoonsHermiteSurfaceMesh.h
* \brief Gestion du maillage des surface d'hermite
* \author Equipe 5
* \version 1.0
*
*/

#pragma once

#include "../nosmaths/CoonsHermiteSurface.h"



/**
 * \brief Maillage géométrique produit à partir d'une surface de Coons.
 * 
 * La surface de Coons est définie à partir de quatre courbe de Hermite.
 */
class CoonsHermiteSurfaceMesh {
public:

	/**
	 * \brief Constructeur
	 * 
	 * \param _precision Nombre de sommet par axe.
	 */
	CoonsHermiteSurfaceMesh(int _precision);

	/**
	 * \brief Destructeur
	 * 
	 * Libère la mémoire de stockage du maillage
	 */
	~CoonsHermiteSurfaceMesh();
	
	/**
	 * \brief Créer un Vertex Array Object sur le GPU.
	 *
	 * Le VAO n'est encore rempli par le maillage géométrique.
	 */
	void setup();

	/**
	 * \brief Produit un nouveau maillage, selon la réprésentation
	 *    mathématique de la surface et le verse dans la VAO.
	 */
	void update();

	/**
	 * \brief Dessine le VAO.
	 */
	void draw();
	
	/**
	 * \brief Retourne une référence à la représentation mathématique de la surface.
	 *
	 * Permet de modifier la surface.
	 */
	nosmaths::CoonsHermiteSurface & getSurfaceRef();
private:
	static int stride; ///< Nombre de données par sommet
	
	nosmaths::CoonsHermiteSurface surface; ///< Représentation mathématique de la surface
	
	int precision; ///< Nombre de point d'échantillon par axe
	
	int nbVertices; ///< Dépend de precision
	int bufferSize; ///< Dépend de stride et nbVertices
	
	GLfloat * bufferdata; ///< Espace mémoire intermédiaire pour passer les sommets au GPU
	int bufferdataSize; ///< Nombre de floats dans bufferdata.
	
	GLuint VBO, VAO; ///< Référence au Vertexr Buffer Object et au Vertex Array Object
};