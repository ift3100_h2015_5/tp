/**
* \file OBJWaveFrontLoader.cpp
* \brief Pour la chargement d'un fichier de type wavefront (.obj).
* \author Equipe 5
* \version 1.0
*
*/

#include "OBJWaveFrontLoader.h"
#include <fstream>
#include <sstream>



OBJWaveFrontLoader::~OBJWaveFrontLoader() {
	for (std::map<std::string, Material *>::iterator it = materials.begin(); it != materials.end(); ++it) {
		delete it->second;
	}

	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
}



void OBJWaveFrontLoader::load(const std::string & filename, bool override_normals) {
	std::vector<ofVec3f> vertices;
	std::vector<ofVec2f> texcoords;
	std::vector<ofVec3f> normals;
	std::vector<GLfloat> bufferData;

	std::string path = ofFilePath::getAbsolutePath(filename);
	std::ifstream infile(path.c_str(), std::ifstream::in);

	std::string line;
	while (std::getline(infile, line)) {
		std::istringstream iss(line);
		std::string command;

		if (!(iss >> command)) continue;

		//Librairie des matériaux
		if (command == "mtllib") {
			std::string enclosing_path = ofFilePath::getEnclosingDirectory(path);
			std::string partial_mtl_path;
			iss >> partial_mtl_path;
			loadMtlLib(enclosing_path + partial_mtl_path);
		}

		//Sommets
		if (command == "v") {
			ofVec3f v;
			while (iss >> v.x >> v.y >> v.z) {
				vertices.push_back(v);
			}

			//Déterminer quel espace le modèle 3D occupe approximativement
			if (v.x < minsBoundingBox.x) minsBoundingBox.x = v.x;
			if (v.y < minsBoundingBox.y) minsBoundingBox.y = v.y;
			if (v.z < minsBoundingBox.z) minsBoundingBox.z = v.z;

			if (v.x > maxsBoundingBox.x) maxsBoundingBox.x = v.x;
			if (v.y > maxsBoundingBox.y) maxsBoundingBox.y = v.y;
			if (v.z > maxsBoundingBox.z) maxsBoundingBox.z = v.z;
		}

		//Coordonnées texture
		if (command == "vt") {
			ofVec2f vt;
			while (iss >> vt.x >> vt.y) {
				texcoords.push_back(vt);
			}
		}

		//Normales
		if (command == "vn") {
			ofVec3f vn;
			while (iss >> vn.x >> vn.y >> vn.z) {
				normals.push_back(vn);
			}
		}

		//Nouvelle étape de rendu
		if (command == "usemtl") {
			std::string name;
			iss >> name;
			Material * material = materials[name];
			RenderStep node;
			node.material = material;
			node.nbVertex = 0;
			node.startVertexIndex = renderPlan.size() > 0 ?
				renderPlan.back().startVertexIndex + renderPlan.back().nbVertex :
				0;
			renderPlan.push_back(node);
		}

		//Faces
		if (command == "f") {
			std::string vertice_attrs;

			std::vector< ofVec3f > face_vertices; ///< for building own vertices

			int nb_vertex = 0;
			while (iss >> vertice_attrs) {
				++nb_vertex;
				renderPlan.back().nbVertex += 1;

				if (nb_vertex > 3) {
					//Flute! Des quads.
					//- répeter le sommet précédent

					//copier 8 (stride) donnees dans un array a partir de bufferdata.size() - 8
					GLfloat vertice[bufferDataStride];
					int offset = bufferData.size() - bufferDataStride;

					for (int ii = 0; ii < bufferDataStride; ++ii) {
						vertice[ii] = bufferData[offset + ii];
					}

					//copier les donnees dans bufferdata
					for (int ii = 0; ii < bufferDataStride; ++ii) {
						bufferData.push_back(vertice[ii]);
					}

					++nb_vertex;
					renderPlan.back().nbVertex += 1;
				}

				std::istringstream splitter(vertice_attrs);
				std::string attr;
				std::vector<std::string> split_attrs;
				while (std::getline(splitter, attr, '/')) {
					split_attrs.push_back(attr);
				}


				//VERTICE

				int vertex_index = atoi(split_attrs[0].c_str());
				ofVec3f vertex = vertices[vertex_index - 1];
				bufferData.push_back(vertex.x);
				bufferData.push_back(vertex.y);
				bufferData.push_back(vertex.z);
				face_vertices.push_back(vertex);


				//TEXCOORD

				int texcoord_index = atoi(split_attrs[1].c_str());
				ofVec2f texcoord = texcoords[texcoord_index - 1];
				//http://stackoverflow.com/questions/22465212/mapping-wavefront-obj-texture-vertex-on-opengl
				GLfloat vt_x = fmod(texcoord.x, 1.0f);
				GLfloat vt_y = fmod(texcoord.y, 1.0f);
				if (vt_x < 0) vt_x = fabs(vt_x);//vt_x += 1.0f;
				if (vt_y < 0) vt_y = fabs(vt_y);//vt_y += 1.0f;

				bufferData.push_back(vt_x);
				bufferData.push_back(vt_y);


				//NORMAL
				if (!override_normals) {
					int normal_index = atoi(split_attrs[2].c_str());
					ofVec3f normal = normals[normal_index - 1];

					bufferData.push_back(normal.x);
					bufferData.push_back(normal.y);
					bufferData.push_back(normal.z);
				}
				else {
					bufferData.push_back(0);
					bufferData.push_back(0);
					bufferData.push_back(0);
				}

				if (nb_vertex > 3) {
					//Flute! Des quads.
					//- répeter le sommet précédent

					//copier 8 (stride) donnees dans un array a partir de bufferdata.size() - 8 * 5
					GLfloat vertice[bufferDataStride];
					int offset = bufferData.size() - bufferDataStride * 5;

					for (int ii = 0; ii < bufferDataStride; ++ii) {
						vertice[ii] = bufferData[offset + ii];
					}

					//copier les donnees dans bufferdata
					for (int ii = 0; ii < bufferDataStride; ++ii) {
						bufferData.push_back(vertice[ii]);
					}

					++nb_vertex;
					renderPlan.back().nbVertex += 1;
				}
			}

			//Make own normals
			if (override_normals) {
				ofVec3f normal = (face_vertices[1] - face_vertices[0]).cross(face_vertices[2] - face_vertices[0]);
				for (int ii = bufferData.size() - bufferDataStride * nb_vertex;
					ii < bufferData.size();
					ii += bufferDataStride) {
					bufferData[ii + 5] = normal.x;
					bufferData[ii + 6] = normal.y;
					bufferData[ii + 7] = normal.z;
				}
			}
		}
	}

	infile.close();

	//Sommets

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);

	//Envoyer les données
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, bufferData.size() * sizeof(GLfloat), &bufferData[0], GL_STATIC_DRAW);

	//Indiquer quoi faire de ces donnees
	// Position attribute
	glVertexAttribPointer(ofShader::POSITION_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(ofShader::POSITION_ATTRIBUTE);
	// TexCoord attribute
	glVertexAttribPointer(ofShader::TEXCOORD_ATTRIBUTE, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(ofShader::TEXCOORD_ATTRIBUTE);
	// Normals attribute
	glVertexAttribPointer(ofShader::NORMAL_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(5 * sizeof(GLfloat)));
	glEnableVertexAttribArray(ofShader::NORMAL_ATTRIBUTE);

	glBindVertexArray(0);
}



void OBJWaveFrontLoader::loadMtlLib(const std::string & full_path) {
	std::ifstream mtlfile(full_path.c_str());

	Material * mat = 0;

	std::string line;
	while (std::getline(mtlfile, line)) {
		std::istringstream iss(line);
		std::string command;

		if (!(iss >> command)) continue;

		if ("newmtl" == command) {
			mat = new Material();
			std::string name;
			iss >> name;
			mat->name = name;
			materials[name] = mat;
		}

		if ("map_Kd" == command) {
			std::string filename;
			iss >> filename;
			std::string texture_path = ofFilePath::getEnclosingDirectory(full_path) + filename;
			mat->texture.loadImage(texture_path);
		}
	}

	mtlfile.close();
}



void OBJWaveFrontLoader::draw(bool skip_textures) const {
	glBindVertexArray(vao);
	for (std::vector< RenderStep >::const_iterator it = renderPlan.begin(); it != renderPlan.end(); ++it) {
		if(!skip_textures) {
			it->material->texture.getTextureReference().bind();
		}
		glDrawArrays(GL_TRIANGLES, it->startVertexIndex, it->nbVertex);
		if(!skip_textures) {
			it->material->texture.getTextureReference().unbind();			
		}
	}
	glBindVertexArray(0);
}


