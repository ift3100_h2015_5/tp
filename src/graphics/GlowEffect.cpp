/**
* \file GlowEffect.cpp
* \brief Permets de faire des effets de bloom.
* \author Equipe 5
* \version 1.0
*
*/

#include "GlowEffect.h"

const float GlowEffect::viewportRatio = 1.0f / 2.0f;



GlowEffect::GlowEffect() :sceneFB(0), glowFB(0), glowBlurH(0), glowBlurV(0)
{ }



GlowEffect::~GlowEffect() {
	if(0 != sceneFB) {
		delete sceneFB;
	}
	
	if(0 != glowFB) {
		delete glowFB;
	}

	if (0 != glowBlurH) {
		delete glowBlurH;
	}

	if (0 != glowBlurV) {
		delete glowBlurV;
	}
}



void GlowEffect::setup() {
	sceneFB = new Framebuffer(ofGetViewportWidth(), ofGetViewportHeight());
	sceneFB->setup();
	glowFB = new Framebuffer(ofGetViewportWidth() * viewportRatio, ofGetViewportHeight() * viewportRatio);
	glowFB->setup();

	additive.load("shaders/specialEffects/id.vert", "shaders/specialEffects/additive.frag");
	blurHShader.load("shaders/specialEffects/blurh.vert", "shaders/specialEffects/blur.frag");
	blurVShader.load("shaders/specialEffects/blurv.vert", "shaders/specialEffects/blur.frag");

	glowBlurH = new FullScreenEffect();
	glowBlurH->setup(blurHShader, -1);
	glowBlurV = new FullScreenEffect();
	glowBlurV->setup(blurVShader, -1);
}



void GlowEffect::rebuildFramebuffers(int w, int h) {
	if(0 != sceneFB) {
		delete sceneFB;
	}
	
	if(0 != glowFB) {
		delete glowFB;
	}
	
	sceneFB = new Framebuffer(w, h);
	sceneFB->setup();
	
	glowFB = new Framebuffer(w * viewportRatio, h * viewportRatio);
	glowFB->setup();
	
	glowBlurH->rebuildFramebuffer(w, h);
	glowBlurV->rebuildFramebuffer(w, h);
}



void GlowEffect::beginScene()
{ sceneFB->begin(); }



void GlowEffect::endScene()
{ sceneFB->end(); }



void GlowEffect::beginGlow()
{ glowBlurH->begin(); }



void GlowEffect::endGlow() {
	glowBlurH->end();

	glowBlurV->begin();
	glowBlurH->draw();
	glowBlurV->end();

	glowFB->begin();
	glowBlurV->draw();
	glowFB->end();
}



void GlowEffect::draw() {
	additive.begin();
	sceneFB->draw(additive, glowFB->getTexture());	
	additive.end();
}