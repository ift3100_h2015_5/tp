/**
* \file GraphicSelector.cpp
* \brief Permets d'implementer la selection graphique a partir d'un FBO
* \author Equipe 5
* \version 1.0
*
*/
#include "GraphicSelector.h"
#include "../nosmaths/nosmaths.h"


GraphicSelector::GraphicSelector() :
selectFramebuffer(0)
{ }



GraphicSelector::~GraphicSelector()
{ }



void GraphicSelector::setup() {
	if(0 != selectFramebuffer) {
		delete selectFramebuffer;
	}
	
	selectFramebuffer = new Framebuffer(ofGetViewportWidth(), ofGetViewportHeight());
	selectFramebuffer->setup();
}



void GraphicSelector::rebuildFramebuffer(int w, int h) {
	if (0 != selectFramebuffer) {
		delete selectFramebuffer;
	}

	selectFramebuffer = new Framebuffer(w, h);
	selectFramebuffer->setup();
}



Sector * GraphicSelector::findSector(int x, int y, scenegraph::NodePtr scene, Maze * maze) {
	y = ofGetViewportHeight() - y;

	//Dessiner seulement un carré de 4 par 4 pixels autour du clic
	int scissor_size = 4;

	selectFramebuffer->begin();
	glEnable(GL_SCISSOR_TEST);
	glScissor(x - scissor_size/2, y - scissor_size/2, scissor_size, scissor_size);
	scene->draw();
	glDisable(GL_SCISSOR_TEST);
	selectFramebuffer->end();

	GLfloat color_clicked[3];
	selectFramebuffer->readPixels(x, y, 1, 1, GL_RGB, GL_FLOAT, color_clicked);

	ofVec3f color(color_clicked[0], color_clicked[1], color_clicked[2]);

	return colorIDManager.getSector(color, maze);
}

std::vector <Sector *> GraphicSelector::findSectors(ofRectangle rect, scenegraph::NodePtr scene, Maze * maze) {
	
	if(nosmaths::closeEnough(0, rect.width, 0.0001)) {
		rect.width = 1;
	}
	
	if(nosmaths::closeEnough(0, rect.height, 0.0001)) {
		rect.height = 1;
	}
	
	rect.y = ofGetViewportHeight() - rect.y;
	rect.y -= rect.height;
	
	
	selectFramebuffer->begin();
	glEnable(GL_SCISSOR_TEST);
	glScissor(rect.x, rect.y, rect.width, rect.height);
	scene->draw();
	glDisable(GL_SCISSOR_TEST);
	selectFramebuffer->end();
	
	int buffer_size = 3 * (int)(rect.width) * (int)(rect.height);
	GLfloat * colors_clicked = new GLfloat[buffer_size];
	selectFramebuffer->readPixels(rect.x, rect.y, rect.width, rect.height, GL_RGB, GL_FLOAT, colors_clicked);

	std::vector <ofVec3f> unique_colors;
	std::vector <Sector *> sectors;
	
	int skipped_pixels = 6; ///< Mesure d'optimisation : ne pas échantillonner toutes les pixels
	for(int ii = 0; ii < buffer_size; ii += 3 * skipped_pixels) {
		ofVec3f color(colors_clicked[ii], colors_clicked[ii+1], colors_clicked[ii+2]);
		bool found = false;
		for(std::vector <ofVec3f>::iterator it = unique_colors.begin();
			it != unique_colors.end() && !found;
			++it) {
			
			if(nosmaths::closeEnough(color.x, it->x, 0.0001f) &&
			   nosmaths::closeEnough(color.y, it->y, 0.0001f) &&
			   nosmaths::closeEnough(color.z, it->z, 0.0001f)) {
				found = true;
			}
		}
		
		if(!found) {
			unique_colors.push_back(color);
			Sector * sector = colorIDManager.getSector(color, maze);
			if (0 != sector) {
				sectors.push_back(sector);
			}
		}
	}

	delete []colors_clicked;
	
	return sectors;
}