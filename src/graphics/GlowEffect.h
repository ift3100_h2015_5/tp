/**
* \file GlowEffect.h
* \brief Permets de faire des effets de bloom.
* \author Equipe 5
* \version 1.0
*
*/

#pragma once

#include "ofMain.h"
#include "Framebuffer.h"
#include "FullScreenEffect.h"

/**
 * \brief Responsable de l'effet de glow (Bloom Effect) du mode Simulation.
 *
 * Stocke le rendu de la sc�ne et le rendu des sources lumineuses
 * dans des framebuffers. Applique un effet de flou en deux passes
 * au rendu des sources lumineuses. Effectue le rendu final par addition
 * des couleurs des deux images.
 */
class GlowEffect {
	
public:
	static const float viewportRatio; ///< Facteur de r�duction du rendu des sources lumineuses, pour optimisation

	/**
	 * \brief Constructeur
	 */
	GlowEffect();

	/**
	 * \brief Destructeur, responsable de d�truire les effets et les framebuffers
	 */
	~GlowEffect();
	
	/**
	 * \brief Pr�paration des framebuffers, des effets et de leurs shaders
	 */
	void setup();

	/**
	 * \brief Reconstruit les framebuffers en fonction de nouvelles dimensions de fen�tre.
	 *
	 * \param w Nouvelle largeur de la fen�tre 
	 * \param h Nouvelle hauteur de la fen�tre 
	 */
	void rebuildFramebuffers(int w, int h);

	/**
	 * \brief D�marre la captation du rendu de la sc�ne dans le framebuffer appropri�.
	 */
	void beginScene();

	/**
	 * \brief Arr�te la captation du rendu de la sc�ne dans le framebuffer appropri�.
	 */
	void endScene();

	/**
	 * \brief D�marre la captation du rendu des sources lumineuses dans le framebuffer appropri�.
	 */
	void beginGlow();

	/**
 	 * \brief Termine la captation du rendu des sources lumineuses dans le framebuffer appropri�.
	 *
	 * Applique aussi un effet de flou en deux passes � ce rendu.
	 */
	void endGlow();
	
	/**
	 * \brief Dessine la sc�ne avec effet de glow sur les sources lumineuses.
	 *
	 * Effectu� en additionnant les textures r�sultant du framebuffer de sc�ne et du
	 * framebuffer de sources lumineuses.
	 */
	void draw();
	
private:
	ofShader additive; ///< Shader d'addition des textures des deux framebuffers
	ofShader blurHShader; ///< Shader de la passe horizontale du flou
	ofShader blurVShader; ///< Shader de la passe verticale du flou

	Framebuffer * sceneFB; ///< Framebuffer stockant le rendu de la sc�ne
	Framebuffer * glowFB; ///< Framebuffer stockant le rendu des sources lumineuses
	
	FullScreenEffect * glowBlurH; ///< Effet de la passe horizontale du flou
	FullScreenEffect * glowBlurV; ///< Effet de la passe verticale du flou
};