/**
* \file OBJWaveFrontLoader.h
* \brief Pour la chargement d'un fichier de type wavefront (.obj).
* \author Equipe 5
* \version 1.0
*
*/

#pragma once
#include <string>
#include "../ofMain.h"


/**
* \brief Gestionnaire d'un modèle 3D stocké dans un fichier de type WaveFront .OBJ,
*
* Chaque instance permet de charge un modèle 3D, de l'envoyer dans un Vertex Array Object,
* et d'en demander le rendu au GPU.
*/

class OBJWaveFrontLoader
{
public:

	typedef std::pair<ofVec3f, ofVec3f> BoundingBox;

	/**
	* \brief Constructeur par défaut
	*/
	OBJWaveFrontLoader() {}



	/**
	* \brief Destructeur
	*
	* Détruit le Vertex Array Object et le Vertex Buffer Object.
	*/

	~OBJWaveFrontLoader();



	/**
	* \brief Lit un fichier OBJ.
	*
	* Le contenu du fichier est chargé dans un VBO et attaché à un VAO.
	* Un plan de rendu indiquant quels matériaux pour quelles faces est créé.
	* Les textures indiquées dans les matériaux sont chargés dans des ofImage.
	*/
	void load(const std::string & filename, bool override_normals = false);



	/**
	* \brief Envoie une les commandes de rendu au GPU.
	*
	* Les textures sont passées au shader.
	*/
	void draw(bool skip_textures = false) const;



	/**
	* \brief Retourne les valeurs minimales et maximales du modèle sur chacun des axes
	*
	* \return Une paire de vecteurs. Le premier contient les minimumns; le second les maximums.
	*/

	BoundingBox getBoundingBox() const
	{
		return BoundingBox(minsBoundingBox, maxsBoundingBox);
	}

private:
	static const GLuint bufferDataStride = 8; ///< Nombre de GLfloat utilisés par sommet



	/**
	* \brief Un matériel tel que spécifié dans un OBJ
	*
	* \todo Tenir compte des paramètres d'illumination?
	*/
	class Material {
	public:
		std::string name;
		ofImage texture;
	};



	/**
	* \brief Une étape de rendu, telle que spécifiée dans un OBJ
	*/
	class RenderStep {
	public:
		Material * material;
		GLuint startVertexIndex; ///< Indice du premier sommet à rendre durant cette étape
		GLuint nbVertex; ///< Nombre de sommets à rendre durant cette étape
	};




	OBJWaveFrontLoader & operator=(const OBJWaveFrontLoader&); ///< Empêcher l'assignation
	OBJWaveFrontLoader(const OBJWaveFrontLoader&); ///< Empêcher la copie



	/**
	* \brief Lit un fichier .mtl, c'est-à-dire une libraire de matériaux
	*
	* Le résultat est stocké dans materials.
	*/
	void loadMtlLib(const std::string & full_path);



	GLuint vao = 0, vbo = 0;

	std::map< std::string, Material * > materials; ///< Matériaux trouvés dans la librairie de matériaux du OBJ
	std::vector< RenderStep > renderPlan; ///< Étapes de rendu (matériaux à utiliser pour chaque face)

	ofVec3f minsBoundingBox = ofVec3f(100000.0, 100000.0, 100000.0); ///< Valeur mimimales des sommets, pour chaque axe
	ofVec3f maxsBoundingBox = ofVec3f(-100000.0, -100000.0, -100000.0); ///< Valeur maximales des sommets, pour chaque axe
};