/**
* \file FullScreenEffect.h
* \brief Permets de faire des effets plein écran
* \author Equipe 5
* \version 1.0
*
*/

#pragma once

#include "ofMain.h"
#include "Framebuffer.h"

/**
 * \brief Effets plein écran
 *
 * Stocke le rendu OpenGL d'une scène dans un framebuffer plein écran.
 * Un shader appliqué à la texture sortante du framebuffer effectue l'effet plein écran.
 */
class FullScreenEffect
{
public:
	/**
	 * \brief Constructeur. N'effectue pas d'initialisation
	 */
	FullScreenEffect();

	/**
	 * \brief Destructeur. Libère le framebuffer.
	 */
	~FullScreenEffect();

	/**
	 * \brief Alloue la mémoire pour le framebuffer et le rectangle d'affichage
	 *
	 * \param shader Shader d'effet.
	 * \param secs_duration Détermine la durée en secondes de l'effet.
	 */
	void setup(ofShader shader, float secs_duration);

	/**
	 * \brief Idem que setup(shader, secs_duration), mais pour un effet à durée indéterminée.
	 *
	 * \param shader Shader d'effet.
	 */
	void setup(ofShader shader);

	/**
	* \brief Reconstruit les framebuffers en fonction de nouvelles dimensions de fenêtre.
	*
	* \param w Nouvelle largeur de la fenêtre
	* \param h Nouvelle hauteur de la fenêtre
	*/
	void rebuildFramebuffer(int w, int h);

	/**
	 * \brief Incrémente le compteur de temps écoulé.
	 *
	 * \param Temps écoulé en secondes, depuis le dernier appel de update().
	 */
	void update(GLfloat secs_elapsed);

	/**
	 * \brief Tout appel de dessin OpenGL suivant cet appel est effectué dans le framebuffer.
	 */
	void begin();

	/**
	 * \brief Arrêter de rediriger les appels dans le framebuffer.
	 */
	void end();

	/**
	 * \brief Dessiner le contenu du framebuffer à l'écran, traité par le shader.
	 */
	void draw();

	/**
	 * \brief L'effet est-il terminé?
	 * 
	 * \return Vrai si le nombre de secondes de l'effet est écoulé.
	 */
	bool done();

private:
	FullScreenEffect & operator=(const FullScreenEffect&); ///< Empêcher l'assignation
	FullScreenEffect(const FullScreenEffect&); ///< Empêcher la copie

	ofShader shader; ///< shader responsable de l'effet plein écran
	Framebuffer * framebuffer; ///< Framebuffer plein écran où le rendu à traiter est stocké.
	
	GLfloat secsDuration = -1.0f; ///< en secondes
	GLfloat secsSpent = 0.0f; ///< en secondes
};