/**
* \file Framebuffer.cpp
* \brief Permets de construire, gerer et dessiner un framebuffer
* \author Equipe 5
* \version 1.0
*
*/

#include "Framebuffer.h"

Framebuffer::Framebuffer(int _w, int _h) : w(_w), h(_h)
{ }


void Framebuffer::setup() {
	//Commencer à bâtir notre FBO
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	
	//Brancher le FBO à une texture
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
	
	//depth et stencil buffers
	GLuint rbo;
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	// Utiliser le même render object pour le depth et le stencil buffer.
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, w, h);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	//On les attache au FBO
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
	//Est-ce que le FBO est complet?
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << endl;
	}
	
	//FBO fin prêt! On retourne au rendu par défaut pour l'instant.
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	//Rectangle pour dessiner le FBO à l'écran
	GLfloat quadVertices[] = {
		// Positions   // TexCoords
		-1.0f, 1.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 0.0f,
		
		-1.0f, 1.0f, 0.0f, 1.0f,
		1.0f, -1.0f, 1.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f
	};
	
	//Préparer le Vertex Array Object du rectangle plein écran
	GLuint quadVBO;
	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glBindVertexArray(0);
}



void Framebuffer::begin() {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}



void Framebuffer::end() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}



void Framebuffer::draw(ofShader & shader, GLuint second_texture) {
	glClear(GL_COLOR_BUFFER_BIT);
	
	glBindVertexArray(quadVAO);

	shader.setUniformTexture("tex0", GL_TEXTURE_2D, texture, 0);

	if(0 != second_texture) {
		shader.setUniformTexture("tex1", GL_TEXTURE_2D, second_texture, 1);
	}

	glDrawArrays(GL_TRIANGLES, 0, 6);
	
	glBindVertexArray(0);
}

void Framebuffer::readPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid * data) {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glReadPixels(x, y, width, height, format, type, data);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


Framebuffer::~Framebuffer()
{
	glDeleteFramebuffers(1, &fbo);
	glDeleteTextures(1, &texture);
}

