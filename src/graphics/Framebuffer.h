/**
* \file Framebuffer.h
* \brief Permets de construire, gerer et dessiner un framebuffer
* \author Equipe 5
* \version 1.0
*
*/
#pragma once
#include "ofMain.h"

/**
 * \brief Gestionnaire pour un Framebuffer OpenGL
 *
 * Présentement, le Framebuffer est nécessairement aux dimensions
 * du viewport, c'est-à-dire plein écran.
 */
class Framebuffer {
public:
	Framebuffer(int _w, int _h);
	~Framebuffer();
	
	/**
	 * \brief Alloue la mémoire pour le framebuffer et le rectangle d'affichage
	 */
	void setup();
	
	/**
	 * \brief Tout appel de dessin OpenGL suivant cet appel est effectué dans le framebuffer.
	 */
	void begin();
	
	/**
	 * \brief Arrêter de rediriger les appels dans le framebuffer.
	 */
	void end();
	
	/**
	 * \brief Dessiner le contenu du framebuffer à l'écran, traité par le shader.
	 */
	void draw(ofShader & shader, GLuint second_texture = 0);

	/**
	 * \brief Lire des pixels dans le framebuffer
	 */
	void readPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid * data);
	
	GLuint getTexture() { return texture; };
private:
	int w, h;

	Framebuffer & operator=(const Framebuffer&); ///< Empêcher l'assignation
	Framebuffer(const Framebuffer&); ///< Empêcher la copie

	GLuint fbo, texture, quadVAO; ///< Identifiants d'objets OpenGL
};
