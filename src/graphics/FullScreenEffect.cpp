/**
* \file FullScreenEffect.cpp
* \brief Permets de faire des effets plein �cran
* \author Equipe 5
* \version 1.0
*
*/

#include "FullScreenEffect.h"



FullScreenEffect::FullScreenEffect() :
framebuffer(0)
{ }



FullScreenEffect::~FullScreenEffect() {
	if (0 != framebuffer) {
		delete framebuffer;
	}
}



void FullScreenEffect::setup(ofShader _shader, GLfloat secs_duration) {
	if (0 == framebuffer) {
		framebuffer = new Framebuffer(ofGetViewportWidth(), ofGetViewportHeight());
		framebuffer->setup();
	}
	
	shader = _shader;
	secsDuration = secs_duration;
}



void FullScreenEffect::setup(ofShader _shader) {
	setup(_shader, -1.0f);
}



void FullScreenEffect::rebuildFramebuffer(int w, int h) {
	if (0 != framebuffer) {
		delete framebuffer;
	}
	
	framebuffer = new Framebuffer(w, h);
	framebuffer->setup();
}



void FullScreenEffect::update(GLfloat secs_ellapsed) {
	secsSpent += secs_ellapsed;
}



void FullScreenEffect::begin() {
	framebuffer->begin();
}



void FullScreenEffect::end() {
	framebuffer->end();
}



bool FullScreenEffect::done() {
	return secsSpent > secsDuration && secsDuration > 0.0f;
}



void FullScreenEffect::draw() {
	shader.begin();

	GLfloat interpolation = secsSpent <= secsDuration ?
		secsSpent / secsDuration :
		1.0f;

	//If no duration, effect always full
	if (secsDuration < 0.0f) {
		interpolation = 0.0f;
	//If done, clamp at 1
	} else if (secsSpent > secsDuration) {
		interpolation = 1.0f;
	//Normalize amount of effect between 0 and 1
	} else {
		interpolation = secsSpent / secsDuration;
	}

	shader.setUniform1f("interpolation", interpolation);

	framebuffer->draw(shader);
	shader.end();
}