/**
* \file GraphicSelector.h
* \brief Permets d'implementer la selection graphique a partir d'un FBO
* \author Equipe 5
* \version 1.0
*
*/

#pragma once
#include "ofMain.h"
#include "Framebuffer.h"
#include "../ColorIDManager.h"
#include "../scenegraph/abstract.h"

/**
 * \brief Détecteur de sélection par pointage.
 *
 * Rend une scène dans un framebuffer, échantillonne 
 * la couleur à une position précise et déduit 
 * le secteur à cette position selon à partir
 * de ColorIDManager.
 */
class GraphicSelector
{
public:
	GraphicSelector();
	~GraphicSelector();

	/**
	 * \brief Prépare le Framebuffer et le shader interne pour utilisation.
	 */
	void setup();

	void rebuildFramebuffer(int w, int h);

	/**
	 * \brief Déduit le secteur cliqué
	 *
	 * \param x Coordonnée X du clic
	 * \param y Coordonnée Y du clic
	 * \param scene Scène de rendu d'où déduire le clic. Doit être préalablement
	 *    configuré pour un rendu en mode Sélection (couleurs unies).
	 * \param maze Labyrinthe où prendre le secteur.
	 *
	 * \return Le secteur cliqué ou 0, si le clic n'était pas sur un secteur
	 */
	Sector * findSector(int x, int y, scenegraph::NodePtr scene, Maze * maze);
	
	std::vector <Sector *> findSectors(ofRectangle rect, scenegraph::NodePtr scene, Maze * maze);
private:
	GraphicSelector & operator=(const GraphicSelector&); ///< Empêcher l'assignation
	GraphicSelector(const GraphicSelector&); ///< Empêcher la copie


	Framebuffer * selectFramebuffer; ///< Framebuffer où faire le rendu et échantillonner par la suite
	ColorIDManager colorIDManager; ///< Gère la conversion secteur/couleur 
};

