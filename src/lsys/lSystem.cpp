/**
* \file lSystem.cpp
* \brief Permet de generer un code de type turtle a partir d'une regle de lsysteme
* \author Pascal Renauld
* \version 1.0
*
*/
#include "lSystem.h"
#include "lSysRule.h"


#include <iostream>

using namespace std;

/**
* \namespace primitives
* \brief Espace de nom des classes lsystemes.
*/
namespace lSys{
	lSystem::lSystem(){
		start = "";
		angle=0;
	}

	lSystem::~lSystem(){
		lSysRule* lastRule;
		while (!rules.empty()){
			lastRule = rules.back();
			delete lastRule;
			rules.pop_back();
		}
	}

	vector<string> lSystem::getVariables() const{ return variables; }
	vector<string> lSystem::getConstants() const{ return constants; }
	vector<lSysRule*> lSystem::getRules() const{ return rules; }

	void lSystem::addVariable(string var){
		variables.push_back(var);
	}

	void lSystem::addConstant(string cte){
		constants.push_back(cte);
	}

	void lSystem::addRule(string startSymbol, string successor){

		if (!ruleExist(startSymbol, successor)){
			rules.push_back(new lSysRule(startSymbol,successor));
		}
	}

	bool lSystem::ruleExist(string startSymbol, string successor)
	{
		bool rulePresent = false;
		for (int ruleNum = 0; ruleNum < rules.size(); ruleNum++){
			if (rules[ruleNum]->getStartSymbol() == startSymbol && rules[ruleNum]->getSuccessor() == successor){
				rulePresent = true;
			}
		}
		return rulePresent;
	}

	void lSystem::setStart(string startPoint){
		start = startPoint;
	}
	void lSystem::setAngle(float theta){
		angle = theta;
	}


	//Utils
	string lSystem::generateNthLevel(int n) const{
		string nthLevel = start;
	
		bool ruleApplied = false;

		for (int j = 0; j < n; j++){
			string newS="";
			for (int i = 0; i < nthLevel.length(); i++){
				for (int ruleNum = 0; ruleNum < rules.size(); ruleNum++){
					if (nthLevel.substr(i, 1) == rules[ruleNum]->getStartSymbol()){
						newS += rules[ruleNum]->getSuccessor();
						ruleApplied = true;
					}
				}
				if(!ruleApplied)
					newS += nthLevel.substr(i, 1);
				ruleApplied = false;				
			}		
			nthLevel = newS;
		}
		return nthLevel;
	}

} // End of lSystem namespace