/**
* \file lSystem.h
* \brief Permet de generer un code de type turtle a partir d'une regle de lsysteme
* \author Pascal Renauld
* \version 1.0
*
*/
#pragma once
#include <sstream>
#include <string>
#include <vector>
#include "lSysRule.h"


using namespace std;

/**
* \namespace primitives
* \brief Espace de nom les classes lsystemes.
*/
namespace lSys
{

	/**
	* \class lsys::lSystem
	* \brief 
	*/
	class lSystem
	{
	public:
		/**
		* \brief Constructeur de la classe l-system.
		*/
		lSystem();

		/**
		* \brief Destructeur de la classe l-system.
		*/
		~lSystem();
		
		//Accessors
		/**
		* \brief Retourne la liste des variable du l-systeme.
		* \return Un vecteur de string contenant les variables.
		*/
		vector<string> getVariables() const;

		/**
		* \brief Retourne la liste des constantes du l-systeme.
		* \return Un vecteur de string contenant les constantes.
		*/
		vector<string> getConstants() const;

		/**
		* \brief Retourne la liste des r�gles du l-systeme.
		* \return Un vecteur de string contenant des pointeurs vers les r�gles.
		*/
		vector<lSysRule*> getRules() const;

		/**
		* \brief Retourne le symbole de d�part du l-syst�me.
		* \return Un string contenant le symbol.
		*/
		inline string getStart() const{ return start; }

		/**
		* \brief Retourne l'angle � utiliser pour les rotations.
		* \return Un float contenant l'angle en degr�s.
		*/
		inline float getAngle() const{ return angle; }
		
		/**
		* \brief Ajoute une variable au l-syst�me.
		* \param var Variable � ajouter.
		*/
		void addVariable(string var);

		/**
		* \brief Ajoute une constante au l-syst�me.
		* \param cte Constante � ajouter.
		*/
		void addConstant(string cte);

		/**
		* \brief Ajoute une r�gle au l-syst�me.
		* \param startSymbol Symbole de d�part de la r�gle.
		* \param successor Symbole de remplacement.
		*/
		void addRule(string startSymbol, string successor);

		/**
		* \brief Permets de r�gler la s�quence de d�part.
		* \param startPoint S�quence de d�part.
		*/
		void setStart(string startPoint);

		/**
		* \brief Permets de sp�cifier l'angle � utiliser pour les rotations.
		* \param theta Angle � utiliser.
		*/
		void setAngle(float theta);

		/**
		* \brief D�termine si une r�gle existe dans le l-syst�me.
		* \param startSymbol Symbole de d�part de la r�gle � tester.
		* \param successor Symboles de remplacement � tester.
		* \return Un vrai si la r�gle existe, faux sinon.
		*/
		bool ruleExist(string startSymbol, string successor);

	
		//Utils
		/**
		* \brief G�n�re la s�quence de niveau n du l-syst�me.
		* \param n Niveau de profondeur.
		* \return La s�quence de niveau n du l-syst�me.
		*/
		string generateNthLevel(int n) const;


		//Everybody needs a friend
		//friend bool operator==(lSystem& r1, lSystem& r2);

	private:
		lSystem(const lSystem&);  // Disallow copy
		lSystem& operator=(const lSystem&); // Prevent assignation

		vector<string> variables;
		vector<string> constants;
		string start;
		vector<lSysRule*> rules;
		float angle;
	};

} // End of lSystem namespace

