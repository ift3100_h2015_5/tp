/**
* \file lSysRule.cpp
* \brief Permet d'implanter une regle pour lsysteme
* \author Pascal Renauld
* \version 1.0
*
*/
#include "lSysRule.h"

using namespace std;

/**
* \namespace primitives
* \brief Espace de nom les classes lsystemes.
*/
namespace lSys{
lSysRule::lSysRule(string startSymb, string success){
	startSymbol = startSymb;
	successor = success;
}

lSysRule::~lSysRule(){}


bool operator==(lSysRule& r1, lSysRule& r2){
	return ((r1.startSymbol == r2.startSymbol)&&(r1.successor == r2.successor));
}

string lSysRule::printRule() const{
	std::ostringstream os;
	os <<  startSymbol << "->" << successor << endl;
	return os.str();
}



} // End of lSystem namespace