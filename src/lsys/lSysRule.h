/**
* \file lSysRule.h
* \brief Permet d'implanter une regle pour lsysteme
* \author Pascal Renauld
* \version 1.0
*
*/

#pragma once
#include <string>
#include <sstream>

/**
* \namespace primitives
* \brief Espace de nom les classes lsystemes.
*/
using namespace std;

namespace lSys
{
	class lSysRule
	{
	public:
		/**
		* \brief Constructeur pour une r�gle de l-syst�me.
		* \param startSym Symbole de d�part.
		* \param succes S�rie de symboles qui utili�s pour le remplacement r�cursif.
		*/
		lSysRule(string startSym, string succes);

		/**
		* \brief Destructeur de la class l-syst�me.
		*/
		~lSysRule();


		/**
		* \brief Accesseur
		* \return Le symbol de d�part.
		*/
		inline string getStartSymbol() const { return startSymbol; }

		/**
		* \brief Accesseur
		* \return Le symbol de remplacement.
		*/
		inline string getSuccessor() const { return successor; }
	
		/**
		* \brief Op�rateur ==
		* \param r1 R�gle 1 � comparer.
		* \param r2 R�gle 2 � comparer.
		* \return Vrai si les deux r�gles sont identiques, faux sinon.
		*/
		friend bool operator==(lSysRule& r1, lSysRule& r2);

	
		/**
		* \brief Retourne la r�gle du l-syst�me
		* \return Une chaine de caract�res contenant la r�gle
		*/
		string printRule() const;

	private:
		lSysRule(const lSysRule&);  // Disallow copy
		lSysRule& operator=(const lSysRule&); // Prevent assignation

		string startSymbol;
		string successor;

	};

} // End of lSystem namespace

