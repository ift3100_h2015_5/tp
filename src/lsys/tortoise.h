/**
* \file tortoise.h
* \brief Permet de dessiner a partir d'instructions de genre turtle
* \author Pascal Renauld
* \version 1.0
*
*/
#pragma once

#include "ofMain.h"
#include "../ofxPrimitives/ofxPrimitives.h"
#include <vector>


/**
* \namespace primitives
* \brief Espace de nom les classes lsystemes.
*/
namespace lSys
{

const ofVec3f Znormal(0, 0, 1);
const ofVec3f Xnormal(1, 0, 0);
const ofVec3f Ynormal(1, 0, 1);

/**
* \class tortoise
* \brief Classe pour faire des dessins de type turtle.
*
*/
class tortoise
{
public:
	//Constructor and destructor

	/**
	* \brief Constructeur sans param�tres
	*/
	tortoise();

	/**
	* \brief Constructeur
	* \param startX Position de d�part en X
	* \param startY Position de d�part en Y
	* \param startZ Position de d�part en Z
	* \param theta Angle pour les rotations
	* \param l Longueur d'un segment
	*/
	tortoise(float startX, float startY, float startZ, float theta, float l);

	/**
	* \brief Destructeur
	*/
	~tortoise(){};

	//Accessors
	inline float getAngle() const { return angle; }
	inline float getLength() const { return length; }
	inline float getX() const { return x; }
	inline float getY() const { return y; }
	inline float getZ() const { return z; }

	/**
	* \brief Permets de r�gler l'angle de rotation
	* \param theta Angle de rotation
	*/
	 void setAngle(float theta);

	 /**
	 * \brief Permets de r�gler la longueur d'un segment
	 * \param l Longueur d'un segment
	 */
	 void setLength(float l);

	 /**
	 * \brief Permets de r�gler la position de d�part en X
	 * \param startX Position de d�part en X
	 */
	 void setX(float startX);

	 /**
	 * \brief Permets de r�gler la position de d�part en Y
	 * \param startY Position de d�part en Y
	 */
	 void setY(float startY);

	 /**
	 * \brief Permets de r�gler la position de d�part en Z
	 * \param startZ Position de d�part en Z
	 */
	 void setZ(float startZ);

	 /**
	 * \brief Permets de r�gler l'angle de roll
	 * \param angle Angle de rotatior du roll
	 */
	 void setRoll(float angle);

	 /**
	 * \brief Permets de r�gler l'angle de pitch
	 * \param angle Angle de rotation du pitch
	 */
	 void setPitch(float angle);

	 /**
	 * \brief Permets de r�gler l'angle du yaw
	 * \param angle Angle de rotation du yaw
	 */
	 void setYaw(float angle);

	 /**
	 * \brief Permets de r�gler la couleur d'affichage
	 * \param color Couleur d'affichage
	 */
	 void setColor(ofColor color);

	 /**
	 * \brief Permets de r�gler la largeur du trait
	 * \param width Largeur du trait
	 */
	 void setLineWidth(float width);



	//Utils

	 /**
	 * \brief Rotation � gauche
	 */
	void left();

	/**
	* \brief Rotation � droite
	*/
	void right();

	/**
	* \brief Effectue un roll positif en fonction de l'angle actuel
	*/
	void rollPlus();

	/**
	* \brief Effectue un roll n�gatif en fonction de l'angle actuel
	*/
	void rollNeg();

	/**
	* \brief Effectue un pitch positif en fonction de l'angle actuel
	*/
	void pitchPlus();

	/**
	* \brief Effectue un pitch n�gatif en fonction de l'angle actuel
	*/
	void pitchNeg();

	/**
	* \brief Effectue un yaw positif en fonction de l'angle actuel
	*/
	void yawPlus();

	/**
	* \brief Effectue un yaw n�gatif en fonction de l'angle actuel
	*/
	void yawNeg();

	/**
	* \brief D�place la tortue d'une longuen en avant.
	* \param penDown Bool�en qui d�termine si la tortue dessine ou non.
	*/
	void forward(bool penDown = true);

	/**
	* \brief Sauvegarde dans une pile les param�tres de la tortue
	*/
	void push();

	/**
	* \brief R�cup�re la derni�re sauvegarde des param�tres
	*/
	void pop();

	/**
	* \brief Dessine une s�quence dans le format lsyst�me
	* \param s String contenant la s�quence d'instructions � dessiner
	* Alphabet � utiliser pour le string de la s�quence:
	* 'F': forward();
	* 'G': forward();
	* 'X': forward(false);
	* 'S': length=length/2;
	* '6': forward();
	* '7': forward();
	* '8': forward();
	* '9': forward();
	* '+': right();
	* '-': left();
	* '&': pitchPlus();
	* '^': pitchNeg();
	* '\\': yawPlus();
	* '/': yawNeg();
	* '|': rollPlus(); rollPlus();
	* '[': push();
	* ']': pop();
	*/
	void draw(string& s);




private:
	tortoise(const tortoise&);  // Disallow copy
	tortoise& operator=(const tortoise&); // Prevent assignation

	float roll;  // rotation around Z-axis
	float pitch;  // rotation around Y-axis
	float yaw;   // rotation around X-axis
	float rollIni;
	float pitchIni;
	float yawIni;
	float angle;
	ofQuaternion totalRot;
	ofQuaternion rollRot;
	ofQuaternion pitchRot;
	ofQuaternion yawRot;
	float length;
	float x;
	float y;
	float z;
	float xIni;
	float yIni;
	float zIni;
	float lengthIni;
	vector<float> stack;
	ofColor lineColor;
	float lineWidth;



};

} // End of lSystem namespace

