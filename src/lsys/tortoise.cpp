/**
* \file tortoise.cpp
* \brief Permet de dessiner a partir d'instructions de genre turtle
* \author Pascal Renauld
* \version 1.0
*
*/
#include "tortoise.h"


/**
* \namespace primitives
* \brief Espace de nom les classes lsystemes.
*/
namespace lSys{
tortoise::tortoise(){
		x = xIni = 0;
		y = yIni = 0;
		z = zIni = 0;
		roll = rollIni = 180;
		pitch = pitchIni = 0;
		yaw = yawIni = 0;
		angle = 90;
		length = lengthIni = 10;
		lineColor = ofColor(255);
		lineWidth = 1.0;
	}


tortoise::tortoise(float startX, float startY, float startZ, float theta, float l){
	x = xIni = startX;
	y = yIni = startY;
	z = zIni = startZ;
	roll = rollIni = 180;
	pitch = pitchIni = 0;
	yaw = yawIni = 0;
	angle = theta;
	length = lengthIni = l;
	lineColor = ofColor(255);
	lineWidth = 1.0;
}


void tortoise::setAngle(float theta){
	angle=theta;
}

void tortoise::setLength(float l){
	length = lengthIni = l;

}
void tortoise::setX(float startX){
	x = xIni = startX;	
}

void tortoise::setY(float startY){
	y = yIni = startY;
}

void tortoise::setZ(float startZ){
	z = zIni = startZ;
}

void tortoise::setRoll(float angle){
	roll = rollIni = angle;
}


void tortoise::setPitch(float angle){
	pitch = pitchIni = angle;
}


void tortoise::setYaw(float angle){
	yaw = yawIni = angle;
}


void tortoise::right(){
	roll += angle;	
}
void tortoise::left(){
	roll -=angle;
}


void tortoise::rollPlus(){
	roll += angle;
}
void tortoise::rollNeg(){
	roll -= angle;
}

void tortoise::pitchPlus(){
	pitch += angle;
}
void tortoise::pitchNeg(){
	pitch -= angle;
}

void tortoise::yawPlus(){
	yaw += angle;
}
void tortoise::yawNeg(){
	yaw -= angle;
}

void tortoise::setColor(ofColor color){
	lineColor = color;
}
void tortoise::setLineWidth(float width){
	lineWidth = width;
}


void tortoise::push(){
	stack.push_back(length);
	stack.push_back(roll);
	stack.push_back(pitch);
	stack.push_back(yaw);
	stack.push_back(x);
	stack.push_back(y);
	stack.push_back(z);
}

void tortoise::pop(){
	z = stack.back();
	stack.pop_back();
	y = stack.back();
	stack.pop_back();
	x = stack.back();
	stack.pop_back();
	yaw = stack.back();
	stack.pop_back();
	pitch = stack.back();
	stack.pop_back();
	roll = stack.back();
	stack.pop_back();
	length = stack.back();
	stack.pop_back();
}

void tortoise::forward(bool penDown){
	//ofEnableAlphaBlending();
	ofSetColor(lineColor, 255);
	ofSetLineWidth(lineWidth);

	rollRot = ofQuaternion(roll, Znormal);
	pitchRot = ofQuaternion(pitch, Xnormal);
	yawRot = ofQuaternion(yaw, Ynormal);
	totalRot = rollRot*pitchRot*yawRot;

	ofVec3f pos(0, 1, 0);
	ofVec3f newV = totalRot*pos;
		
	if (penDown){
		primitives::p_line(x, y, z, x + newV.x*length, y + newV.y*length, z + newV.z*length);
	}
	x = x+newV.x*length;
	y = y+newV.y*length;
	z = z+newV.z*length;
}

void tortoise::draw(string& s){
	roll = rollIni;
	pitch = pitchIni;
	yaw = yawIni;
	x = xIni;
	y = yIni;
	z = zIni;
	
	length = lengthIni;
	for (int i = 0; i < s.length(); i++){
		if (s[i] == 'F')
			forward();
		else if (s[i] == 'G')
			forward();
		else if (s[i] == 'X')
			forward(false);
		else if (s[i] == 'S')
			length=length/2;
		else if (s[i] == '6')
			forward();
		else if (s[i] == '7')
			forward();
		else if (s[i] == '8')
			forward();
		else if (s[i] == '9')
			forward();
		else if (s[i] == '+')
			right();
		else if (s[i] == '-')
			left();
		else if (s[i] == '&')
			pitchPlus();
		else if (s[i] == '^')
			pitchNeg();
		else if (s[i] == '\\')
			yawPlus();
		else if (s[i] == '/')
			yawNeg();
		else if (s[i] == '|'){
			rollPlus(); rollPlus(); 
		}
		else if (s[i] == '[')
			push();
		else if (s[i] == ']')
			pop();
	}


}


} // End of lSystem namespace