/**
* \file HermiteCurve.h
* \brief Permets la creation de courbes d'hermite.
* \author Equipe 5
* \version 1.0
*
*/

#pragma once

#include "ofMain.h"

namespace nosmaths {

	/**
	 * \brief Une courbe de Hermite d�finie par deux points et des vecteurs de vitesse.
	 */
	class HermiteCurve {
	public:
		HermiteCurve() { };
		HermiteCurve(ofPoint _p1, ofPoint _p2, ofVec3f _v1, ofVec3f _v2);
		
		ofPoint getP1() const
		{ return p1; }
		
		ofPoint getP2() const
		{ return p2; }
		
		ofVec3f getV1() const
		{ return v1; }
		
		ofVec3f getV2() const
		{ return v2; }
		
		void setP1(ofPoint);
		void setP2(ofPoint);
		void setV1(ofVec3f);
		void setV2(ofVec3f);
		
		/**
		 * \brief Obtenir un point sur la courbe.
		 *
		 * \param t Endroit sur la courbe o� �chantillonner le point. Doit �tre entre 0 et 1.
		 */
		ofPoint getPoint(float t) const;
		
	private:
		static ofMatrix4x4 matx; ///< Matrice de base d'une courbe de Hermite
		ofPoint p1;
		ofPoint p2;
		
		ofVec3f v1;
		ofVec3f v2;
		
		ofMatrix4x4 GM; ///< Matrice de base multipli�e � la matrice des contraintes g�om�triques (les points et les vecteurs)
		
		void updateGM();
	};
}