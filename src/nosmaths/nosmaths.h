/**
* \file nosmaths.h
* \brief Diverses fonctions mathematiques personnalisees
* \author Equipe 5
* \version 1.0
*
*/

#pragma once

/**
 * \namespace nosmaths
 * 
 * \brief Regroupe les composantes mathématiques que nous avons programmées.
 */

#include "quaternion.h"
#include "CoonsHermiteSurface.h"

namespace nosmaths {
	bool closeEnough(float a, float b, float precision);
	ofMatrix4x4 matr4x4multiplication(ofMatrix4x4 pMat1, ofMatrix4x4 pMat2);
	ofMatrix4x4 matr4x4rotationX(ofMatrix4x4 pMatr, float pAngle);
	ofMatrix4x4 matr4x4rotationY(ofMatrix4x4 pMatr, float pAngle);
	ofMatrix4x4 matr4x4scale(ofMatrix4x4 pMatr, float pScaleX, float pScaleY, float pScaleZ);
	ofMatrix4x4 matr4x4translation(ofMatrix4x4 pMatr, float pTransX, float pTransY, float pTransZ);
}