/**
* \file nosmaths.cpp
* \brief Diverses fonctions mathematiques personnalisees
* \author Equipe 5
* \version 1.0
*
*/
#include "nosmaths.h"

namespace nosmaths {
	bool closeEnough(float a, float b, float precision) {
		return fabs(a - b) < precision;
	}

	inline float degreeToRadian(float pDegree)
	{
		return pDegree / 180 * PI;
	}

	ofMatrix4x4 matr4x4scale(ofMatrix4x4 pMatr, float pScaleX, float pScaleY, float pScaleZ)
	{
		pMatr(0, 0) *= pScaleX;
		pMatr(1, 0) *= pScaleX;
		pMatr(2, 0) *= pScaleX;
		pMatr(3, 0) *= pScaleX;

		pMatr(0, 1) *= pScaleY;
		pMatr(1, 1) *= pScaleY;
		pMatr(2, 1) *= pScaleY;
		pMatr(3, 1) *= pScaleY;

		pMatr(0, 2) *= pScaleZ;
		pMatr(1, 2) *= pScaleZ;
		pMatr(2, 2) *= pScaleZ;
		pMatr(3, 2) *= pScaleZ;

		return pMatr;
	}

	ofMatrix4x4 matr4x4rotationY(ofMatrix4x4 pMatr, float pAngle)
	{
		pAngle = degreeToRadian(pAngle);

		ofMatrix4x4 rotMatrix;

		rotMatrix(0, 0) = cos(pAngle);
		rotMatrix(2, 0) = sin(pAngle);
		rotMatrix(0, 2) = (sin(pAngle) * -1);
		rotMatrix(2, 2) = cos(pAngle);

		return matr4x4multiplication(pMatr, rotMatrix);
	}

	ofMatrix4x4 matr4x4rotationX(ofMatrix4x4 pMatr, float pAngle)
	{
		pAngle = degreeToRadian(pAngle);

		ofMatrix4x4 rotMatrix;

		rotMatrix(1, 1) = cos(pAngle);
		rotMatrix(2, 1) = (sin(pAngle) * -1);
		rotMatrix(1, 2) = sin(pAngle);
		rotMatrix(2, 2) = cos(pAngle);

		return matr4x4multiplication(pMatr, rotMatrix);
	}

	ofMatrix4x4 matr4x4multiplication(ofMatrix4x4 pMat1, ofMatrix4x4 pMat2)
	{
		ofMatrix4x4 answer;

		answer(0, 0) = (pMat1(0, 0) * pMat2(0, 0)) + (pMat1(0, 1) * pMat2(1, 0)) + (pMat1(0, 2) * pMat2(2, 0)) + (pMat1(0, 3) * pMat2(3, 0));
		answer(0, 1) = (pMat1(0, 0) * pMat2(0, 1)) + (pMat1(0, 1) * pMat2(1, 1)) + (pMat1(0, 2) * pMat2(2, 1)) + (pMat1(0, 3) * pMat2(3, 1));
		answer(0, 2) = (pMat1(0, 0) * pMat2(0, 2)) + (pMat1(0, 1) * pMat2(1, 2)) + (pMat1(0, 2) * pMat2(2, 2)) + (pMat1(0, 3) * pMat2(3, 2));
		answer(0, 3) = (pMat1(0, 0) * pMat2(0, 3)) + (pMat1(0, 1) * pMat2(1, 3)) + (pMat1(0, 2) * pMat2(2, 3)) + (pMat1(0, 3) * pMat2(3, 3));

		answer(1, 0) = (pMat1(1, 0) * pMat2(0, 0)) + (pMat1(1, 1) * pMat2(1, 0)) + (pMat1(1, 2) * pMat2(2, 0)) + (pMat1(1, 3) * pMat2(3, 0));
		answer(1, 1) = (pMat1(1, 0) * pMat2(0, 1)) + (pMat1(1, 1) * pMat2(1, 1)) + (pMat1(1, 2) * pMat2(2, 1)) + (pMat1(1, 3) * pMat2(3, 1));
		answer(1, 2) = (pMat1(1, 0) * pMat2(0, 2)) + (pMat1(1, 1) * pMat2(1, 2)) + (pMat1(1, 2) * pMat2(2, 2)) + (pMat1(1, 3) * pMat2(3, 2));
		answer(1, 3) = (pMat1(1, 0) * pMat2(0, 3)) + (pMat1(1, 1) * pMat2(1, 3)) + (pMat1(1, 2) * pMat2(2, 3)) + (pMat1(1, 3) * pMat2(3, 3));

		answer(2, 0) = (pMat1(2, 0) * pMat2(0, 0)) + (pMat1(2, 1) * pMat2(1, 0)) + (pMat1(2, 2) * pMat2(2, 0)) + (pMat1(2, 3) * pMat2(3, 0));
		answer(2, 1) = (pMat1(2, 0) * pMat2(0, 1)) + (pMat1(2, 1) * pMat2(1, 1)) + (pMat1(2, 2) * pMat2(2, 1)) + (pMat1(2, 3) * pMat2(3, 1));
		answer(2, 2) = (pMat1(2, 0) * pMat2(0, 2)) + (pMat1(2, 1) * pMat2(1, 2)) + (pMat1(2, 2) * pMat2(2, 2)) + (pMat1(2, 3) * pMat2(3, 2));
		answer(2, 3) = (pMat1(2, 0) * pMat2(0, 3)) + (pMat1(2, 1) * pMat2(1, 3)) + (pMat1(2, 2) * pMat2(2, 3)) + (pMat1(2, 3) * pMat2(3, 3));

		answer(3, 0) = (pMat1(3, 0) * pMat2(0, 0)) + (pMat1(3, 1) * pMat2(1, 0)) + (pMat1(3, 2) * pMat2(2, 0)) + (pMat1(3, 3) * pMat2(3, 0));
		answer(3, 1) = (pMat1(3, 0) * pMat2(0, 1)) + (pMat1(3, 1) * pMat2(1, 1)) + (pMat1(3, 2) * pMat2(2, 1)) + (pMat1(3, 3) * pMat2(3, 1));
		answer(3, 2) = (pMat1(3, 0) * pMat2(0, 2)) + (pMat1(3, 1) * pMat2(1, 2)) + (pMat1(3, 2) * pMat2(2, 2)) + (pMat1(3, 3) * pMat2(3, 2));
		answer(3, 3) = (pMat1(3, 0) * pMat2(0, 3)) + (pMat1(3, 1) * pMat2(1, 3)) + (pMat1(3, 2) * pMat2(2, 3)) + (pMat1(3, 3) * pMat2(3, 3));

		return answer;
	}

	ofMatrix4x4 matr4x4translation(ofMatrix4x4 pMatr, float pTransX, float pTransY, float pTransZ)
	{
		pMatr(0, 0) += pTransX * pMatr(0, 3);
		pMatr(1, 0) += pTransX * pMatr(1, 3);
		pMatr(2, 0) += pTransX * pMatr(2, 3);
		pMatr(3, 0) += pTransX * pMatr(3, 3);

		pMatr(0, 1) += pTransY * pMatr(0, 3);
		pMatr(1, 1) += pTransY * pMatr(1, 3);
		pMatr(2, 1) += pTransY * pMatr(2, 3);
		pMatr(3, 1) += pTransY * pMatr(3, 3);

		pMatr(0, 2) += pTransZ * pMatr(0, 3);
		pMatr(1, 2) += pTransZ * pMatr(1, 3);
		pMatr(2, 2) += pTransZ * pMatr(2, 3);
		pMatr(3, 2) += pTransZ * pMatr(3, 3);

		return pMatr;
	}
}