/**
* \file quaternion.cpp
* \brief Implémentation de notre classe personnalisee de quaternions
* \author Equipe 5
* \version 1.0
*
*/


#include "quaternion.h"

namespace nosmaths {

	Quaternion::Quaternion(double angle, ofVec3f axis) {
		w = cos(angle / 2.0f);
		x = axis.x * sin(angle / 2.0f);
		y = axis.y * sin(angle / 2.0f);
		z = axis.z * sin(angle / 2.0f);
	};

	
	
	Quaternion & Quaternion::operator = (const Quaternion & other) {
		if (this != &other) {
			w = other.w;
			x = other.x;
			y = other.y;
			z = other.z;
		}
		
		return *this;
	}


	
	ofMatrix4x4 Quaternion::getMatx() const {
		ofMatrix4x4 matx;
		matx.makeIdentityMatrix();

		matx(0, 0) = 1.0f - 2.0f*pow(y, 2.0) - 2.0f*pow(z, 2.0);
		matx(1, 0) = 2.0f*x*y - 2.0f*w*z;
		matx(2, 0) = 2.0f*x*z + 2.0f*w*y;
		matx(3, 0) = 0;

		matx(0, 1) = 2.0f*x*y + 2.0f*w*z;
		matx(1, 1) = 1 - 2.0f*pow(x, 2.0) - 2.0f*pow(z, 2.0);
		matx(2, 1) = 2.0f*y*z - 2.0f*w*x;
		matx(3, 1) = 0;

		matx(0, 2) = 2.0f*x*z - 2.0f*w*y;
		matx(1, 2) = 2.0f*y*z + 2.0f*w*x;
		matx(2, 2) = 1 - 2.0f*pow(x, 2.0) - 2.0f*pow(y, 2.0);
		matx(3, 2) = 0;

		matx(0, 3) = 0;
		matx(1, 3) = 0;
		matx(2, 3) = 0;
		matx(3, 3) = 1;

		return matx;
	}




	Quaternion Quaternion::operator*(const Quaternion & other) const {
		double  qw = w * other.w - getV().dot(other.getV());
		ofVec3f qv1 = getV().cross(other.getV());
		ofVec3f qv2 = other.w * getV();
		ofVec3f qv3 = w * other.getV();
		ofVec3f qv = qv1 + qv2 + qv3;

		Quaternion result(qw, qv.x, qv.y, qv.z);
		result.makeUnit(); //En théorie, ce ne serait pas nécessaire. En pratique, les floats/doubles ne sont pas 100% précis.

		return result;
	};

}