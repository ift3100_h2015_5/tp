/**
* \file quaternion.h
* \brief Implémentation de notre classe personnalisee de quaternions
* \author Equipe 5
* \version 1.0
*
*/
#pragma once

#include "ofMain.h"

namespace nosmaths {
	/**
 	 * Permet de stocker une rotation.
	 *
	 * L'interpolation entre rotations est plus sûre avec les quaternions qu'avec les matrices.
	 *
	 * Est maintenu normalisé automatiquement.
	 */
	class Quaternion {
	public:
		double w;
		double x;
		double y;
		double z;

		Quaternion(const Quaternion & other) :
			w(other.w), x(other.x), y(other.y), z(other.z)
		{}

		
		/**
		 * Constructeur à partir des valeurs reelles du vecteur 4d
		 * du Quaternion.
		 */
		Quaternion(double _w, double _x, double _y, double _z) :
			w(_w), x(_x), y(_y), z(_z)
		{}
		

		
		/**
		 * Constructeur à partir d'un angle d'Euler.
		 *
		 * \param angle Angle de rotation en radians.
		 *
		 * \param axis Axe normalisé de rotation.
		 */
		Quaternion(double angle, ofVec3f axis);

		
		
		Quaternion & operator=(const Quaternion & other);
		
		

		/**
		 * Génère la matrice de rotation équivalente au Quaternion.
		 */
		ofMatrix4x4 getMatx() const;


		
		/**
		 * Obtient le vecteur 3d du Quaternion.
		 */
		ofVec3f getV() const { return ofVec3f(x, y, z); }


		
		/**
		 * Multiplication entre deux Quaternions.
		 */
		Quaternion operator*(const Quaternion & other) const;

		
		
		/**
		 * Conjugaison du quaternion.
		 */
		Quaternion conj() const {
			return Quaternion(w, -x, -y, -z);
		}

		
		
		/**
		 * Inverse du quaternion. Vu qu'on le maintient normalisé,
		 * ça correspond automatiquement à la conjugaison.
		 */
		Quaternion inverse() const
		{ return conj(); }

	private:
		double tolerance = 0.0001; ///< Au-delà de cette limite, on normalise le quaternion à nouveau.

		
		
		/**
		 * Normalise le quaternion, si on le juge nécessaire.
		 */
		void makeUnit() {
			double squared = w*w + x*x + y*y + z*z;
			if (abs(squared - 1.0f) < tolerance) return;

			double magnitude = sqrt(squared);

			w = w / magnitude;
			x = x / magnitude;
			y = y / magnitude;
			z = z / magnitude;
		}
	};
}