/**
* \file CoonsHermiteSurface.cpp
* \brief Permets de creer des surfaces d'hermite
* \author Equipe 5
* \version 1.0
*
*/

#include "CoonsHermiteSurface.h"

namespace nosmaths {
	CoonsHermiteSurface::CoonsHermiteSurface(HermiteCurve _c1, HermiteCurve _c2, HermiteCurve _c3, HermiteCurve _c4) :
		c1(_c1), c2(_c2), c3(_c3), c4(_c4)
	{ }
	
	void CoonsHermiteSurface::setC1(HermiteCurve _c1) {
		c1 = _c1;
	}
	
	void CoonsHermiteSurface::setC2(HermiteCurve _c2) {
		c2 = _c2;
	}
	
	void CoonsHermiteSurface::setC3(HermiteCurve _c3) {
		c3 = _c3;
	}
	
	void CoonsHermiteSurface::setC4(HermiteCurve _c4) {
		c4 = _c4;
	}
	
	ofPoint CoonsHermiteSurface::getPoint(float u, float v) {
		ofPoint blerp =
		(1 - u) * (1 - v) * c1.getPoint(0) +
		u       * (1 - v) * c1.getPoint(1) +
		(1 - u) * v       * c2.getPoint(0) +
		u       * v       * c2.getPoint(1);
		
		ofPoint lerpu = (1 - v) * c1.getPoint(u) + v * c2.getPoint(u);
		
		ofPoint lerpv = (1 - u) * c3.getPoint(v) + u * c4.getPoint(v);
		
		return lerpu + lerpv - blerp;
	}
}