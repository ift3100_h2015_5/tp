/**
* \file CoonsHermiteSurface.h
* \brief Permets de creer des surfaces d'hermite
* \author Equipe 5
* \version 1.0
*
*/

#pragma once

#include "HermiteCurve.h"

namespace nosmaths {

	/**
	* \class CoonsHermiteSurface
	* \brief Permets de g�n�rer une surface de coons � partir de courbes d'Hemite.
	*
	*/
	class CoonsHermiteSurface {
	public:
		/**
		* \brief Constructeur sans param�tres
		*/
		CoonsHermiteSurface() {};
		
		/**
		* \brief Constructeur avec param�tres
		* \param _c1 Courbe d'hermite 1
		* \param _c2 Courbe d'hermite 2
		* \param _c3 Courbe d'hermite 3
		* \param _c4 Courbe d'hermite 4
		*/
		CoonsHermiteSurface(HermiteCurve _c1, HermiteCurve _c2, HermiteCurve _c3, HermiteCurve _c4);
		
		HermiteCurve getC1()
		{ return c1; }

		HermiteCurve getC2()
		{ return c2; }

		HermiteCurve getC3()
		{ return c3; }

		HermiteCurve getC4()
		{ return c4; }
		

		/**
		* \brief Permets d'assigner une courbe comme courbe d'hermite 1
		* \param _c1 Courbe d'hermite 1
		*/
		void setC1(HermiteCurve _c1);

		/**
		* \brief Permets d'assigner une courbe comme courbe d'hermite 2
		* \param _c2 Courbe d'hermite 2
		*/
		void setC2(HermiteCurve _c2);

		/**
		* \brief Permets d'assigner une courbe comme courbe d'hermite 3
		* \param _c3 Courbe d'hermite 3
		*/
		void setC3(HermiteCurve _c3);

		/**
		* \brief Permets d'assigner une courbe comme courbe d'hermite 4
		* \param _c4 Courbe d'hermite 4
		*/
		void setC4(HermiteCurve _c4);
		
		/**
		* \brief Retourne les coordonn�es d'un point en fonction de u et v
		* \param u Param�tre u
		* \param v Param�tre v
		* \return point de la surface
		*/
		ofPoint getPoint(float u, float v);
		
	private:
		HermiteCurve c1;
		HermiteCurve c2;
		HermiteCurve c3;
		HermiteCurve c4;
	};
}