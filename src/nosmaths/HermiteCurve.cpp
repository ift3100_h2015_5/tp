/**
* \file HermiteCurve.cpp
* \brief Permets la creation de courbes d'hermite.
* \author Equipe 5
* \version 1.0
*
*/
#include "HermiteCurve.h"

namespace nosmaths {
	
	ofMatrix4x4 HermiteCurve::matx = ofMatrix4x4(1, 0, -3,  2,
												 0, 0,  3, -2,
												 0, 1, -2,  1,
												 0, 0, -1,  1);
	
	HermiteCurve::HermiteCurve(ofPoint _p1, ofPoint _p2, ofVec3f _v1, ofVec3f _v2) :
		p1(_p1), p2(_p2), v1(_v1), v2(_v2) {
			updateGM();
	}
	
	void HermiteCurve::updateGM() {
		ofMatrix4x4 constraints(p1.x, p2.x, v1.x, v2.x,
								p1.y, p2.y, v1.y, v2.y,
								p1.z, p2.z, v1.z, v2.z,
								0,    0,    0,    0);
		
		GM = constraints * matx;
	}
	
	void HermiteCurve::setP1(ofPoint _p1) {
		p1 = _p1;
		updateGM();
	}
	
	void HermiteCurve::setP2(ofPoint _p2) {
		p2 = _p2;
		updateGM();
	}

	void HermiteCurve::setV1(ofVec3f _v1) {
		v1 = _v1;
		updateGM();
	}

	void HermiteCurve::setV2(ofVec3f _v2) {
		v2 = _v2;
		updateGM();
	}
	
	ofPoint HermiteCurve::getPoint(float t) const {
		return GM * ofVec4f(1, t, t*t, t*t*t);
	}
	
};