/**
* \file primitives3d.cpp
* \brief Permet de produire des primitives 3D (les 5 solides platoniques)
* \author Pascal Renauld
* \version 1.0
*
*/
#include "primitives3d.h"

/**
* \namespace primitives
* \brief Espace de nom les classes des primitives.
*/
namespace primitives{

	std::map< Primitives3dType, GLuint > primitives3d::vertexArrayObjs;

	GLuint primitives3d::getVertexArrayObj(Primitives3dType primitive_type) {
		VAOMap::iterator it = vertexArrayObjs.find(primitive_type);
		if (it != vertexArrayObjs.end()) {
			return (*it).second;
		}

		vector<GLfloat> vp; //Points
		vector<GLfloat> vn; //Normales
		vector<GLfloat> vt;  //Texture
		vector<GLushort> vi; // Indices



		if (primitive_type == P_CUBE){
			GLfloat cubeVerticesValues[] =
			{ 1, 1, 1, -1, 1, 1, -1, -1, 1, 1, -1, 1,   // v0,v1,v2,v3 (front)
			1, 1, 1, 1, -1, 1, 1, -1, -1, 1, 1, -1,   // v0,v3,v4,v5 (right)
			1, 1, 1, 1, 1, -1, -1, 1, -1, -1, 1, 1,   // v0,v5,v6,v1 (top)
			-1, 1, 1, -1, 1, -1, -1, -1, -1, -1, -1, 1,   // v1,v6,v7,v2 (left)
			-1, -1, -1, 1, -1, -1, 1, -1, 1, -1, -1, 1,   // v7,v4,v3,v2 (bottom)
			1, -1, -1, -1, -1, -1, -1, 1, -1, 1, 1, -1 }; // v4,v7,v6,v5 (back)
			vector<GLfloat> cubeVertices(cubeVerticesValues, cubeVerticesValues + 72);
			
			GLfloat cubeNormalsValues[] = {
				0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,   // v0,v1,v2,v3 (North) 
				1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,   // v0,v3,v4,v5 (west)
				0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,   // v0,v5,v6,v1 (east)
				-1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0,   // v1,v6,v7,v2 (bottom??)
				0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0,   // v7,v4,v3,v2 (top??)
				0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1 }; // v4,v7,v6,v5 (south)
			vector<GLfloat> cubeNormals(cubeNormalsValues, cubeNormalsValues + 72);
			
			GLfloat cubeTexValues[] =
			{ 1, 1, 0, 1, 0, 0, 1, 0,// v0,v1,v2,v3 (front)
			1, 1, 0, 1, 0, 0, 1, 0,// v0,v3,v4,v5 (right)
			1, 1, 0, 1, 0, 0, 1, 0,// v0,v5,v6,v1 (top)
			1, 1, 0, 1, 0, 0, 1, 0, // v1,v6,v7,v2 (left)
			1, 1, 0, 1, 0, 0, 1, 0,// v7,v4,v3,v2 (bottom)
			1, 1, 0, 1, 0, 0, 1, 0 }; // v4,v7,v6,v5 (back)
			vector<GLfloat> cubeTex(cubeTexValues, cubeTexValues + 48);
			
			GLushort indices[] = { 0, 1, 2, 2, 3, 0,      // front
				4, 5, 6, 6, 7, 4,      // right
				8, 9, 10, 10, 11, 8,      // top
				12, 13, 14, 14, 15, 12,      // left
				16, 17, 18, 18, 19, 16,      // bottom
				20, 21, 22, 22, 23, 20 };    // back
			vector<GLushort> cubeIndices(indices, indices + 36);

			vp = cubeVertices;
			vn = cubeNormals;
			vt = cubeTex;
			vi = cubeIndices;
		}
		else if (primitive_type == P_TETRA){
			GLfloat tetraVerticesValues[] = {
				0, 0, 0, 1, 1, 0, 1, 0, 1,	 		// Side 1: V0,V1,V2
				0, 0, 0, 0, 1, 1, 1, 1, 0,	 		// Side 2: V2,V3,V1
				1, 1, 0, 0, 1, 1, 1, 0, 1, 	 		// Side 3: V1,V3,V0
				0, 1, 1, 0, 0, 0, 1, 0, 1, 	 		// Side 4: V0,V3,V2				
			};
						
			vector<GLfloat> tetraVertices(tetraVerticesValues, tetraVerticesValues + 36);
			
			GLushort tetraIndicesValues[] = {
				0, 1, 2,  // Side 1				
				6, 7, 8,  // Side 3
				3, 4, 5,  // Side 2
				9, 10, 11   // Side 4
			};
			vector<GLushort> tetraIndices(tetraIndicesValues, tetraIndicesValues + 12);
			
			vector<GLfloat> tetraNormals;
			tetraNormals.resize(36);

			for (int i = 0; i < 12; i += 3){
				int p1 = tetraIndicesValues[i] * 3;   // On prend 3 point d'un cote et on construit 2 vecteur
				int p2 = tetraIndicesValues[i + 1] * 3;
				int p3 = tetraIndicesValues[i + 2] * 3;

				ofVec3f v1(tetraVerticesValues[p1] - tetraVerticesValues[p2], tetraVerticesValues[p1 + 1] - tetraVerticesValues[p2 + 1], tetraVerticesValues[p1 + 2] - tetraVerticesValues[p2 + 2]);
				ofVec3f v2(tetraVerticesValues[p2] - tetraVerticesValues[p3], tetraVerticesValues[p2 + 1] - tetraVerticesValues[p3 + 1], tetraVerticesValues[p2 + 2] - tetraVerticesValues[p3 + 2]);
				v2.cross(v1);// Le resultat du produit vectoriel est la normale
				v2.normalize();
				tetraNormals[p1] = -v2.x;
				tetraNormals[p1 + 1] = -v2.y;
				tetraNormals[p1 + 2] = -v2.z;
				tetraNormals[p2] = -v2.x;
				tetraNormals[p2 + 1] = -v2.y;
				tetraNormals[p2 + 2] = -v2.z;
				tetraNormals[p3] = -v2.x;
				tetraNormals[p3 + 1] = -v2.y;
				tetraNormals[p3 + 2] = -v2.z;
			}

			GLfloat tetraTexValues[] =
			{ 1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0 };
			vector<GLfloat> tetraTex(tetraTexValues, tetraTexValues + 24);

			vp = tetraVertices;
			vn = tetraNormals;
			vt = tetraTex;
			vi = tetraIndices;
		}
		else if (primitive_type == P_OCTA){
			GLfloat octaVerticesValues[] = {
				0, 1, 0, -1, 0, 0, 0, 0, 1,
				0, 1, 0, 0, 0, 1, 1, 0, 0,
				0, 1, 0, 1, 0, 0, 0, 0, -1,
				0, 1, 0, 0, 0, -1, -1, 0, 0,
				0, -1, 0, 0, 0, 1, -1, 0, 0,
				0, -1, 0, 1, 0, 0, 0, 0, 1,
				0, -1, 0, 0, 0, -1, 1, 0, 0,
				0, -1, 0, -1, 0, 0, 0, 0, -1
			};
			vector<GLfloat> octaVertices(octaVerticesValues, octaVerticesValues + 72);

			GLushort octaIndicesValues[] = {
				0,1,2,
				3,4,5,
				6,7,8,
				9,10,11,
				12,13,14,
				15,16,17,
				18,19,20,
				21,22,23

			};
			vector<GLushort> octaIndices(octaIndicesValues, octaIndicesValues + 24);


			vector<GLfloat> octaNormals;
			octaNormals.resize(72);

			for (int i = 0; i < 24; i += 3){
				int p1 = octaIndicesValues[i] * 3;   // On prend 3 point d'un cote et on construit 2 vecteur
				int p2 = octaIndicesValues[i + 1] * 3;
				int p3 = octaIndicesValues[i + 2] * 3;

				ofVec3f v1(octaVerticesValues[p1] - octaVerticesValues[p2], octaVerticesValues[p1 + 1] - octaVerticesValues[p2 + 1], octaVerticesValues[p1 + 2] - octaVerticesValues[p2 + 2]);
				ofVec3f v2(octaVerticesValues[p2] - octaVerticesValues[p3], octaVerticesValues[p2 + 1] - octaVerticesValues[p3 + 1], octaVerticesValues[p2 + 2] - octaVerticesValues[p3 + 2]);
				v2.cross(v1);// Le resultat du produit vectoriel est la normale
				v2.normalize();
				octaNormals[p1] = -v2.x;
				octaNormals[p1 + 1] = -v2.y;
				octaNormals[p1 + 2] = -v2.z;
				octaNormals[p2] = -v2.x;
				octaNormals[p2 + 1] = -v2.y;
				octaNormals[p2 + 2] = -v2.z;
				octaNormals[p3] = -v2.x;
				octaNormals[p3 + 1] = -v2.y;
				octaNormals[p3 + 2] = -v2.z;
			}


			GLfloat octaTexValues[] =
			{ 
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0 };
			vector<GLfloat> octaTex(octaTexValues, octaTexValues + 48);

			vp = octaVertices;
			vn = octaNormals;
			vt = octaTex;
			vi = octaIndices;

		}


		else if (primitive_type == P_DODE){
			const float A = 0.847214f;
			const float B = 0.323607f;
			const float C = 0.523607f;

			GLfloat dodeVerticesValues[] = {
				 0,  A,  B,  0,  A, -B, -C,  C,  C,    //0 1 2
				 0,  A, -B, -C,  C, -C, -C,  C,  C,    //1 3 2
				-C,  C, -C, -A,  B,  0, -C,  C,  C,    //3 4 2
				 0,  A, -B,  0,  A,  B,  C,  C, -C,    //1 0 5
				 0,  A,  B,  C,  C,  C,  C,  C, -C,    //0 6 5
				 C,  C,  C,  A,  B,  0,  C,  C, -C,    //6 7 5
				 0,  A, -B,  C,  C, -C, -C,  C, -C,    //1 5 3
				 C,  C, -C,  B,  0, -A, -C,  C, -C,    //5 8 3
				 B,  0, -A, -B,  0, -A, -C,  C, -C,    //8 9 3
				 0,  A,  B, -C,  C,  C,  C,  C,  C,    //0 2 6
				-C,  C,  C, -B,  0,  A,  C,  C,  C,    //2 10 6
				-B,  0,  A,  B,  0,  A,  C,  C,  C,    //10 11 6
				-C,  C, -C, -B,  0, -A, -A,  B,  0,    //3 9 4
				-B,  0, -A, -C, -C, -C, -A,  B,  0,    //9 12 4
				-C, -C, -C, -A, -B,  0, -A,  B,  0,    //12 13 4
				-C,  C,  C, -A,  B,  0, -B,  0,  A,    //2 4 10
				-A,  B,  0, -A, -B,  0, -B,  0,  A,    //4 13 10
				-A, -B,  0, -C, -C,  C, -B,  0,  A,    //13 14 10
				 C,  C,  C,  B,  0,  A,  A,  B,  0,    //6 11 7
				 B,  0,  A,  C, -C,  C,  A,  B,  0,    //11 15 7
				 C, -C,  C,  A, -B,  0,  A,  B,  0,    //15 16 7
				 C,  C, -C,  A,  B,  0,  B,  0, -A,    //5 7 8
				 A,  B,  0,  A, -B,  0,  B,  0, -A,    //7 16 8
				 A, -B,  0,  C, -C, -C,  B,  0, -A,    //16 17 8
				-B,  0, -A,  B,  0, -A, -C, -C, -C,    //9 8 12
				 B,  0, -A,  C, -C, -C, -C, -C, -C,    //8 17 12
				 C, -C, -C,  0, -A, -B, -C, -C, -C,    //17 18 12
				 B,  0,  A, -B,  0,  A,  C, -C,  C,    //11 10 15
				-B,  0,  A, -C, -C,  C,  C, -C,  C,    //10 14 15
				-C, -C,  C,  0, -A,  B,  C, -C,  C,    //14 19 15
				-A, -B,  0, -C, -C, -C, -C, -C,  C,    //13 12 14
				-C, -C, -C,  0, -A, -B, -C, -C,  C,    //12 18 14
				 0, -A, -B,  0, -A,  B, -C, -C,  C,    //18 19 14
				 A, -B,  0,  C, -C,  C,  C, -C, -C,    //16 15 17
				 C, -C,  C,  0, -A,  B,  C, -C, -C,    //15 19 17
				 0, -A,  B,  0, -A, -B,  C, -C, -C    //19 18 17
			}; 
			vector<GLfloat> dodeVertices(dodeVerticesValues, dodeVerticesValues + 324);

			GLushort dodeIndicesValues[] = {
				0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,
				36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,
				72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107				
				};
			vector<GLushort> dodeIndices(dodeIndicesValues, dodeIndicesValues + 108);

			vector<GLfloat> dodeNormals;
			dodeNormals.resize(324);

			for (int i = 0; i < 108; i += 3){
				int p1 = dodeIndicesValues[i] * 3;   // On prend 3 point d'un cote et on construit 2 vecteur
				int p2 = dodeIndicesValues[i + 1] * 3;
				int p3 = dodeIndicesValues[i + 2] * 3;

				ofVec3f v1(dodeVerticesValues[p1] - dodeVerticesValues[p2], dodeVerticesValues[p1 + 1] - dodeVerticesValues[p2 + 1], dodeVerticesValues[p1 + 2] - dodeVerticesValues[p2 + 2]);
				ofVec3f v2(dodeVerticesValues[p2] - dodeVerticesValues[p3], dodeVerticesValues[p2 + 1] - dodeVerticesValues[p3 + 1], dodeVerticesValues[p2 + 2] - dodeVerticesValues[p3 + 2]);
				v2.cross(v1);// Le resultat du produit vectoriel est la normale
				v2.normalize();
				dodeNormals[p1] = -v2.x;
				dodeNormals[p1 + 1] = -v2.y;
				dodeNormals[p1 + 2] = -v2.z;
				dodeNormals[p2] = -v2.x;
				dodeNormals[p2 + 1] = -v2.y;
				dodeNormals[p2 + 2] = -v2.z;
				dodeNormals[p3] = -v2.x;
				dodeNormals[p3 + 1] = -v2.y;
				dodeNormals[p3 + 2] = -v2.z;
			}



			GLfloat dodeTexValues[] =
			{
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667,
				0.817018839, 0.436338998, 0.695928417, 0.063661002, 0.5, 0.666666667,
				0.695928417, 0.063661002, 0.304071583, 0.063661002, 0.5, 0.666666667,
				0.304071583, 0.063661002, 0.182981161, 0.436338998, 0.5, 0.666666667
			};
			vector<GLfloat> dodeTex(dodeTexValues, dodeTexValues + 216);


			vp = dodeVertices;
			vn = dodeNormals;
			vt = dodeTex;
			vi = dodeIndices;

		}


		else if (primitive_type== P_ISO){
			const float goldenRatio = (1.0 + sqrt(5.0)) / 2.0;
			const float size = .5f;
			const float A = goldenRatio * size;
			const float B = size;
			GLfloat isoVerticesValues[] = {
				-B, A, 0.0f, -A, 0.0f, B, 0.0f, B, A,  // 0 11 5
				-B, A, 0.0f, 0.0f, B, A, B, A, 0.0f,   // 0 5 1
				-B, A, 0.0f, B, A, 0.0f, 0.0f, B, -A,  //0 1 7
				-B, A, 0.0f, 0.0f, B, -A, -A, 0.0f, -B,    //0 7 10
				-B, A, 0.0f, -A, 0.0f, -B, -A, 0.0f, B,   // 0 10 11
				B, A, 0.0f, 0.0f, B, A, A, 0.0f, B,    //1 5 9
				0, B, A, -A, 0.0f, B, 0.0f, -B, A,   // 5 11 4
				-A, 0, B, -A, 0.0f, -B, -B, -A, 0.0f,  // 11 10 2
				-A, 0, -B, 0.0f, B, -A, 0.0f, -B, -A,  // 10 7 6
				0, B, -A, B, A, 0.0f, A, 0.0f, -B,  //7 1 8
				B, -A, 0.0f, A, 0.0f, B, 0.0f, -B, A,   //3 9 4
				B, -A, 0.0f, 0.0f, -B, A, -B, -A, 0.0f,   //3 4 2
				B, -A, 0.0f, -B, -A, 0.0f, 0.0f, -B, -A,   //3 2 6
				B, -A, 0.0f, 0.0f, -B, -A, A, 0.0f, -B,   //3 6 8
				B, -A, 0.0f, A, 0.0f, -B, A, 0.0f, B,   //3 8 9
				0, -B, A, A, 0.0f, B, 0.0f, B, A,   //4 9 5
				-B, -A, 0.0f, 0.0f, -B, A, -A, 0.0f, B,  //2 4 11
				0, -B, -A, -B, -A, 0.0f, -A, 0.0f, -B,  //6 2 10
				A, 0, -B, 0.0f, -B, -A, 0.0f, B, -A,   //8 6 7
				A, 0, B, A, 0.0f, -B, B, A, 0.0f    //9 8 1
			};
			vector<GLfloat> isoVertices(isoVerticesValues, isoVerticesValues + 180);

			GLushort isoIndicesValues[] = {			
				0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
				36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59
			};
			vector<GLushort> isoIndices(isoIndicesValues, isoIndicesValues + 60);

			vector<GLfloat> isoNormals;
			isoNormals.resize(180);

			for (int i = 0; i < 60; i += 3){
				int p1 = isoIndicesValues[i] * 3;   // On prend 3 point d'un cote et on construit 2 vecteur
				int p2 = isoIndicesValues[i + 1] * 3;
				int p3 = isoIndicesValues[i + 2] * 3;

				ofVec3f v1(isoVerticesValues[p1] - isoVerticesValues[p2], isoVerticesValues[p1 + 1] - isoVerticesValues[p2 + 1], isoVerticesValues[p1 + 2] - isoVerticesValues[p2 + 2]);
				ofVec3f v2(isoVerticesValues[p2] - isoVerticesValues[p3], isoVerticesValues[p2 + 1] - isoVerticesValues[p3 + 1], isoVerticesValues[p2 + 2] - isoVerticesValues[p3 + 2]);
				v2.cross(v1);// Le resultat du produit vectoriel est la normale
				v2.normalize();
				isoNormals[p1] = -v2.x;
				isoNormals[p1 + 1] = -v2.y;
				isoNormals[p1 + 2] = -v2.z;
				isoNormals[p2] = -v2.x;
				isoNormals[p2 + 1] = -v2.y;
				isoNormals[p2 + 2] = -v2.z;
				isoNormals[p3] = -v2.x;
				isoNormals[p3 + 1] = -v2.y;
				isoNormals[p3 + 2] = -v2.z;
			}


			GLfloat isoTexValues[] =
			{ 
			1, 1, 0, 1,	0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0,
			1, 1, 0, 1, 0, 0
			};
			vector<GLfloat> isoTex(isoTexValues, isoTexValues + 120);



			vp = isoVertices;
			vn = isoNormals;
			vt = isoTex;
			vi = isoIndices;
		}

		GLuint vao = 0;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		GLuint vertexbuffer;
		glGenBuffers(1, &vertexbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vp.size(), &vp[0], GL_STATIC_DRAW);
		glVertexAttribPointer(ofShader::POSITION_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(ofShader::POSITION_ATTRIBUTE);

		GLuint texbuffer;
		glGenBuffers(1, &texbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, texbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vt.size(), &vt[0], GL_STATIC_DRAW);
		glVertexAttribPointer(ofShader::TEXCOORD_ATTRIBUTE, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(ofShader::TEXCOORD_ATTRIBUTE);


		GLuint normalbuffer;
		glGenBuffers(1, &normalbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vn.size(), &vn[0], GL_STATIC_DRAW);
		glVertexAttribPointer(ofShader::NORMAL_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(ofShader::NORMAL_ATTRIBUTE);

		GLuint indicesbuffer;
		glGenBuffers(1, &indicesbuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesbuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)*vi.size(), &vi[0], GL_STATIC_DRAW);
		
		glBindVertexArray(0);

		vertexArrayObjs[primitive_type] = vao;

		return vao;
	}

	primitives3d::primitives3d(Primitives3dType primitiveType, vector<GLfloat>& primitiveData)		
	{					
		GLuint vao = getVertexArrayObj(primitiveType);
		glBindVertexArray(vao);
		draw(primitiveType);
		glBindVertexArray(0);
	}


	void primitives3d::draw(Primitives3dType primitiveType){
		if (primitiveType == P_CUBE){
			glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, 0);
		}
		else if (primitiveType == P_TETRA){
			glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_SHORT, 0);
		}
		else if (primitiveType == P_OCTA){
			glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_SHORT, 0);
		}
		else if (primitiveType == P_DODE){							
			glDrawElements(GL_TRIANGLES, 108, GL_UNSIGNED_SHORT, 0);			
		}
		else if (primitiveType == P_ISO){
			glDrawElements(GL_TRIANGLES, 60, GL_UNSIGNED_SHORT, 0);
		}		
	}

	primitives3d::~primitives3d(){}

	


} // end of namespace primitives


