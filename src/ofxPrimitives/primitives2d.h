/**
* \file primitives2d.h
* \brief Permet de produire des primitives 2D
* \author Pascal Renauld
* \version 1.0
*
*/
#pragma once

#include "ofMain.h"
#include "ofConstants.h"


/**
* \namespace primitives
* \brief Espace de nom les classes des primitives.
*/
namespace primitives{

	enum Primitives2dType{
		P_POINT,
		P_LINE,
		P_TRIANGLE,
		P_SQUARE,
		P_RECTANGLE,
		P_POLYGON,
		P_CIRCLE,
		P_ELIPSE,
		P_BEZIER
	};

	/**
	* \class primitives2d
	* \brief Primitives 2d
	*
	* Classe responsable de la gestion et de 
	* l'affichage des primitives 2D.
	* Neuf types de primitives 2D sont disponnibles:
	* Point, ligne, triangle, carr�, rectangle,
	* polygon, cercle, elipse, bezier.
	*/
	class primitives2d{
	public:	

		/**
		* \brief Constructeur
		*
		* \param primitiveType Type de primitive � g�n�rer
		* P_POINT, P_LINE,	P_TRIANGLE, P_SQUARE,
		* P_RECTANGLE, P_POLYGON,P_CIRCLE,
		* P_ELIPSE,	P_BEZIER
		* \param primitiveData Vecteur contenant les coordonn�es
		* de chaque points sous la forme x1,y1,z1,x2,y2,z2...
		* \param polySides Nombre de cot� pour les polygones (4 par d�faut)
		*/
		primitives2d(Primitives2dType primitiveType, vector<GLfloat>& primitiveData, GLuint polySides = 4);

		/**
		* \brief Affiche la primitive. Le type de primitive doit
		* tout de m�me �tre fournit
		* \param primitiveType Type de primitive � g�n�rer
		* P_POINT, P_LINE,	P_TRIANGLE, P_SQUARE,
		* P_RECTANGLE, P_POLYGON,P_CIRCLE,
		* P_ELIPSE,	P_BEZIER
		*/
		void draw(Primitives2dType primitiveType);


		/**
		* \brief Destructeur de la classe primitive2d
		*/
		virtual ~primitives2d();

			
	private:
		primitives2d(const primitives2d&); //Disallow copy
		primitives2d& operator=(const primitives2d&);//Prevent assignation
				
		GLuint vao;
		vector<GLfloat> vertices;
		GLuint sides;

};

	


} // end of namespace tp