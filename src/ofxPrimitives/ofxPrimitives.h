/**
* \file ofxPrimitives.h
* \brief Permet de produire des primitives 2D de meme que les 5 solides platoniques
* \author Pascal Renauld
* \version 1.0
*
*/
#pragma once

#include "ofMain.h"
#include "primitives2d.h"
#include "primitives3d.h"


/**
* \namespace primitives
* \brief Espace de nom les classes des primitives.
*/
namespace primitives{

	void p_point(GLfloat x1, GLfloat y1, GLfloat z1);	
	void p_point(GLfloat x1, GLfloat y1);
	void p_line(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2);
	void p_line(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);
	void p_triangle(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3);
	void p_triangle(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3);
	void p_square(GLfloat x1, GLfloat y1, GLfloat sideLength);
	void p_rectangle(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);
	void p_polygon(GLfloat x1, GLfloat y1, GLfloat sideLength, GLfloat nbSides);
	void p_polygon(GLfloat x1, GLfloat y1, GLfloat sideLength, GLfloat nbSides, GLfloat rotation);
	void p_circle(GLfloat x1, GLfloat y1, GLfloat radius, GLfloat precision=100);
	void p_elipse(GLfloat x1, GLfloat y1, GLfloat a, GLfloat b, GLfloat precision = 100);
	void p_bezier(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3);
		
	void p_cube();
	void p_dode();
	void p_tetra();
	void p_octa();
	void p_iso();




} // end of namespace primitives