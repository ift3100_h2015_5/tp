/**
* \file primitives2d.cpp
* \brief Permet de produire des primitives 2D
* \author Pascal Renauld
* \version 1.0
*
*/
#include "primitives2d.h"


/**
* \namespace primitives
* \brief Espace de nom les classes des primitives.
*/
namespace primitives{
	
	primitives2d::primitives2d(Primitives2dType primitiveType, vector<GLfloat>& primitiveData, GLuint polySides)
		:sides(polySides)
	{
		if (primitiveType == P_POINT){
			vertices.resize(3);	
			vertices = primitiveData;			
		}
		else if (primitiveType == P_LINE){
			vertices.resize(6);
			vertices = primitiveData;
		}
		else if (primitiveType == P_TRIANGLE){
			vertices.resize(9);	
			vertices = primitiveData;
		}
		else if (primitiveType == P_SQUARE || primitiveType == P_RECTANGLE){
			sides = 4;
			vertices.resize(3 * sides);
			vertices = primitiveData;
		}
		else if (primitiveType == P_POLYGON){
			vertices.resize(3*sides);
			vertices = primitiveData;
		}
		else if (primitiveType == P_CIRCLE){
			sides = 4;
			vertices.resize(3 * sides);
			vertices[0] = primitiveData[0];
			vertices[2] = primitiveData[1];
			vertices[2] = primitiveData[2];
		}		
		else if (primitiveType == P_BEZIER){			
			vertices.resize(primitiveData.size());
			vertices = primitiveData;
		}
		
		

		GLuint vertexbuffer;
		glGenBuffers(1, &(vertexbuffer));
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vertices.size(), &vertices[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

		draw(primitiveType);

		glDeleteBuffers(1, &vertexbuffer);
		glDeleteVertexArrays(1, &vao);
	}


	void primitives2d::draw(Primitives2dType primitiveType){
		glEnableVertexAttribArray(ofShader::POSITION_ATTRIBUTE);
		
		if (primitiveType == P_BEZIER){
			glVertexAttribPointer(ofShader::POSITION_ATTRIBUTE, 1, GL_FLOAT, GL_FALSE, 0, 0);
		}
		else{
			glVertexAttribPointer(ofShader::POSITION_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}
				
		
		if (primitiveType == P_POINT){
			glDrawArrays(GL_POINTS, 0, 1);
		}
		else if (primitiveType == P_LINE){
			glDrawArrays(GL_LINES, 0, 2);
		}
		else if (primitiveType == P_TRIANGLE){
			glDrawArrays(GL_TRIANGLES, 0, 3);
		}
		else if (primitiveType == P_POLYGON){			
			glDrawArrays(GL_TRIANGLE_FAN, 0, sides);
		}
		else if (primitiveType == P_BEZIER){
			glDrawArrays(GL_LINE_STRIP, 0, 1000);
		}
		
		glDisableVertexAttribArray(ofShader::POSITION_ATTRIBUTE);
	}

	primitives2d::~primitives2d(){
	}

} // end of namespace primitives
