/**
* \file primitives3d.h
* \brief Permet de produire des primitives 3D (les 5 solides platoniques)
* \author Pascal Renauld
* \version 1.0
*
*/
#pragma once

#include "ofMain.h"
#include "ofConstants.h"


/**
* \namespace primitives
* \brief Espace de nom les classes des primitives.
*/
namespace primitives{


	enum Primitives3dType{
		P_CUBE,
		P_TETRA,
		P_OCTA,
		P_DODE,
		P_ISO
	};


	/**
	* \class primitives3d
	* \brief Primitives 3d
	*
	* Classe responsable de la gestion et de
	* l'affichage des primitives 3D.
	* Cinq types de primitives 3D sont disponnibles,
	* il s'agit des cinq polygones platoniques:
	* Tétraède, cube, octaèdre, dodécaèdre, icosaèdre.
	*/
	class primitives3d{
	public:	

		/**
		* \brief Constructeur
		*
		* \param primitiveType Type de primitive à générer
		* P_CUBE, P_TETRA, P_OCTA, P_DODE, P_ISO
		* \param primitiveData Vecteur contenant les coordonnées
		* de chaque points sous la forme x1,y1,z1,x2,y2,z2...
		*/
		primitives3d(Primitives3dType primitiveType, vector<GLfloat>& primitiveData);


		/**
		* \brief Affiche la primitive. Le type de primitive doit
		* tout de même être fournit
		* \param primitiveType Type de primitive à générer:
		* P_CUBE, P_TETRA, P_OCTA, P_DODE, P_ISO,
		*/
		void draw(Primitives3dType primitiveType);


		/**
		* \brief Destructeur de la classe primitive3d
		*/
		virtual ~primitives3d();

			
	private:
		primitives3d(const primitives3d&); //Disallow copy
		primitives3d& operator=(const primitives3d&);//Prevent assignation
		GLfloat size;

		typedef std::map< Primitives3dType, GLuint > VAOMap;
		
		/**
		 * ID des vertex array objects pour chaque solide platonique.
		 */
		static VAOMap vertexArrayObjs;

		/**
		 * Initialise et donne accès au vertex array object the chaque solide.
		 * 
		 * Lors du premier appel pour un solide donné,
		 * le vertex array object de ce solide est créé, envoyé au GPU, puis son ID retourné.
		 * Pour tout appel subséquent concernant le même solide, l'ID du vertex array object
		 * précédemment créé est retourné.
		 */
		static GLuint getVertexArrayObj(Primitives3dType primitive_type);
};

	


} // end of namespace tp