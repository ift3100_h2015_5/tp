/**
* \file ofxPrimitives.cpp
* \brief Permet de produire des primitives 2D de meme que les 5 solides platoniques
* \author Pascal Renauld
* \version 1.0
*
*/
#include "ofxPrimitives.h"

/**
* \namespace primitives
* \brief Espace de nom les classes des primitives.
*/
namespace primitives{


	/**
	* \brief Affiche un point
	* \param[in] x1 coordonee en x
	* \param[in] y1 coordonee en y
	* \param[in] z1 coordonee en z
	*/
	void p_point(GLfloat x1, GLfloat y1, GLfloat z1){
		GLfloat values[] = { x1, y1, z1 };
		vector<GLfloat> dataPoints(values, values+3);
		primitives2d(P_POINT, dataPoints);
	}

	/**
	* \brief Affiche un point
	* \param[in] x1 coordonee en x
	* \param[in] y1 coordonee en y	
	*/
	void p_point(GLfloat x1, GLfloat y1){
		p_point(x1, y1, 0.0f);
	}

	/**
	* \brief Affiche une ligne
	* \param[in] x1 coordonee en x de depart de la ligne
	* \param[in] y1 coordonee en y de depart de la ligne
	* \param[in] z1 coordonee en z de depart de la ligne
	* \param[in] x2 coordonee en x de fin de la ligne
	* \param[in] y2 coordonee en y de fin de la ligne
	* \param[in] z2 coordonee en z de fin de la ligne
	*/
	void p_line(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2){
		GLfloat values[] = { x1, y1, z1, x2, y2, z2};
		vector<GLfloat> dataPoints(values, values+6);
		primitives2d(P_LINE, dataPoints);
	}
	
	/**
	* \brief Affiche une ligne
	* \param[in] x1 coordonee en x de depart de la ligne
	* \param[in] y1 coordonee en y de depart de la ligne	
	* \param[in] x2 coordonee en x de fin de la ligne
	* \param[in] y2 coordonee en y de fin de la ligne
	*/
	void p_line(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2){
		p_line(x1, y1, 0.0f, x2, y2, 0.0f);
	}

	/**
	* \brief Affiche un triangle
	* \param[in] x1 coordonee en x du vertex 1 du triangle
	* \param[in] y1 coordonee en y du vertex 1 du triangle
	* \param[in] z1 coordonee en z du vertex 1 du triangle
	* \param[in] x2 coordonee en x du vertex 2 du triangle
	* \param[in] y2 coordonee en y du vertex 2 du triangle
	* \param[in] z2 coordonee en z du vertex 2 du triangle
	* \param[in] x3 coordonee en x du vertex 3 du triangle
	* \param[in] y3 coordonee en y du vertex 3 du triangle
	* \param[in] z3 coordonee en z du vertex 3 du triangle
	*/
	void p_triangle(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3){GLfloat values[] = { x1, y1, z1, x2, y2, z2, x3, y3, z3 };
		vector<GLfloat> dataPoints(values, values+9);
		primitives2d(P_TRIANGLE, dataPoints);
	}

	/**
	* \brief Affiche un triangle
	* \param[in] x1 coordonee en x du vertex 1 du triangle
	* \param[in] y1 coordonee en y du vertex 1 du triangle
	* \param[in] x2 coordonee en x du vertex 2 du triangle
	* \param[in] y2 coordonee en y du vertex 2 du triangle
	* \param[in] x3 coordonee en x du vertex 3 du triangle
	* \param[in] y3 coordonee en y du vertex 3 du triangle
	*/
	void p_triangle(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat x3, GLfloat y3){
		p_triangle(x1, y1, 0.0f, x2, y2, 0.0f, x3, y3, 0.0f);
	}
	
	/**
	* \brief Affiche un carre
	* \param[in] x1 coordonee en x du vertex du coin supperieur
	* \param[in] y1 coordonee en y du vertex du coin supperieur
	* \param[in] sideLength longueur du cote du carre
	*/
	void p_square(GLfloat x1, GLfloat y1, GLfloat sideLength){
		p_rectangle(x1, y1, x1 + sideLength, y1 + sideLength);
	}

	/**
	* \brief Affiche un carre
	* \param[in] x1 coordonee en x du vertex 1 du coin supperieur
	* \param[in] y1 coordonee en y du vertex 1 du coin supperieur
	* \param[in] x2 coordonee en x du vertex 2s du coin inferieur
	* \param[in] y2 coordonee en y du vertex 2 du coin inferieur	
	*/
	void p_rectangle(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2){
		GLfloat length = x2 - x1;
		GLfloat height = y2 - y1;
		GLfloat values[] = {
			x1, y1, 0.0f,
			x1 + length, y1, 0.0f,
			x1 + length, y1 + height, 0.0f,
			x1, y1 + height, 0.0f
		};
		vector<GLfloat> dataPoints(values, values+12);
		primitives2d(P_POLYGON, dataPoints, 4);
	}


	/**
	* \brief Affiche un polygon
	* \param[in] x1 coordonee en x du vertex du coin supperieur
	* \param[in] y1 coordonee en y du vertex du coin supperieur
	* \param[in] sideLength longueur du cote du polygone
	* \param[in] nbSides nombre de cotes du polygone
	*/
	void p_polygon(GLfloat x1, GLfloat y1, GLfloat sideLength, GLfloat nbSides){
		p_polygon(x1, y1, sideLength, nbSides, 0.0f);
	}

	/**
	* \brief Affiche un polygon
	* \param[in] x1 coordonee en x du vertex du centre
	* \param[in] y1 coordonee en y du vertex du centre
	* \param[in] sideLength longueur du cote du polygone
	* \param[in] nbSides nombre de cotes du polygone
	* \param[in] rotation angle duquel on tourne le polygone
	*/
	void p_polygon(GLfloat x1, GLfloat y1, GLfloat sideLength, GLfloat nbSides, GLfloat rotation){
		vector<GLfloat> dataPoints;
		GLfloat deltaAngle = TWO_PI/nbSides;
		GLfloat distanceFromVertex = sqrt((sideLength*sideLength)/(2*1+cos(deltaAngle)));
		
		for (GLfloat angle = rotation; angle < TWO_PI + rotation; angle += deltaAngle){
			dataPoints.push_back(x1 + distanceFromVertex*sin(angle));
			dataPoints.push_back(y1 + distanceFromVertex*cos(angle));
			dataPoints.push_back(0.0f);
		}
		primitives2d(P_POLYGON, dataPoints, nbSides);
	}


	/**
	* \brief Affiche un cercle
	* \param[in] x1 coordonee en x du vertex du centre
	* \param[in] y1 coordonee en y du vertex du centre
	* \param[in] radius rayon du cercle
	* \param[in] precision precision du contour	
	*/
	void p_circle(GLfloat x1, GLfloat y1, GLfloat radius, GLfloat precision){
		p_elipse(x1, y1, radius, radius, precision);
	}

	/**
	* \brief Affiche une ellipse
	* \param[in] x1 coordonee en x du vertex du centre
	* \param[in] y1 coordonee en y du vertex du centre
	* \param[in] a axe a de l'ellipse
	* \param[in] b axe a de l'ellipse
	* \param[in] precision precision du contour
	*/
	void p_elipse(GLfloat x1, GLfloat y1, GLfloat a, GLfloat b, GLfloat precision){
		vector<GLfloat> dataPoints;
		GLfloat deltaAngle = TWO_PI / precision;

		for (GLfloat angle = 0.0f; angle < TWO_PI; angle += deltaAngle){
			dataPoints.push_back(x1 + a*cos(angle));
			dataPoints.push_back(y1 + b*sin(angle));
			dataPoints.push_back(0.0f);
		}
		primitives2d(P_POLYGON, dataPoints, precision);
	}



	/**
	* \brief Affiche une courbe de bezier
	* \param[in] x1 coordonee en x de depart de la ligne
	* \param[in] y1 coordonee en y de depart de la ligne
	* \param[in] z1 coordonee en z de depart de la ligne
	* \param[in] x2 coordonee en x du point de reference
	* \param[in] y2 coordonee en y du point de reference
	* \param[in] z2 coordonee en z du point de reference
	* \param[in] x3 coordonee en x de fin de la ligne
	* \param[in] y3 coordonee en y de fin de la ligne
	* \param[in] z3 coordonee en z de fin de la ligne
	*/
	void p_bezier(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3){
		vector<GLfloat> dataPoints;
		for (GLfloat i = 0.0f; i < 1000; i+=.001f){
			dataPoints.push_back(i);
		}		
		ofShader shader2;
		shader2.load("shaderGL3/bezier");
		shader2.begin();
		shader2.setUniform4f("start", x1, y1, z1, 1);
		shader2.setUniform4f("referencePoint", x2, y2, z2, 1);
		shader2.setUniform4f("stop", x3, y3, z3, 1);	
		primitives2d(P_BEZIER, dataPoints);
		shader2.end();
}


	/**
	* \brief Affiche un cube
	*/
	void p_cube(){		
		vector<GLfloat> dataPoints;
		dataPoints.push_back(0.0f);
		primitives3d(P_CUBE, dataPoints);
	}

	/**
	* \brief Affiche un dodecaedre
	*/
	void p_dode(){
		vector<GLfloat> dataPoints;
		dataPoints.push_back(0.0f);
		primitives3d(P_DODE, dataPoints);
	}


	/**
	* \brief Affiche un tetraaedre
	*/
	void p_tetra(){
		vector<GLfloat> dataPoints;
		dataPoints.push_back(0.0f);
		primitives3d(P_TETRA, dataPoints);
	}

	/**
	* \brief Affiche un octaedre
	*/
	void p_octa(){
		vector<GLfloat> dataPoints;
		dataPoints.push_back(0.0f);
		primitives3d(P_OCTA, dataPoints);
	}

	/**
	* \brief Affiche un isoedre
	*/
	void p_iso(){
		vector<GLfloat> dataPoints;
		dataPoints.push_back(0.0f);
		primitives3d(P_ISO, dataPoints);
	}

} // end of namespace primitives



