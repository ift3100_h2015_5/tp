/**
* \file ColorIDManager.cpp
* \brief Permets de determiner la couleur d'un secteur pour la detection des clics.
* \author Equipe 5
* \version 1.0
*
*/
#include "ColorIDManager.h"
#include "nosmaths/nosmaths.h"

ColorIDManager::ColorIDManager()
{ }



ColorIDManager::~ColorIDManager()
{ }



ofVec3f ColorIDManager::getID(Sector * sector) {
	ofVec3f result;
	Maze * maze = sector->getParent();
	
	result.x = (float)sector->getMazePosX() / maze->getNbSectorsH();
	result.y = (float)sector->getMazePosY() / maze->getNbSectorsV();
	result.z = 0;

	return result;
}



Sector * ColorIDManager::getSector(ofVec3f color_id, Maze * maze) {
	///S'il y a du bleu, ce n'est pas un secteur
	if (!nosmaths::closeEnough(color_id.z, 0.0f, 0.0001f)) {
		return 0;
	}
	
	int x = floor(color_id.x * maze->getNbSectorsH() + 0.5);
	int y = floor(color_id.y * maze->getNbSectorsV() + 0.5);

	Sector * sector = maze->getSector(x, y);

	return sector;
}