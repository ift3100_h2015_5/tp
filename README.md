# LISEZ MOI #

Ceci est un projet réalisé pour le cours Infographie (IFT-3100) de la session d'Hiver 2015, à l'Université Laval. Il s'agissait de produire un progiciel implémentant un certains nombre de concepts du domaine de l'infographie. Le sujet et les technologies utilisées étaient libres.

Nos souvenirs d'enfance, de même que notre désir d'explorer plusieurs facettes de l'infographie, ont été des facteurs déterminants dans le choix du concept de notre application. Deux modes principaux d’utilisation y sont présentés. Il s'agit d'abord d'un éditeur de labyrinthe avec une vue orthographique. Ce dernier permet de créer un labyrinthe 3D, d'ajouter ou retirer des murs et des obstacles, de même que de modifier certains éléments du décor. Le deuxième consiste de naviguer dans un de ces labyrinthes, généré aléatoirement ou créer via l’éditeur, sous une vue 3D à la première personne.

Voici deux présentations vidéos du projet, chacune ayant été produite dans la foulée d'une remise :

https://youtu.be/py2VLBq80jc?list=PLmnIn9A29-ClavbEJeDvwWkA36w5ZG1qC

### Installation ###

Nous avons compilé des exécutables pour les plateformes suivantes :

* Windows 64 bits
* Windows 32 bits
* OSX

Voir la section [Downloads](https://bitbucket.org/ift3100_h2015_5/tp/downloads) du projet.

### Compilation et documentation technique ###

Ce projet est produit à l'aide de [OpenFrameworks](http://openframeworks.cc/). Nous avons cependant réimplémenté bon nombre de fonctionnalités déjà présentes dans ce framework à des fins pédagogiques.

Pour de plus amples informations, voir le document [doc/Design.pdf](https://bytebucket.org/ift3100_h2015_5/tp/raw/master/doc/Design.pdf).

### Licence ###

Ce projet est publié sous la licence [BSD-3](http://opensource.org/licenses/BSD-3-Clause) :

Copyright (c) 2015, Patrick Landry, Pascal Renauld et François Moreau

All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.