#version 150

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

in vec3  position;
in vec2  texcoord;
in vec3  normal;
	 
out vec2 texCoordVarying;
out vec4 normalVarying;
out vec3 worldPosition;
out vec4 viewSpace;

void main()
{
	texCoordVarying = texcoord;  
	normalVarying = vec4(normal, 1.0);
	worldPosition = (model * vec4(position, 1.0)).xyz;  
	viewSpace= view * model * vec4(position, 1.0); 
	gl_Position = projection *viewSpace; 
}

