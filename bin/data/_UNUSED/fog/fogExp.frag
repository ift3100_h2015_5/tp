#version 150

uniform sampler2D tex0;
uniform vec3 ambientLight;
uniform vec3 diffuseLight;
uniform vec3 lightPosition;

const int depthFog = 1;
const vec3 edgeColor = vec3(0.2, 0.2, 0.2);
const vec3 fogColor = vec3(0.5, 0.5, 0.5);
const float fogDensity = 0.85;

in vec2 texCoordVarying;
in vec4 normalVarying;
in vec3 worldPosition;
in vec4 viewSpace;

out vec4 color;

void main()
{
	//Calcul de la normale et de la direction de la lumiere
	vec3 norm = normalize(normalVarying.xyz);
	vec3 lightDirection = normalize(lightPosition - worldPosition); 

	// Eclairage diffus
	vec3 diffuseComponent = diffuseLight;
	float diffuse = max(dot(norm, lightDirection), 0.0);
	diffuseComponent = diffuse * diffuseComponent;

	// La limite n'a pas les memes specificitees
	float edge = 1-max(dot(lightDirection, worldPosition),0.0);
	edge = smoothstep(0.5,1.0,edge);
	vec3 edgeTotal = edgeColor*vec3(edge,edge,edge);

	// Integration de la texture
	vec2 upside_texcoord = vec2(texCoordVarying.x, 1.0 - texCoordVarying.y);
        vec4 colorWithTexture = vec4((ambientLight + diffuseComponent), 1.0) * texture(tex0, upside_texcoord);
	vec3 lightColor = edgeTotal + diffuse + colorWithTexture.rgb;



	// La quantite de lumiere decroit selon une exp de la distance
	float distance = length(viewSpace);
	float fogFactor = 1.0 /exp(distance * fogDensity);
        fogFactor = clamp( fogFactor, 0.0, 1.0 );
	
        vec3 foggyColor = mix(fogColor, lightColor, fogFactor);
	color=vec4(foggyColor, 1);

} 