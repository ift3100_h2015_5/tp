#version 150 core
in vec2 TexCoords;

out vec4 color;
uniform float interpolation;
uniform sampler2D tex0;
const float PI=3.14159;

void main()
{ 
    vec4 texcolor = texture(tex0, TexCoords);
	color = vec4(texcolor.rgb, 1.0);

	// Inversion des couleurs.
	vec3 coldFX=vec3(1.0- texture(tex0, TexCoords));

	color = vec4((1.0-interpolation)*coldFX+interpolation*texcolor.rgb, 1.0);
}