#version 150 core
 
uniform sampler2D tex0;
 
in vec2 TexCoords;
 
out vec4 color;

void main()
{
    color = vec4(0.0);
    color += texture(tex0, TexCoords);
}