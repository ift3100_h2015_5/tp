#version 150 core

in vec2 position;
in vec2 texCoords;
 
out vec2 TexCoords;
out vec2 BlurTexCoords[14];
 
void main()
{
    gl_Position = vec4(position.x, position.y, 0.0, 1.0); 
    TexCoords = texCoords;
    BlurTexCoords[ 0] = TexCoords + vec2(0.0, -0.028);
    BlurTexCoords[ 1] = TexCoords + vec2(0.0, -0.024);
    BlurTexCoords[ 2] = TexCoords + vec2(0.0, -0.020);
    BlurTexCoords[ 3] = TexCoords + vec2(0.0, -0.016);
    BlurTexCoords[ 4] = TexCoords + vec2(0.0, -0.012);
    BlurTexCoords[ 5] = TexCoords + vec2(0.0, -0.008);
    BlurTexCoords[ 6] = TexCoords + vec2(0.0, -0.004);
    BlurTexCoords[ 7] = TexCoords + vec2(0.0,  0.004);
    BlurTexCoords[ 8] = TexCoords + vec2(0.0,  0.008);
    BlurTexCoords[ 9] = TexCoords + vec2(0.0,  0.012);
    BlurTexCoords[10] = TexCoords + vec2(0.0,  0.016);
    BlurTexCoords[11] = TexCoords + vec2(0.0,  0.020);
    BlurTexCoords[12] = TexCoords + vec2(0.0,  0.024);
    BlurTexCoords[13] = TexCoords + vec2(0.0,  0.028);
}