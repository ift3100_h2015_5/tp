#version 150 core
in vec2 TexCoords;
out vec4 color;
uniform sampler2D tex0;
uniform float interpolation;  
const vec2 offset=vec2(.01,.01);    // TODO Transformer en uniform avec (1/largeur,1/hauteur)
const vec4 luminance = vec4(0.299, 0.587, 0.114, 0);

void main() {
  vec4 texColor = vec4(texture(tex0, TexCoords));

  // On fait un échantillonage des pixels environnant selon l'offset
  vec2 texels0 = TexCoords.st + vec2(-offset.s, -offset.t);
  vec2 texels1 = TexCoords.st + vec2(         0.0, -offset.t);
  vec2 texels2 = TexCoords.st + vec2(-offset.s,          0.0);
  vec2 texels3 = TexCoords.st + vec2(+offset.s,          0.0);
  vec2 texels4 = TexCoords.st + vec2(         0.0, +offset.t);
  vec2 texels5 = TexCoords.st + vec2(+offset.s, +offset.t);
  
  // On identifie la couleur des pixels environnant  
  vec4 color0 = texture(tex0, texels0);
  vec4 color1 = texture(tex0, texels1);
  vec4 color2 = texture(tex0, texels2);
  vec4 color3 = texture(tex0, texels3);
  vec4 color4 = texture(tex0, texels4);
  vec4 color5 = texture(tex0, texels5);

  vec4 total = vec4(0.5) + (color0 + color1 + color2) - (color3 + color4 + color5);
  float lumi = dot(total, luminance)*(1.0-interpolation);
  color = vec4(lumi+interpolation,lumi+interpolation,lumi+interpolation,1.0) * texColor;
}