#version 150 core
in vec2 TexCoords;
out vec4 color;
uniform sampler2D tex0;
uniform float interpolation;  
const vec2 offset=vec2(.01,.01);  // TODO Transformer en uniform avec (1/largeur,1/hauteur)

void main() {
  vec4 texColor = vec4(texture(tex0, TexCoords)); 
 
 
  // On fait un échantillonage des pixels environnant selon l'offset
  vec2 texels0 = TexCoords.st + vec2(-offset.s, -offset.t);
  vec2 texels1 = TexCoords.st + vec2(         0.0, -offset.t);
  vec2 texels2 = TexCoords.st + vec2(+offset.s, -offset.t);
  vec2 texels3 = TexCoords.st + vec2(-offset.s,          0.0);
  vec2 texels4 = TexCoords.st + vec2(         0.0,          0.0);
  vec2 texels5 = TexCoords.st + vec2(+offset.s,          0.0);
  vec2 texels6 = TexCoords.st + vec2(-offset.s, +offset.t);
  vec2 texels7 = TexCoords.st + vec2(         0.0, +offset.t);
  vec2 texels8 = TexCoords.st + vec2(+offset.s, +offset.t);

  // On identifie la couleur des pixels environnant  
  vec4 color0 = texture(tex0, texels0);
  vec4 color1 = texture(tex0, texels1);
  vec4 color2 = texture(tex0, texels2);
  vec4 color3 = texture(tex0, texels3);
  vec4 color4 = texture(tex0, texels4);
  vec4 color5 = texture(tex0, texels5);
  vec4 color6 = texture(tex0, texels6);
  vec4 color7 = texture(tex0, texels7);
  vec4 color8 = texture(tex0, texels8);

  // On intensifi la couleur du pixel central par rapport aux couleurs
  // environnantes pour mettre en valeur les contours.
  vec4 total = (1.0-interpolation)*(8.0 * color4 - (color0 + color1 + color2 + color3 + color5 + color6 + color7 + color8)); 
  vec3 colorProportion=vec3(interpolation);
  color = vec4(total.rgb+colorProportion, 1.0) * texColor;
}