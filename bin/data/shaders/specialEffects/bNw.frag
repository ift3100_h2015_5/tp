#version 150 core
in vec2 TexCoords;

out vec4 color;
uniform float interpolation;
uniform sampler2D tex0;

void main()
{ 
    vec4 texcolor = texture(tex0, TexCoords);
	color = vec4(texcolor.rgb, 1.0);

	// Effent noir et blanc
	float average = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
	float bNwProportion=(1.0-interpolation)*average;	
   	color = vec4(bNwProportion+interpolation*color.r, bNwProportion+interpolation*color.g, bNwProportion+interpolation*color.b, 1.0);
}


