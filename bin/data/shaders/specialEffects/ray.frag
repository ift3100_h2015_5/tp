#version 150 core
in vec2 TexCoords;

out vec4 color;
uniform sampler2D tex0;
uniform float interpolation;
const float  N = 400;   // Nombre d'�chantillons
const float Density= 1.0;
const float Weight= 0.5;
const float Decay= .95;
const float Exposure= 0.5;


void main()
{ 
  vec2 newTexCoords=TexCoords;
  vec2 lightPos=vec2(1.0,1.0);
  vec3 texColor = vec3(texture(tex0, newTexCoords)); 

   // Vecteur part rapport � la source de lumi�re
  vec2 deltaTexCoord = newTexCoords - lightPos;
  deltaTexCoord *= 1.0f / N * Density;
  float illuminationDecay = 1.0f; 
  
   // Prend des �chantillons le long du rayon de lumi�re
  float N2=(1.0-interpolation)*N+1;
  for (int i = 0; i < N2; i++)  
  {  
    newTexCoords -= deltaTexCoord;
    vec3 sample = vec3(texture(tex0, newTexCoords)); 
    sample = vec3(sample.r*illuminationDecay * Weight,sample.g*illuminationDecay * Weight,sample.b*illuminationDecay * Weight); 
 
    // On accumule les couleurs  
    texColor +=  sample;  
    illuminationDecay *= Decay;  
  }  

  color = vec4( texColor.r * Exposure, texColor.g * Exposure, texColor.b * Exposure, 1.0);  
}