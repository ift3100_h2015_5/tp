#version 150 core
in vec2 TexCoords;

out vec4 color;

uniform sampler2D tex0;
uniform float interpolation;
const float piTimes8 = 4*2*3.14159;

void main()
{ 
  vec2 newTexCoords=TexCoords;
  float phaseShift=interpolation*piTimes8/4;
  newTexCoords.x += sin(newTexCoords.y * piTimes8  + phaseShift) / 100;
  color = texture(tex0, newTexCoords);

}