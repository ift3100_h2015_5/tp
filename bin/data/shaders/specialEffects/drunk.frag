#version 150 core
in vec2 TexCoords;

out vec4 color;

uniform sampler2D tex0;
uniform float interpolation;

void main()
{ 
    vec4 texcolor = texture(tex0, TexCoords);
	color = vec4(texcolor.rgb, 1.0);



	float offset = 1.0/100 * (1.0f - interpolation);

	//Génération de la matrice d'offset pour sélectionner les pixels
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset), vec2(0.0f, offset), vec2(offset, offset),  
        vec2(-offset, 0.0f),   vec2(0.0f, 0.0f),   vec2(offset,  0.0f),   
        vec2(-offset, -offset),vec2(0.0f, -offset),vec2(offset,  -offset));

	// Kernel pour effet de flou
	float kernel[9] = float[](
          1.0 / 16, 2.0 / 16, 1.0 / 16,
          2.0 / 16, 4.0 / 16, 2.0 / 16,
          1.0 / 16, 2.0 / 16, 1.0 / 16 );

        vec3 sample[9];
        for(int i = 0; i < 9; i++){
		sample[i] =  vec3(texture(tex0, TexCoords.st + offsets[i]));
    	}
    
	vec3 col;
	col=vec3(0,0,0);
    for(int i = 0; i < 9; i++){
        	col += sample[i] * kernel[i];
	}


	color = vec4(col, 1.0);

}