#version 150 core
 
uniform sampler2D tex0;
 
in vec2 TexCoords;
in vec2 BlurTexCoords[14];
 
out vec4 color;

void main()
{
    color = vec4(0.0);
    color += texture(tex0, BlurTexCoords[ 0])*0.0044299121055113265;
    color += texture(tex0, BlurTexCoords[ 1])*0.00895781211794;
    color += texture(tex0, BlurTexCoords[ 2])*0.0215963866053;
    color += texture(tex0, BlurTexCoords[ 3])*0.0443683338718;
    color += texture(tex0, BlurTexCoords[ 4])*0.0776744219933;
    color += texture(tex0, BlurTexCoords[ 5])*0.115876621105;
    color += texture(tex0, BlurTexCoords[ 6])*0.147308056121;
    color += texture(tex0, TexCoords         )*0.159576912161;
    color += texture(tex0, BlurTexCoords[ 7])*0.147308056121;
    color += texture(tex0, BlurTexCoords[ 8])*0.115876621105;
    color += texture(tex0, BlurTexCoords[ 9])*0.0776744219933;
    color += texture(tex0, BlurTexCoords[10])*0.0443683338718;
    color += texture(tex0, BlurTexCoords[11])*0.0215963866053;
    color += texture(tex0, BlurTexCoords[12])*0.00895781211794;
    color += texture(tex0, BlurTexCoords[13])*0.0044299121055113265;
}