#version 150 core
in vec2 TexCoords;

out vec4 color;

uniform sampler2D tex0;

void main()
{
	color = texture(tex0, TexCoords);
}