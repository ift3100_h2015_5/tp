#version 150 core
in vec2 TexCoords;

out vec4 color;

uniform float interpolation;
uniform sampler2D tex0;

void main()
{ 
    vec4 texcolor = texture(tex0, TexCoords);
	color = vec4(texcolor.r, texcolor.g * interpolation, texcolor.b * interpolation, 1.0);
}