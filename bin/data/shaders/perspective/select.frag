#version 150

uniform int do_texture;

uniform sampler2D tex0;

uniform vec3 select_color;

in vec2 texCoordVarying;
in vec4 normalVarying;
in vec3 worldPosition;

out vec4 color;

void main()
{
	vec2 upside_texcoord = vec2(texCoordVarying.x, 1.0 - texCoordVarying.y);
	
	if(1 == do_texture) {
		color = texture(tex0, upside_texcoord);
	} else {
		color = vec4(select_color, 1);
	}
}