#version 150
uniform int skip_projectors;

uniform int do_normalmap;

uniform int do_flat_color;
uniform vec3 flat_color;

uniform vec3 ambientLight;

uniform vec3 camera_position;

uniform vec3 player_pt_light_pos;
uniform vec3 player_pt_light_color;
uniform float player_pt_light_range;

uniform vec3 directional_light_color;
uniform vec3 directional_light_dir;

uniform vec3 pt_lights_pos[4];
uniform vec3 pt_lights_color[4];
uniform float pt_lights_range[4];

uniform vec3 projectors_pos[3];
uniform vec3 projectors_color[3];
uniform vec3 projectors_dir[3];
uniform float projectors_range[3];
uniform float projectors_cone[3];

uniform sampler2D tex0;
uniform sampler2D texN;

in vec2 texCoordVarying;
in vec3 normalVarying;
in vec3 worldPosition;

in mat3 TBN;

out vec4 color;



/**
 * Calcul de l'unique lumière directionnelle.
 *
 * \param normal La normale du fragment courant.
 *
 * \return Impact lumineux de cette lumière sur le fragment courant.
 */
vec3 directional_light(in vec3 normal);



/**
 * Calcul d'une lumière ponctuelle.
 *
 * \param normal La normale du fragment courant.
 * \param pt_light_pos Position de l'émetteur de lumière.
 * \param pt_light_color Couleur de la lumière.
 * \param pt_light_range Rayon d'action de la lumière.
 * 
 * \return Impact lumineux de cette lumière sur le fragment courant.
 */
vec3 pt_light(in vec3 normal, in vec3 pt_light_pos, in vec3 pt_light_color, in float pt_light_range);



/**
 * Calcul d'une lumière projecteur.
 * 
 * \param projector_pos Position de l'émetteur de lumière.
 * \param projector_color Couleur de la lumière.
 * \param projector_dir Direction vers laquelle le projecteur est pointé.
 * \param projector_range Rayon d'action de la lumière.
 * \param projector_cone Contrôle de la largeur de cône. Plus la valeur est grande, plus le cône est petit.
 *
 * \return Impact lumineux de cette lumière sur le fragment courant.
 */
vec3 proj_light(in vec3 projector_pos, in vec3 projector_color, in vec3 projector_dir, in float projector_range, in float projector_cone);



void main()
{
	vec2 upside_texcoord = vec2(texCoordVarying.x, 1.0 - texCoordVarying.y);

	//Utile pour les sources lumineuses
	if(do_flat_color == 1) {
		color = vec4(flat_color, 1);
		return;
	}
	
	//Utile pour les drapeaux, dont on dessine les deux côtés des faces
	vec3 normal = normalVarying;
	if(!gl_FrontFacing) {
		normal = normal * -1.0;
	}

	if(do_normalmap == 1) {
		vec3 color_from_nmap = texture(texN, upside_texcoord).rgb;
		vec3 normal_from_nmap = color_from_nmap * 2.0 - vec3(1, 1, 1);
		normal = normalize(TBN * normal_from_nmap);
	}

	vec3 light = vec3(0, 0, 0);

	light += ambientLight;

	light += directional_light(normal);
	
	//Lumière ponctuelle du joueur
	light += pt_light(normal, player_pt_light_pos, player_pt_light_color, player_pt_light_range);

	//Lumières ponctuelles des torches
	int ii;
	for(ii = 0; ii < 4; ++ii) {
		light += pt_light(normal, pt_lights_pos[ii], pt_lights_color[ii], pt_lights_range[ii]);
	}

	//Lumières projecteurs du monstre
	if(1 != skip_projectors) {
		for(ii = 0; ii < 3; ++ii) {
			light += proj_light(projectors_pos[ii], projectors_color[ii], projectors_dir[ii], projectors_range[ii], projectors_cone[ii]);
		}
	}

	//Éviter du brûler la texture, telle une photo surexposée
	light = min(light, vec3(1, 1, 1));

	color = vec4(light, 1.0f) * texture(tex0, upside_texcoord);
}



vec3 directional_light(in vec3 normal) {
	vec3 LD = directional_light_dir;

	return max(dot(normal, LD), 0.0f) * directional_light_color;
}



vec3 pt_light(in vec3 normal, in vec3 pt_light_pos, in vec3 pt_light_color, in float pt_light_range) {
	vec3 light = vec3(0, 0, 0);

	float dist = distance(worldPosition, pt_light_pos);

	if(dist > pt_light_range) {
		return light;
	}

	//Réflexion diffuse
	vec3 L = normalize(pt_light_pos - worldPosition);

	//Limiter le champ d'impact
	float intensity = 1 - pow( dist / pt_light_range, 2);
	intensity = max(intensity, 0);

	light = light + max(dot(normal, L), 0.0f) * pt_light_color * intensity;

	//Réflexion spéculaire
	//Vraiment bof pour l'instant
	//Faudrait contrôler par matériel et jouer avec les paramètres
	vec3 V = normalize(camera_position - worldPosition); //Direction du fragment vers l'observateur
	vec3 H = normalize(V + L); //Direction à mi-chemin entre la lumière et V
	light = light + pow( max(dot(normal, H), 0.0f), 256) * vec3(.85, .85, .85) * intensity;

	return light;
}



vec3 proj_light(in vec3 projector_pos, in vec3 projector_color, in vec3 projector_dir, in float projector_range, in float projector_cone) {
	vec3 light = vec3(0, 0, 0);
	float dist = distance(worldPosition, projector_pos);

	if(dist > projector_range) {
		return light;
	}

	//Réflexion diffuse
	vec3 LP = normalize(projector_pos - worldPosition);
		
	//Limiter le champ d'impact
	float intensity = 1 - pow( dist / projector_range, 2);
	intensity = max(intensity, 0);

	light += pow( max( dot(-projector_dir, LP), 0), projector_cone) * projector_color * intensity;

	return light;
}