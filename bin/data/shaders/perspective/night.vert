#version 150

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 normal_matx;

in vec3  position;
in vec2  texcoord;
in vec3  normal;
in vec3 tangent;
in vec3 bitangent;
	 
out vec2 texCoordVarying;
out vec3 normalVarying;
out vec3 worldPosition;

out vec3 tangentFrag;
out vec3 bitangentFrag;

out mat3 TBN; //

void main()
{
	mat3 normal_matx3 = mat3(normal_matx);
	
	texCoordVarying = texcoord;
	normalVarying = normalize(normal_matx3 * normal);
	worldPosition = (model * vec4(position, 1.0f)).xyz;
	gl_Position = projection * view * vec4(worldPosition, 1.0f);
	
	TBN = mat3(normalize(normal_matx3 * tangent),
			   normalize(normal_matx3 * bitangent),
			   normalVarying);
}

