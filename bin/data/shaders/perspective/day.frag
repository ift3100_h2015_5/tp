#version 150

uniform sampler2D tex0;
uniform vec3 fogColor;
uniform vec3 diffuseLight;
uniform vec3 lightPosition;

const float fogDensity = 0.5;

in vec2 texCoordVarying;
in vec4 normalVarying;
in vec3 worldPosition;
in vec4 viewSpace;

out vec4 color;

void main()
{
	vec3 diffuseComponent = diffuseLight;
	vec3 norm = normalize(normalVarying.xyz);
	if(!gl_FrontFacing) {
		norm = norm * -1.0;
	}
	vec3 lightDir = normalize(lightPosition - worldPosition);
	
	// Illuminer selon l'angle de la lumiere
	float diffuse = max(dot(norm, lightDir), 0.0f);
	diffuseComponent = diffuse * diffuseComponent;

	vec2 upside_texcoord = vec2(texCoordVarying.x, 1.0 - texCoordVarying.y);
	vec4 light_color = vec4(diffuseComponent, 0.0f) + texture(tex0, upside_texcoord);

	//fog
	float distance = length(viewSpace);
	float fogFactor = 1.0 /exp(distance * fogDensity);
    fogFactor = clamp(fogFactor, 0.0, 1.0);
    vec3 foggyColor = mix(fogColor, light_color.rgb, fogFactor);
	color = vec4(foggyColor, light_color.a);
}