#version 150

uniform sampler2D tex0;
uniform vec3 color_effect;
in vec2 texCoordVarying;
out vec4 color;

void main()
{
    color = texture(tex0, texCoordVarying) * vec4(color_effect, 1.0f);
} 