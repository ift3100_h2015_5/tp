#version 150

in vec3 maze_position; //Suffixe pour éviter les collisions de noms avec les binds d'OF
in vec2 maze_texcoord; //Suffixe pour éviter les collisions de noms avec les binds d'OF

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec2 texCoordVarying;

void main()
{
    gl_Position = projection * view * model * vec4(maze_position, 1.0f);
	texCoordVarying = maze_texcoord;
}

