#version 150

uniform sampler2D tex0;
uniform vec3 color_effect;
in vec2 texCoordVarying;
out vec4 color;

void main()
{
    color = vec4( vec3(1, 1, 1) * color_effect, 1.0 );
} 